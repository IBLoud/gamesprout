NewsItem.raise_on_save_failure = true

def report_error(item, error)
  puts "\n"
  puts item.class.to_s + ": "

  ap item.attributes
  
  puts "\n"
  puts "Errors:"
  item.valid? # must call before it knows whether there are error or not
  if item.errors.any?
    ap item.errors.to_h
  end

  unless Rails.env.production?
    raise error
  end
end

def create_news_item(h)
  news_item = NewsItem.new
  news_item.title = h[:title]
  news_item.summary = h[:summary]
  news_item.content = h[:content]
  news_item.pub_date = h[:pub_date]

  news_item.created_at = h[:created_at]
  news_item.updated_at = h[:updated_at]

  news_item.posted_by = User.first(moped_id: h[:posted_by_id].to_s)
  news_item.edited_by = User.first(moped_id: h[:edited_by_id].to_s) if h[:edited_by_id]

  begin
    news_item.save!
  rescue Exception => e
    report_error(news_item, e)
  end
end

proc = Proc.new {
  news_items = YAML.load_file "#{Rails.root}/tmp/news.yaml"
  news_items.each do |hash|
    create_news_item(hash)
  end
}

true if proc.call