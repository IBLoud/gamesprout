namespace :reputation do
  desc 'recalculate reputation data for all users'
  task :calculate => :environment do
    UserReputationData.get_reputation_data.each do |d|
      user_rep = UserReputationData.for_user_id d["user_id"]
      user_rep.comments_count         = d["comments_count"]
      user_rep.discussions_count      = d["discussions_count"]
      user_rep.polls_count            = d["polls_count"]
      user_rep.arts_count             = d["arts_count"]
      user_rep.videos_count           = d["videos_count"]
      user_rep.demos_count            = d["demos_count"]
      user_rep.thanked_count          = d["thanks_received"]

      user_rep.save
    end
  end
end