namespace :gamejam do
  desc 'automatically create accounts for gamejame participants and send them a notification'
  task :import, [:url] => :environment do |t, args|
    require 'open-uri'

    url = args[:url]
    xml = nil
    open(url) { |f| xml = f.readlines }
    xml = xml.join

    data = Hash.from_xml xml
    data["users"]["user"].each do |user_hash|
      username = user_hash["Username"]
      email = user_hash["E_mail"]
      password = ('a'..'z').to_a.shuffle[0,8].join

      if email == nil
        puts "\r-- empty email, moving on.\r\r"
        next
      end

      if User.all(:email => email).any? # if existing users signed up, we do nothing
        puts "\r#{email} - an account with this email already exists\r"
        next
      end

      user = User.new
      user.username = username
      user.email = email
      user.password = user.password_confirmation = password
      user.terms = '1'
      user_saved = user.save

      if user_saved
        UserMailer.gamejam_signup(user, password).deliver
        puts "Saved user: #{username}\r"
      else
        puts "\rFAILED TO SAVE USER: #{user_hash.to_s}\r"
        user.errors.full_messages.each do |msg|
          puts msg + "\r"
        end

        puts "\r\r"
      end
    end
  end
end