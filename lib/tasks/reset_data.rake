namespace :db do
  desc 'reset database and data'  
  task :reset_data => :environment do
    puts "Automigrating database"
    `rake db:automigrate`

    print "Importing users... "
    require "#{Rails.root}/lib/import_users.rb"
    puts "Done!"

    print "Importing ideas... "
    require "#{Rails.root}/lib/import_ideas.rb"
    puts "Done!"

    print "Importing follows... "
    require "#{Rails.root}/lib/import_follows.rb"
    puts "Done!"

    print "Importing news... "
    require "#{Rails.root}/lib/import_news.rb"
    puts "Done!"
  end

  desc 'sets all user passwords to Mike@345'
  task :reset_passwords => :environment do
    User.each do |user|
      user.password = user.password_confirmation = 'Mike@345'
      user.save
      print '.'
    end
    puts ''
  end
end