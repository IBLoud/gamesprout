require 'open-uri'

namespace :notifications do
  desc 'sends a batch email with recent activity to each user'
  task :send => :environment do
    User.all.each do |user|
      send_notification_to(user)
    end
  end
  
  desc 'send notification to specific user (for local testing)'
  task :send_to, [:username] => :environment do |t, args|
    user = User.first(username: args[:username])
    send_notification_to(user)
  end

  desc 'send notification to specific user but do not save last_notified_on (for local testing)'
  task :send_to_and_forget, [:username] => :environment do |t, args|
    user = User.first(username: args[:username])
    send_notification_to(user, :forget => true)
  end

  def send_notification_to(user, options={})
    if user.notify
      activity = Notifications::Notifier.notifiable_activity_for(user)
      if activity.any? && activity[:count] > 0 && UserMailer.activity_notification(user, activity).deliver
        user.last_notified_on = DateTime.now
        user.save unless options[:forget]
      end
    end
  end
end