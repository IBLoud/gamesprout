namespace :cache do
  desc 'Clear the cache regardless of the provider'
  task :clear => :environment do
    Rails.cache.clear
  end
end