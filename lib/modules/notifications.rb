# TODO: The logic in here needs a complete rewrite to work with DataMapper and the new one-Post-model architecture

module Notifications
  class Notifier
    @@post_type_sort_order = {
      "discussion" => 1,
      "poll" => 2,
      "art_collection" => 3,
      "prototype" => 4
    }

    def initialize(user)
      @user = user
    end

    def self.notifiable_activity_for(user)
      notifier = self.new(user)
      notifier.compile_recent_activity
    end

    def compile_recent_activity
      activity = {}      

      # -- user's ideas that have had activity (yesses, posts, comments)
      users_active_ideas = get_active_owner_ideas
      compiled_users_active_ideas = compile_ideas_hash(users_active_ideas, :include_yes_count => true)
      
      # -- followed ideas that have had activity (posts, comments)
      followed_active_ideas = get_active_followed_ideas
      compiled_followed_active_ideas = compile_ideas_hash(followed_active_ideas)

      # -- ideas that have posts user commented on, ignore ideas user owns or is following
      participated_in_ideas = get_partipated_in_ideas(users_active_ideas, followed_active_ideas)
      participated_in_ideas_hash = compile_ideas_hash(participated_in_ideas) do |post|
        post.user == @user || post.has_comments_by?(@user)
      end

      # HACK: I don't know why sometimes there are ideas with no meaningful activity, but until the query is debugged, remove any ideas with no comment
      # TODO: Fix the query, or whatever is causing this.
      participated_in_ideas_hash = participated_in_ideas_hash.select do |idea, data|
        data[:comments].any?
      end

      # no posts will ever be kept for participated_in_ideas_hash, but we do want to know what posts are releveant, so we pull them out of the relevant comments
      participated_in_ideas_hash.each do |idea, data|
        posts = []
        data[:comments].each do |comment|
          posts.push comment.post
        end

        data[:posts] = (data[:posts] + posts).uniq
      end

      # -- count up how many comments and ideas total we have, so that we can easily determine if any meaningful activity actually occured
      count = 0
      [compiled_users_active_ideas, compiled_followed_active_ideas, participated_in_ideas_hash].each do |hash|
        if hash.is_a?(Hash) && hash.any?
          count += hash.keys.size
        end
      end      

      # get new posts and comments from ideas from each set of ideas
      {
        user_ideas_activity: compiled_users_active_ideas,
        user_followed_ideas_activity: compiled_followed_active_ideas,
        user_participated_in_activity: participated_in_ideas_hash,
        count: count
      }
    end

    private
    def get_active_owner_ideas
      with_new_yesses   = Idea.all(:owner_id => @user.id).all(:yeses => {:created_at.gt => @user.last_notified_on})
      with_new_posts    = Idea.all(:owner_id => @user.id).all(:posts => {:created_at.gt => @user.last_notified_on})
      with_new_comments = Idea.all(:owner_id => @user.id).all(:posts => {:comments => {:created_at.gt => @user.last_notified_on}})

      with_new_yesses | with_new_posts | with_new_comments
    end

    def get_active_followed_ideas
      followed_ids = @user.followed_ideas.collect { |follow| follow.idea_id }

      with_new_posts    = Idea.all(:id => followed_ids).all(:posts => {:created_at.gt => @user.last_notified_on})
      with_new_comments = Idea.all(:id => followed_ids).all(:posts => {:comments => {:created_at.gt => @user.last_notified_on}})

      with_new_posts | with_new_comments
    end

    def get_partipated_in_ideas(*ideas)      
      ignore_ids = ideas.sum.collect { |idea| idea.id }

      # ideas not owned or followed, with posts by the user that has new comments (ie. replies)
      with_posts_by_user = Idea.all(:id.not => ignore_ids).all(:posts => {:user_id => @user.id, :comments => {:created_at.gt => @user.last_notified_on}}).to_a

      # ideas not owned or followed, with posts by a different user, that this user has commented on and has new comments (ie. replies)
      with_comments_by_user = Idea.all(:id.not => ignore_ids).all(:posts => 
        {:user_id.not => @user.id, :comments => 
          {:user_id => @user.id, :created_at.gt => @user.last_notified_on}
        }
      )

      with_posts_by_user | with_comments_by_user
    end

    def compile_ideas_hash(ideas, options={}, &block)
      idea_activity_hash = {}

      ideas.each do |idea|
        # -- set up the data structure
        idea_activity_hash[idea] = {
          :new_yes_count => nil, # optional, since only the users own idea hash needs it
          :posts => [], # new posts since last notification
          :comments => [] # new comments since last notification
        }

        # aliased for easy reference
        new_yes_count = idea_activity_hash[idea][:new_yes_count]
        posts = idea_activity_hash[idea][:posts]
        comments = idea_activity_hash[idea][:comments]

        # count yesses if needed
        if options[:include_yes_count]
          idea_activity_hash[idea][:new_yes_count] = idea.yeses.select { |yes| yes.created_at > @user.last_notified_on }.count
        end

        idea_posts = block.nil? ? idea.posts : (idea.posts.select &block)
        idea_posts.each do |post|
          # -- find new posts (always push if custom filtering was applied via &block)
          if post.created_at > @user.last_notified_on
            posts.push(post)
          end

          # -- find new comments
          post.comments.each do |comment|
            if comment.created_at > @user.last_notified_on
              comments.push(comment)
            end
          end
        end

        # sort the posts in order of post type
        def order_of(post)
          type = post.is_prototype_or_video? ? "prototype" : post.type
          @@post_type_sort_order[type]
        end

        idea_activity_hash[idea][:posts].sort! do |a,b|
          order_of(a) <=> order_of(b)
        end
      end

      return idea_activity_hash
    end
  end
end