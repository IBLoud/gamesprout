Idea.raise_on_save_failure = true

def report_error(item, error)
  puts "\n"
  puts item.class.to_s + ": "

  ap item.attributes

  raise error
end

User.each do |user|
  if follows_hash = @remembered_follows[user.id]
    follows_hash.each do |fh|
      follow = Follow.new
      follow.user = user
      follow.idea = Idea.first(moped_id: fh[:idea_id])

      follow.save
    end
  end
end