module CustomSanitizeConfig
  USER = {
    :elements => ['a', 'img', 'strong', 'em', 'p', 'br', 'ul', 'ol', 'li', 'i', 'b'],
    :attributes => {
      'a' => ['href', 'title'],
      'img' => ['src', 'alt', 'width', 'height']
    },
    :protocols => {
      'a' => {'href' => ['http', 'https', :relative]},
      'img' => {'src'  => ['http', 'https']}
      },
    :add_attributes => {
      'a' => {'rel' => 'nofollow'}
    }
  }

  COMMENT = {
    :elements => ['a', 'br'],
    :attributes => {
      'a' => ['href', 'title']
    },
    :protocols => {
      'a' => {'href' => ['http', 'https', :relative]}
      },
    :add_attributes => {
      'a' => {'rel' => 'nofollow'}
    }
  }
end