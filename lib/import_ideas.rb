# raise error on save for any model
DataMapper::Model.raise_on_save_failure = true

def report_error(item, error, more={})
  puts "\n"
  puts item.class.to_s + ": "

  ap item.attributes
  
  puts "\n"  
  puts "Valid? " + item.valid?.to_s # must call before it knows whether there are error or not
  puts "Errors:"
  if item.errors.any?
    ap item.errors.to_h
  end

  unless Rails.env.production?
    puts "Hey, did you forget to turn off validations on posts??"
    binding.pry
  end

  raise error
end

def create_details(idea, idea_hash)
  details = IdeaDetails.new

  if details_hash = idea_hash[:details]
    details.title = details_hash[:title]
    details.pitch = details_hash[:pitch]
    details.platform = details_hash[:platform]
    details.mechanics = details_hash[:mechanics]
    details.aesthetics = details_hash[:aesthetics]
    details.favorite = details_hash[:favorite]
    
    details.banner_image = details_hash[:banner_image]["url"] if details_hash[:banner_image]

    if art_collection = details_hash[:art_collection]
      if image_links = art_collection["image_links"]
        image_links.each do |image_link_hash|
          image = ImageLink.new
          image.url = image_link_hash[:url]
          image.description = image_link_hash[:description]
          
          details.image_links << image
        end
      end
    end
  end  
  
  details.idea_id = idea.id

  begin
    raise "Details for Idea##{idea.id} failed to save!" unless details.save
  rescue Exception => e
    report_error(details, e)
  end
end

def create_comments(item, post)
  if comments = item[:comments]
    comments.each do |ch|
      comment = Comment.new
      comment.text = ch[:text]
      comment.removed = ch[:removed]
      comment.user_id = ch[:user_ip]

      comment.removed_by = User.first(moped_id: ch[:removed_by_id]) if ch[:removed_by_id]
      comment.user = User.first(moped_id: ch[:user_id])

      comment.created_at = ch[:created_at]
      comment.updated_at = ch[:updated_at]

      comment.post = post
      
      begin
        comment.save!
      rescue Exception => e
        report_error(comment, e)
      end
    end
  end
end

def create_idea(h)
  i = Idea.new
  i.moped_id = h[:_id].to_s

  # -- associate with an existing user(s)
  i.owner = User.first(moped_id: h[:owner_id].to_s)

  if h.has_key?(:submitter_id) && h[:submitter_id]
    i.submitter = User.first(moped_id: h[:submitter_id].to_s)
  end

  if deleted_by_id = h[:deleted_by_id]
    i.deleted_by = User.first(moped_id: deleted_by_id)
  end

  # if deleted_at = h[:deleted_at]
  #   i.deleted_at = h[:deleted_at]
  #   i.deleted = true
  # end

  i.created_at = h[:created_at]
  i.updated_at = h[:updated_at]

  begin
    i.save!
  rescue Exception => e
    report_error(i, e)
  end

  # -- load idea details
  create_details(i, h)

  # -- load tags
  if tags_array = h[:tags_array]
    tags_array.each do |tag_name|
      tag = Tag.first_or_create(name: tag_name.downcase)
      i.tags << tag
    end

    i.save
  end

  # -- load design documents
  if additional_docs = h[:additional_docs]
    additional_docs.each do |doc_h|
      doc = DesignDoc.new
      doc.title = doc_h[:title]
      doc.body = doc_h[:body]

      doc.idea = i

      begin
        doc.save
      rescue Exception => e
        report_error(doc, e)
      end
    end
  end

  # -- load posts of each type
  [:discussions, :polls, :art_collections, :prototypes].each do |post_type|
    if items = h[post_type]
      items.each do |item|
        post = Post.new

        # copy attributes
        [:title, :text, :total_votes, :summary, :description, :url, :source_url, :image, :instructions].each do |attribute|
          attr_equals = "#{attribute.to_s}=".to_sym
          post.send attr_equals, item[attribute]
        end

        # keep the moped id around
        post.moped_id = item[:_id].to_s

        # keep the removed status and remover information
        post.removed = item[:removed] || false
        post.removed_by = User.first(moped_id: item[:removed_by_id]) if post.removed?

        # find the poster
        poster = User.first(moped_id: item[:user_id].to_s)
        post.user = poster

        # set the type of the post        
        if item[:_type]
          post.type = item[:_type].underscore
        else
          post.type = post_type.to_s.singularize
        end

        post.created_at = item[:created_at]
        post.updated_at = item[:updated_at]
        
        i.posts << post
        
        begin
          post.save!
        rescue Exception => e
          report_error(post, e)
        end

        if post.is_poll?
          # load poll options
          item[:poll_options].each do |poll_options_hash|
            poll_option = PollOption.new
            poll_option.text = poll_options_hash[:text]
            poll_option.vote_count = poll_options_hash[:total_votes]
            poll_option.post = post

            begin
              poll_option.save
            rescue Exception => e
              report_error(poll_option, e)
            end

            if poll_options_hash[:poll_votes]
              poll_options_hash[:poll_votes].each do |poll_vote_hash|
                vote = PollVote.new
                vote.created_at = poll_vote_hash[:created_at]
                vote.user = User.first(moped_id: poll_vote_hash[:user_id])
                vote.poll_option = poll_option

                begin
                  vote.save!
                rescue Exception => e
                  report_error(vote, e)
                end
              end
            end
          end
        end
        
        # image links (if any)
        if image_links = item[:image_links]
          image_links.each do |ilh|
            image_link = ImageLink.new
            image_link.url = ilh[:url]
            image_link.description = ilh[:description]

            post.image_links << image_link
            
            begin
              image_link.save!
            rescue Exception => e
              report_error(image_link, e)
            end
          end
        end

        # comments
        create_comments(item, post)

        # count comments
        post.comment_count = post.comments.count
      end
    end
  end

  # -- remove any invalid posts
  Post.all.select { |p| not p.valid? }.each(&:destroy)

  i.recount_activity!

  # -- featured demo
  if featured_demo_moped_id = h[:details][:featured_demo]
    unless (featured_demo_moped_id.nil? || featured_demo_moped_id.empty?)
      demo = i.posts.first(moped_id: featured_demo_moped_id)
      demo.featured_demo = true
      
      begin
        demo.save
      rescue Exception => e
        report_error(demo, e)
      end
    end
  end

  # -- demo gallery
  demo_gallery = h[:details][:demo_gallery]
  if demo_gallery && !demo_gallery.empty?
    demo_gallery.select { |x| not x.nil? }.reverse.each do |demo_moped_id|
      demo = i.posts.prototypes.first(moped_id: demo_moped_id)
      demo.in_gallery = true
      demo.save
    end
  end

  # -- save yes instances
  if yeses = h[:ratings]
    yeses.each do |yes_hash|
      if yes_hash[:up] # yeses were ratings in the old system, and could be Boolean up -- only up => true count as yeses
        yes = Yes.new        
        yes.created_at = yes_hash[:created_at]
        yes.user = User.first(moped_id: yes_hash[:user_id])
        yes.idea = i

        begin
          yes.save!
        rescue Exception => e
          report_error(yes, e)
        end
      end
    end
  end

  # -- cache count of yeses
  i.yes_count = i.yeses.count  

  # -- load goals
  if goals = h[:goals]
    goals.each do |goal_hash|
      next if goal_hash[:deleted]

      goal = Goal.new
            
      goal.summary = goal_hash[:summary]
      goal.details = goal_hash[:details]
      goal.what_kind = goal_hash[:what_kind]

      goal.created_at = goal_hash[:created_at]
      goal.updated_at = goal_hash[:updated_at]

      goal.idea = i

      begin
        goal.save!
      rescue Exception => e
        report_error(goal, e, :from => goal_hash)
      end
    end
  end

  # -- load tasks checklist
  i.tasks = h[:tasks]

  # -- reset the updated_at date again since we may have saved without skipping methods (save!) before this
  # WARNING: Do not save again after this!
  i.updated_at = h[:updated_at]
  begin
    i.save! # bang it so that updated_at is maintained
  rescue Exception => e
    report_error(i, e)
  end

end

proc = Proc.new {
  ideas = YAML.load_file("#{Rails.root}/tmp/ideas.yaml")
  ideas.each do |hash|
    create_idea(hash)
  end
}

true if result = proc.call