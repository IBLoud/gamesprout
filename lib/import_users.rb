# TODO: need to save created_at/updated_at and save! (which wasn't saving before, so need to figure out why)

@remembered_follows = {}

def report_error(item, error, more={})
  puts "\n"
  puts item.class.to_s + ": "

  ap item.attributes
  
  puts "\n"  
  puts "Valid? " + item.valid?.to_s # must call before it knows whether there are error or not
  puts "Errors:"
  if item.errors.any?
    ap item.errors.to_h
  end

  unless Rails.production
    binding.pry
  end

  raise error
end

def create_user(h)
  u = User.new
  u.username = h[:username]
  u.email = h[:email]  
  
  # need to fill first two in to prevent triggering validations, but won't matter because we set encrypted_password
  u.password = "password"
  u.password_confirmation = "password"
  u.encrypted_password = h[:encrypted_password]

  u.admin = h[:admin]
  u.is_jesse_schell = h[:is_jesse_schell]
  u.moped_id = h[:_id].to_s

  # -- Get tracking info
  u.sign_in_count = h[:sign_in_count]
  u.current_sign_in_at = h[:current_sign_in_at]
  u.last_sign_in_at = h[:last_sign_in_at]
  u.current_sign_in_ip = h[:current_sign_in_ip]
  u.last_sign_in_ip = h[:last_sign_in_ip]
  u.visit_count = h[:visit_count]
  u.last_visit_at = h[:last_visit_at] unless h[:last_visit_at].nil?
  u.last_visit_ip = h[:last_visit_ip] unless h[:last_visit_ip].nil?

  u.last_notified_on = h[:last_notified_on]
  u.notify = h[:notification_settings]["daily"] unless h[:notification_settings].nil?

  # facebook
  u.provider = h[:provider]
  u.uid = h[:uid]

  # -- Get user details
  d = UserDetails.new
  dh = h[:user_details]
  dh.keys.each do |key|
    method = "#{key}=".to_sym
    d.send method, dh[key] if d.respond_to?(method)
  end
  u.user_details = d

  # this (user has no email) should never have happened, but it did for one user due to some fluke in the Facebook sign up process
  # so we generate them an email
  if u.email.nil? || u.email.empty?
    u.email = "generated_email_#{SecureRandom.uuid}@example.com"
  end

  # same goes for missing usernames
  if u.username.nil? || u.username.empty?
    u.username = "user_#{SecureRandom.uuid}"
  end

  # still have to accept terms of service!
  u.terms = "1"

  # -- Save user
  begin
    u.save
  rescue Exception => e
    report_error(u, e)
  end

  # -- save user's follows in a hash that can be accessed once ideas have been imported
  if follows = h[:follows]
    follows.each do |follow_hash|
      @remembered_follows[u.id] ||= [] 
      @remembered_follows[u.id] << follow_hash
    end
  end
end

proc = Proc.new {
  users = YAML.load_file "#{Rails.root}/tmp/users.yaml"
  users.each do |hash|
    create_user(hash)
  end
}

true if result = proc.call