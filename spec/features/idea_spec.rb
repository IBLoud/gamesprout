require 'spec_helper'

feature 'Idea' do
  given(:user) {
    FactoryGirl.create(:user)
  }

  given(:admin) {
    FactoryGirl.create(:admin)
  }

  def idea
    Idea.find(@idea_id)
  end

  scenario 'can be submitted by user' do
    sign_in(:user)

    visit new_idea_path

    title = Faker::Lorem.words(Random.rand(1..4)).join(' ')
    pitch = Faker::Lorem.sentences(2).join(' ')

    fill_in "idea_details_title", with: title
    fill_in "pitch", with: pitch

    click_on "Continue to Design Document"

    page.should have_content title
  end

  # -- Design page editing
  scenario 'banner image can be edited' do
    user = sign_in(:user)
    idea = create_new_idea(user)
    new_image = "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s

    visit design_document_idea_path(idea)
    click_on 'edit-banner-image'
    within '#edit-banner-image-form' do |form|
      fill_in 'url', with: new_image
      click_on 'top-submit'
    end

    page.find("#billboard").should have_css("img[src*=placehold]")
  end

  scenario 'gallery image can be added then deleted' do
    user = sign_in(:user)
    idea = create_new_idea(user)

    visit design_document_idea_path(idea)

    gallery_id = '#gallery' + idea.id.to_s
    image = "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
    within gallery_id do
      fill_in 'url', with: image
      click_on 'top-submit'
    end

    first('.gallery').should have_css("img[src*=placehold]")

    first('.rm-gallery-image').click

    first('.gallery').should_not have_css("img[src*=placehold]")
  end

  scenario 'demo can be added to demo gallery, then removed' do
    user = sign_in(:user)
    idea = create_new_idea(user)

    title = Faker::Lorem.words(3..6).join(' ')
    image = "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
    visit idea_path(idea)

    click_on "Submit a Demo"
    within '#demo-tab-links' do
      click_on "Video"
    end
    within '#form-2' do |form|
      fill_in "post_title", with: title
      fill_in "post_description", with: Faker::Lorem.sentences(3..4).join(' ')
      fill_in "post_image", with: image
      fill_in "post_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_source_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      
      click_on "Create Video Demo"
    end

    should_have_no_error_alert
    page.should have_content title

    visit design_document_idea_path(idea)

    demo_id = "#demo" + idea.id.to_s
    within demo_id do
      select title, :from => "demo"
      click_on 'bottom-submit'
    end

    first('.demos').should have_css("img[src*=placehold]")

    first('.rm-gallery-demo').click

    first('.demos').should_not have_css("img[src*=placehold]")
  end

  # -- Discussion
  def create_discussion_by(user)
    idea = create_new_idea

    discussion = Faker::Lorem.sentences(2).join(' ')
    visit idea_path(idea)

    click_on "Discuss Idea"
    within first('#new_post') do |form|
      fill_in "post_text", with: discussion
      click_on "Create Discussion"
    end

    should_have_no_error_alert
    page.should have_content discussion

    return page
  end

  scenario 'can have a discussion added to it if you are logged on' do
    user = sign_in(:user)
    create_discussion_by(user)
  end

  scenario 'can have a discussion added to it by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_discussion_by(user)
    page.should have_content "Staff"
  end

  # -- Poll
  def create_poll_by(user)
    idea = create_new_idea

    poll = Faker::Lorem.words(5..10).join(' ')
    visit idea_path(idea)

    find(:xpath, "(//a[text()='Create Poll'])").click
    within all('#new_post')[1] do |form|
      fill_in "post_title", with: poll
      (rand(2..4)).times do |i|
        option_name = "post_poll_options_text" + i.to_s
        fill_in option_name, with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      end
      click_on "Create Poll"
    end

    should_have_no_error_alert
    page.should have_content poll

    return page
  end

  scenario 'can have a poll added to it if you are logged on' do
    user = sign_in(:user)
    create_poll_by(user)
  end

  scenario 'can have a poll added to it by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_poll_by(user)
    page.should have_content "Staff"
  end

  # -- Art Collection
  def create_art_collection_by(user)
    idea = create_new_idea

    art_summary = Faker::Lorem.sentences(3..4).join(' ')
    visit idea_path(idea)

    click_on "Submit Art"
    within all('#new_post')[2] do |form|
      (rand(2..4)).times do |i|
        url = "post_image_links_image_link_" + i.to_s
        fill_in url, with: Faker::Lorem.words(2..4).join(' ')
      end
      fill_in "post_summary", with: art_summary
      
      click_on "Create Art Collection"
    end

    should_have_no_error_alert
    page.should have_content art_summary

    return page
  end

  scenario 'can have an art collection added to it if you are logged on' do
    user = sign_in(:user)
    create_art_collection_by(user)
  end

  scenario 'can have an art collection added by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_art_collection_by(user)
    page.should have_content 'Staff'
  end

  # -- Browser Playable Demo
  def create_web_playable_demo_by(user)
    idea = create_new_idea

    title = Faker::Lorem.words(3..4).join(' ')
    visit idea_path(idea)

    click_on "Submit a Demo"
    within '#demo-tab-links' do
      click_on "Web Playable Game"
    end
    within '#form-1' do |form|
      fill_in "post_title", with: title
      fill_in "post_description", with: Faker::Lorem.sentences(3..4).join(' ')
      fill_in "post_image", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_source_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_instructions", with: Faker::Lorem.sentences(3..6).join(' ')
      
      click_on "Create Browser Playable Demo"
    end

    should_have_no_error_alert
    page.should have_content title

    return page
  end

  scenario 'can have a web-playable demo added to it if you are logged on' do
    user = sign_in(:user)
    create_web_playable_demo_by(user)
  end

  scenario 'can have a web-playable demo added to it by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_web_playable_demo_by(user)
    page.should have_content 'Staff'
  end
  
  # -- Video
  def create_video_demo_by(user)
    idea = create_new_idea

    title = Faker::Lorem.words(3..4).join(' ')
    visit idea_path(idea)

    click_on "Submit a Demo"
    within '#demo-tab-links' do
      click_on "Video"
    end
    within '#form-2' do |form|
      fill_in "post_title", with: title
      fill_in "post_description", with: Faker::Lorem.sentences(3..4).join(' ')
      fill_in "post_image", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_source_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      
      click_on "Create Video Demo"
    end

    should_have_no_error_alert
    page.should have_content title

    return page
  end

  scenario 'can have a video demo added to it if you are logged on' do
    user = sign_in(:user)
    create_video_demo_by(user)
  end

  scenario 'can have a video demo added to it by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_video_demo_by(user)
    page.should have_content "Staff"
  end

  # -- Downloadable Demo
  def create_downloadable_demo_by(user)    
    idea = create_new_idea

    title = Faker::Lorem.words(3..6).join(' ')
    visit idea_path(idea)

    click_on "Submit a Demo"
    within '#demo-tab-links' do
      click_on "Download"
    end
    within '#form-3' do |form|
      fill_in "post_title", with: title
      fill_in "post_description", with: Faker::Lorem.sentences(3..4).join(' ')
      fill_in "post_image", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_source_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_instructions", with: Faker::Lorem.sentences(3..4).join(' ')
      
      click_on "Create Downloadable Demo"
    end

    should_have_no_error_alert
    page.should have_content title

    return page
  end

  scenario 'can have a downloadable demo added to it if you are logged on' do
    user = sign_in(:user)
    create_downloadable_demo_by(user)
  end

  scenario 'can have a downloadable demo added to by staff and will be marked as such' do
    user = sign_in(:admin)
    page = create_downloadable_demo_by(user)
    page.should have_content 'Staff'
  end

  # -- Yessing and following
  scenario 'can be yessed by a non-owner member and then no-ed' do
    user = sign_in(:user)
    idea = create_new_idea(user)
    sign_out
    user = sign_in(:user)

    visit idea_path(idea)

    page.should have_content "Would you play this game?"
    first('.survey').click_on "Yes"

    page.should have_content "You've already said you would play this idea!"

    first('.survey').click_on "I changed my mind"

    page.should have_content "Would you play this game?"
  end

  scenario 'can be followed by a non-owner member and then unfollowed' do
    user = sign_in(:user)
    idea = create_new_idea(user)
    sign_out
    user = sign_in(:user)

    visit idea_path(idea)

    page.should have_content "Follow"
    find("#btn_follow").click

    page.should have_content "Unfollow"

    find("#btn_follow").click

    page.should have_content "Follow"
  end

  scenario 'can have a featured demo added and then removed by an idea owner' do
    user = sign_in(:user)
    idea = create_new_idea(user)

    title = Faker::Lorem.words(3..6).join(' ')
    image = "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
    visit idea_path(idea)

    click_on "Submit a Demo"
    within '#demo-tab-links' do
      click_on "Video"
    end
    within '#form-2' do |form|
      fill_in "post_title", with: title
      fill_in "post_description", with: Faker::Lorem.sentences(3..6).join(' ')[0..270]
      fill_in "post_image", with: image
      fill_in "post_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      fill_in "post_source_url", with: "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s
      
      click_on "Create Video Demo"
    end

    should_have_no_error_alert
    page.should have_content title

    visit design_document_idea_path(idea)

    featured_id = "#featured" + idea.id.to_s
    within featured_id do
      select title, :from => "demo"
      click_on 'Feature Demo'
    end

    first('.featured').should have_css("img[src*=placehold]")

    within featured_id do
      click_on 'Remove Featured Demo'
    end

    first('.featured').should_not have_css("img[src*=placehold]")
  end

  scenario 'can have its design doc fields edited by the idea owner' do
    user = sign_in(:user)
    idea = create_new_idea(user)
    visit design_document_idea_path(idea)

    plat = Faker::Lorem.words(3..6).join(' ')
    mech = Faker::Lorem.sentences(2..4).join(' ')
    aesth = Faker::Lorem.sentences(2..4).join(' ')
    fav = Faker::Lorem.sentences(2..4).join(' ')

    within "#design-details" do
      fill_in "platform", with: plat
      find('.btn-save-platform').click

      fill_in "mechanics", with: mech
      find('.btn-save-mechanics').click

      fill_in "aesthetics", with: aesth
      first('.btn-save-aesthetics').click
      
      fill_in "favorite", with: fav
      first('.btn-save-favorite').click
    end

    page.should have_content plat
    page.should have_content mech
    page.should have_content aesth
    page.should have_content fav
  end

  scenario 'post can be edited by the original poster' do
    discussion = create_discussion_for_idea
    idea = discussion.idea
    user = discussion.user

    sign_in(user)
    visit idea_path(idea)

    id_tag = '#' + discussion.id.to_s
    post_edit = discussion.text + "(edited)"
    within id_tag do
      click_on "edit"
    end

    fill_in "post_text", with: post_edit
    click_on "Update Discussion"

    page.should have_content post_edit 
  end

  scenario 'post can be deleted by the original poster' do
    discussion = create_discussion_for_idea
    idea = discussion.idea
    user = discussion.user

    sign_in(user)
    visit idea_path(idea)

    id_tag = '#' + discussion.id.to_s
    post_text = discussion.text
    within id_tag do
      click_on "remove"
    end

    visit idea_path(idea)

    page.should_not have_content post_text 
  end

  scenario 'post can be edited by an admin' do
    discussion = create_discussion_for_idea
    idea = discussion.idea
    user = discussion.user

    sign_in(:admin)
    visit idea_path(idea)

    id_tag = '#' + discussion.id.to_s
    post_edit = discussion.text + "(edited)"
    within id_tag do
      click_on "edit"
    end

    fill_in "post_text", with: post_edit
    click_on "Update Discussion"

    page.should have_content post_edit 
  end

  scenario 'post can be deleted by an admin' do
    discussion = create_discussion_for_idea
    idea = discussion.idea
    user = discussion.user

    sign_in(:admin)
    visit idea_path(idea)

    id_tag = '#' + discussion.id.to_s
    post_text = discussion.text
    within id_tag do
      click_on "remove"
    end

    visit idea_path(idea)

    page.should_not have_content post_text 
  end

  scenario 'post can be deleted by the idea owner' do
    discussion = create_discussion_for_idea
    idea = discussion.idea
    user = idea.owner

    sign_in(user)
    visit idea_path(idea)

    id_tag = '#' + discussion.id.to_s
    post_text = discussion.text
    within id_tag do
      click_on "remove"
    end

    visit idea_path(idea)

    page.should_not have_content post_text 
  end

  scenario 'can be deleted by the idea owner' do
    user = sign_in(:user)
    idea = create_new_idea(user)

    visit idea_path(idea)

    within "#billboard" do
      click_on "Delete Idea"
    end
    first('.alert-success').should have_content "Idea successfully deleted. If this was a mistake, please contact GameSprout support: gs-support@schellgames.com"

  end

end