FactoryGirl.define do
  factory :base_user, class: User do
    terms '1'

    factory :user, class: User do
      username  { Faker::Internet.user_name }
      email     { Faker::Internet.email }

      password = Faker::Lorem.characters(10)
      password_confirmation password
      password              password

      factory :admin, class: User do
        admin true
      end
    end

    after(:create) do |user|
      user.save
    end
  end  
end