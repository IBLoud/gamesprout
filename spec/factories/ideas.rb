FactoryGirl.define do
  factory :initial_idea_details, class: IdeaDetails do    
    title                       { Faker::Lorem.words(Random.rand(1..3)).join(' ') }
    pitch                       { Faker::Lorem.sentences(Random.rand(1..2)).join(' ') }
  end

  factory :full_idea_details, class: IdeaDetails do    
    platform                       { Faker::Lorem.words(Random.rand(1..4)).join(' ') }
    mechanics                      { Faker::Lorem.sentences(Random.rand(1..4)).join(' ') }
    aesthetics                     { Faker::Lorem.sentences(Random.rand(1..4)).join(' ') }
    favorite                       { Faker::Lorem.sentences(Random.rand(1..4)).join(' ') }
  end

  factory :design_details, class: DesignDoc do
    title                       { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    body                        { Faker::Lorem.paragraphs(Random.rand(1..4)).join(' ') }
  end

  factory :tags, class: Idea do
    tags_array                  { [Faker::Lorem.words(Random.rand(1..1)).join(' '),Faker::Lorem.words(Random.rand(1..1)).join(' '),Faker::Lorem.words(Random.rand(1..1)).join(' ')] }
  end

  factory :new_idea, class: Idea do    
  end  
end