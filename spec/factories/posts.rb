
FactoryGirl.define do
  factory :discussion, class: Post do    
    text                       { Faker::Lorem.paragraphs(Random.rand(1..2)).join(' ') }
    type                       { "discussion" }
  end

  factory :poll, class: Post do
    title                      { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    poll_options               { [FactoryGirl.build(:poll_option), FactoryGirl.build(:poll_option), FactoryGirl.build(:poll_option)] }
    type                       { "poll" }
  end

  factory :poll_option, class: PollOption do
    text                       { Faker::Lorem.words(Random.rand(1..3)).join(' ') }
  end

  factory :browser_prototype, class: Post do
    title                      { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    description                { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    url                        { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    source_url                 { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    image                      { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    instructions               { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    type                       { "browser_playable_prototype" }
  end

  factory :downloadable_prototype, class: Post do
    title                      { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    description                { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    url                        { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    source_url                 { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    image                      { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    instructions               { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    type                       { "downloadable_prototype" }
  end

  factory :video_prototype, class: Post do
    title                      { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    description                { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    url                        { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    source_url                 { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    image                      { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    type                       { "video_prototype" }
  end

  factory :art_collection, class: Post do
    summary                    { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
    image_links                { [FactoryGirl.build(:image_link), FactoryGirl.build(:image_link), FactoryGirl.build(:image_link)] }
    type                       { "art_collection" }
  end

  factory :image_link, class: ImageLink do
    heading                    { Faker::Lorem.words(Random.rand(3..4)).join(' ') }
    url                        { "placehold.it/" + rand(150..350).to_s + "x" + rand(150..350).to_s }
    description                { Faker::Lorem.sentences(Random.rand(3..4)).join(' ') }
  end

end