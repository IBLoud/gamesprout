module SpecHelperExtension
  #! Put methods that you might want to use anywhere here, kind of like application helper
  def should_have_no_error_alert
    page.should have_no_selector('.alert-error')
  end

  def signed_in?(user)
    page.should have_no_selector('.alert-error')
    page.should have_selector('.alert-success', text: "Signed in successfully.")
    page.should have_content(user.username)
  end

  def sign_in(user)
    user = FactoryGirl.create(user) if user.is_a?(Symbol)

    visit root_path

    within first('#new_user') do |form|
      fill_in 'user_login', with: user.username
      fill_in 'user_password', with: user.password
      click_button 'Sign In'
    end

    signed_in?(user)

    return user
  end

  def sign_out
    click_on 'Sign Out'
  end

  def sign_out_js
    first(".drop-trigger").click
    click_on 'Sign Out'
  end

  def create_new_idea(user=nil)
    user = user || FactoryGirl.create(:user)

    idea = FactoryGirl.build(:new_idea)
    idea.details = FactoryGirl.build(:initial_idea_details)
    idea.owner = idea.submitter = user
    idea.save
    idea
  end

  def create_new_idea_with_tags(user=nil)
    user = user || FactoryGirl.create(:user)

    idea = FactoryGirl.build(:tags)
    idea.details = FactoryGirl.build(:initial_idea_details)
    idea.owner = idea.submitter = user
    idea.save
    idea
  end

  def create_idea_with_design_details(user=nil)
    idea = create_new_idea(user)
    idea.additional_docs = []
    5.times do
      idea.additional_docs << FactoryGirl.build(:design_details)
    end
    idea.save
    idea
  end

  def create_discussion_for_idea(idea=nil)
    idea = idea || create_new_idea
    discussion = FactoryGirl.build(:discussion)
    discussion.user = FactoryGirl.create(:user)
    discussion.idea = idea
    discussion.save
    
    discussion
  end

  def create_poll_for_idea(idea=nil)
    idea = idea || create_new_idea
    poll = FactoryGirl.build(:poll)
    poll.user = FactoryGirl.create(:user)
    poll.idea = idea
    poll.save
    
    poll
  end

  def create_browser_prototype_for_idea(idea=nil)
    idea = idea || create_new_idea
    browser_prototype = FactoryGirl.build(:browser_prototype)
    browser_prototype.user = FactoryGirl.create(:user)
    browser_prototype.idea = idea
    browser_prototype.save
    
    browser_prototype
  end

  def create_downloadable_prototype_for_idea(idea=nil)
    idea = idea || create_new_idea
    downloadable_prototype = FactoryGirl.build(:downloadable_prototype)
    downloadable_prototype.user = FactoryGirl.create(:user)
    downloadable_prototype.idea = idea
    downloadable_prototype.save

    downloadable_prototype
  end

  def create_video_prototype_for_idea(idea=nil)
    idea = idea || create_new_idea
    video_prototype = FactoryGirl.build(:video_prototype)
    video_prototype.user = FactoryGirl.create(:user)
    video_prototype.idea = idea
    video_prototype.save

    video_prototype
  end

  def create_art_collection_for_idea(idea=nil)
    idea = idea || create_new_idea
    art_collection = FactoryGirl.build(:art_collection)
    art_collection.user = FactoryGirl.create(:user)
    art_collection.idea = idea
    art_collection.save
    
    art_collection
  end

  def create_comment_for_commentable(commentable, user=nil)
    user = user || FactoryGirl.create(:user)
    if commentable.nil? || !(commentable.is_a?(Commentable))
      return nil
    end
    comment = FactoryGirl.build(:comment)
    comment.user = user
    commentable.comments << comment
    comment
  end
end