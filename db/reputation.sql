-- Comments by you on other people's posts
select count(*) from comments
inner join posts on comments.post_id = posts.id
where comments.user_id = ?
and posts.user_id != ?

-- Discussion posts by you on other people's ideas
select count(*) from posts
inner join ideas on posts.idea_id = ideas.id
where posts.type = 'discussion'
and posts.user_id = ?
and ideas.owner_id != ?

-- Polls by you on other people's ideas
select count(*) from posts
inner join ideas on posts.idea_id = ideas.id
where posts.type = 'poll'
and posts.user_id = ?
and ideas.owner_id != ?

-- Art Posts by you on other people's ideas
select count(*) from posts
inner join ideas on posts.idea_id = ideas.id
where posts.type = 'art_collection'
and posts.user_id = ?
and ideas.owner_id != ?

-- Videos posts by you on other people's ideas
select count(*) from posts
inner join ideas on posts.idea_id = ideas.id
where posts.type = 'video_prototype'
and posts.user_id = ?
and ideas.owner_id != ?

-- Playable prototypes posted by you on other people's ideas
select count(*) from posts
inner join ideas on posts.idea_id = ideas.id
where (posts.type = 'browser_playable_prototype' OR posts.type = 'downloadable_prototype')
and posts.user_id = ?
and ideas.owner_id != ?

-- Comments by OTHERS on posts by YOU
select count(*) from comments -- comments
inner join posts on comments.post_id = posts.id
where comments.user_id != ? -- by others
and posts.user_id = ? -- on posts by you

-- Thanks given TO YOU [ie. comments that are thanks not by you on posts by you]
select count(*) from comments
inner join posts on comments.post_id = posts.id
where comments.is_thanks IS TRUE
and comments.user_id != ?
and posts.user_id = ?

-- Thanks you GAVE to OTHERS [ie. comments by you, that are thanks, on posts that are not by you]
select count(*) from comments
inner join posts on comments.post_id = posts.id
where comments.is_thanks IS TRUE
and comments.user_id = ?
and posts.user_id != ?


-- Trying to put it all together (this approach would require using imperative calls as a crutch)
select 
	comments_by_user_count.count as comments_by_user_count,
	discussions_by_user_count.count as discussions_by_user_count,
	polls_by_user_count.count as polls_by_user_count

from
	(-- Comments by you on other people's posts
	select count(*) from comments
	inner join posts on comments.post_id = posts.id
	where comments.user_id = 9
	and posts.user_id != 9) as comments_by_user_count

join
	(-- Discussion posts by you on other people's ideas
	select count(*) from posts
	full join ideas on posts.idea_id = ideas.id
	where posts.type = 'discussion'
	and posts.user_id = 9
	and ideas.owner_id != 9) as discussions_by_user_count
on 1=1

join
	(-- Polls by you on other people's ideas
	select count(*) from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'poll'
	and posts.user_id = 9
	and ideas.owner_id != 9) as polls_by_user_count
on 1=1

-- NON-IMPERATIVE APPROACHES
select 	
	users.id as user_id,
	comments_tmp.*
from users

inner join 
	(-- Comments by you on other people's posts
	select count(*) as comments_by_user_count, comments.user_id as commentor_id from comments
	inner join posts on comments.post_id = posts.id
	group by comments.user_id) as comments_tmp
on users.id = comments_tmp.commentor_id

-----


-- Comments by you on other people's posts
create TEMP table comments as
select 
	count(*) as comments_by_user_count, 
	comments.user_id as commentor_id,
	posts.user_id as posts_user_id
from comments
inner join posts 
on comments.post_id = posts.id

-----

select distinct on(user_id) users.username, rep_data.*, count(*) OVER (PARTITION BY user_id) as count from users

inner join
(
	select 	
		users.id as user_id,
		comments_tmp.*
	from users

	inner join 
		(-- Comments by you on other people's posts
		select 
			comments.user_id as commentor_id,
			posts.user_id as posts_user_id
		from comments
		inner join posts on comments.post_id = posts.id
		group by comments.user_id, posts.user_id) 
		as comments_tmp
	on users.id = comments_tmp.commentor_id
) as rep_data
on users.id = rep_data.user_id

where user_id != posts_user_id
order by user_id

-----

-- Simplified the above version
select distinct on(users.id) users.id as user_id, count(*) OVER (PARTITION BY users.id) as comments_count from users

inner join
	(-- Comments by you on other people's posts
	select 
		comments.user_id as commentor_id,
		posts.user_id as posts_user_id
	from comments
	inner join posts on comments.post_id = posts.id
	group by comments.user_id, posts.user_id) 
	as comments_tmp
on users.id = comments_tmp.commentor_id

where users.id != posts_user_id
order by users.id


-----

select comments.id as comment_id, posts.id as post_id, ideas.id as idea_id from comments
inner join posts on comments.post_id = posts.id
inner join ideas on posts.idea_id = idea_id
where comments.user_id = 9
and posts.user_id != 9

-----

select count(*)
from
	(select 
	distinct on(comments.id) 
		comments.id as comment_id, 
		posts.id as post_id, 
		ideas.id as idea_id 
	from comments

	inner join posts on comments.post_id = posts.id
	inner join ideas on posts.idea_id = idea_id

	where comments.user_id = 9
	and posts.user_id != 9
	and posts.removed_by_id is NULL
	and ideas.deleted_by_id is NULL
	) as comments


-----

-- FIXED THE ISSUE

select distinct on(users.id) users.id as user_id, count(*) OVER (PARTITION BY users.id) as comments_count from users

inner join
	(-- Comments by you on other people's posts
	select 
		comments.user_id as commentor_id,
		posts.user_id as posts_user_id
	from comments
	inner join posts on comments.post_id = posts.id) 
	as comments_tmp
on users.id = comments_tmp.commentor_id

where users.id != posts_user_id
order by users.id

-- EXPANDING IT TO MORE DATA ASPECTS USING TEMP TABLES

drop table comments_by_user_count;
drop table discussions_by_user_count;

create TEMP table comments_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as comments_count 

from users

inner join
	(-- Comments by you on other people's posts
	select 
		comments.user_id as commentor_id,
		posts.user_id as posts_user_id
	from comments
	inner join posts on comments.post_id = posts.id) 
	as comments_tmp
on users.id = comments_tmp.commentor_id

where users.id != posts_user_id
order by users.id);

--

create TEMP table discussions_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as discussions_count 

from users

inner join
	(-- Discussion posts by you on other people's ideas
	select 
		posts.user_id as discussions_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'discussion'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as discussions_tmp

on users.id = discussions_tmp.discussions_user_id

where users.id = discussions_tmp.discussions_user_id
and users.id != discussions_tmp.ideas_user_id);

--



--

select * from comments_by_user_count
inner join discussions_by_user_count
on comments_by_user_count.user_id = discussions_by_user_count.user_id;

--


------ FINISHED

DISCARD TEMP;

--
-- Comments by you on other people's posts
create TEMP table comments_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as comments_count 

from users

inner join
	(
	select 
		comments.user_id as commentor_id,
		posts.user_id as posts_user_id
	from comments
	inner join posts on comments.post_id = posts.id) 
	as comments_tmp
on users.id = comments_tmp.commentor_id

where users.id != posts_user_id
order by users.id);

--

create TEMP table discussions_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as discussions_count 

from users

inner join
	(-- Discussion posts by you on other people's ideas
	select 
		posts.user_id as discussions_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'discussion'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.discussions_user_id

where users.id = subquery.discussions_user_id
and users.id != subquery.ideas_user_id);

--
-- Polls by you on other people's ideas
create TEMP table polls_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as polls_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'poll'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Art Posts by you on other people's ideas
create TEMP table arts_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as arts_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'art_collection'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Videos posts by you on other people's ideas
create TEMP table videos_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as videos_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'video_prototype'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Playable prototypes posted by you on other people's ideas
create TEMP table demos_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as demos_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where (posts.type = 'browser_playable_prototype' OR posts.type = 'downloadable_prototype')
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Thanks given TO YOU [ie. comments that are thanks not by you on posts by you]
create TEMP table thanks_recieved_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as thanks_recieved 

from users

inner join
	(
	select 
		comments.id as thanks_comment_id,
		comments.user_id as thanks_user_id,
		posts.user_id as post_user_id
	from comments
	inner join posts on comments.post_id = posts.id
	where comments.is_thanks IS TRUE)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.thanks_user_id);

--
-- Now OUTER JOIN all the temp tables together 
-- (outer to make sure that we have a row even when a user has no entry in one or more of the tables)

select 
	comments_by_user_count.user_id, 
	comments_count, 
	discussions_count, 
	polls_count,
	arts_count,
	videos_count,
	demos_count,
	thanks_recieved
	
from comments_by_user_count

full outer join discussions_by_user_count
on comments_by_user_count.user_id = discussions_by_user_count.user_id

full outer join polls_by_user_count
on comments_by_user_count.user_id=polls_by_user_count.user_id

full outer join arts_by_user_count
on comments_by_user_count.user_id=arts_by_user_count.user_id

full outer join videos_by_user_count
on comments_by_user_count.user_id=videos_by_user_count.user_id

full outer join demos_by_user_count
on comments_by_user_count.user_id=demos_by_user_count.user_id

full outer join thanks_recieved_count
on comments_by_user_count.user_id=thanks_recieved_count.user_id

--