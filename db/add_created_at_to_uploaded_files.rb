UploadedFile.all(:created_at => nil).each do |file|
  url = file.url # ex. format: https://s3.amazonaws.com/gamesprout-prod/uploads/demos/post_1977/user_11/2014-01-23-18-35-36-912/EnemyMind.swf

  rx = /user_[0-9]+\//
  created_at_str = url.split(rx)[1].split('/')[0] # this parses, but we lose time info
  date_time_parts = created_at_str.split('-') # from strftime format: "%Y-%m-%d-%k-%M-%S-%L" | [3] => hour (24 hr clock); [4] => minutes; [5] => seconds
  date_time_parts.map!(&:to_i) # convert to int array
  date = DateTime.new(*date_time_parts) # pass array into DateTime init as argument using splat (*) operator

  file.created_at = date
  file.save
end