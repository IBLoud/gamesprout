DISCARD TEMP;

--
-- Comments by you on other people's posts
create TEMP table comments_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as comments_count 

from users

inner join
	(
	select 
		comments.user_id as commentor_id,
		posts.user_id as posts_user_id
	from comments
	inner join posts on comments.post_id = posts.id) 
	as comments_tmp
on users.id = comments_tmp.commentor_id

where users.id != posts_user_id
order by users.id);

--

create TEMP table discussions_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as discussions_count 

from users

inner join
	(-- Discussion posts by you on other people's ideas
	select 
		posts.user_id as discussions_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'discussion'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.discussions_user_id

where users.id = subquery.discussions_user_id
and users.id != subquery.ideas_user_id);

--
-- Polls by you on other people's ideas
create TEMP table polls_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as polls_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'poll'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Art Posts by you on other people's ideas
create TEMP table arts_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as arts_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'art_collection'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Videos posts by you on other people's ideas
create TEMP table videos_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as videos_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where posts.type = 'video_prototype'
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Playable prototypes posted by you on other people's ideas
create TEMP table demos_by_user_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as demos_count 

from users

inner join
	(
	select 
		posts.user_id as post_user_id,
		ideas.owner_id as ideas_user_id
	from posts
	inner join ideas on posts.idea_id = ideas.id
	where (posts.type = 'browser_playable_prototype' OR posts.type = 'downloadable_prototype')
	and posts.removed_by_id is null
	and ideas.deleted_by_id is null)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.ideas_user_id);

--
-- Thanks given TO YOU [ie. comments that are thanks not by you on posts by you]
create TEMP table thanks_recieved_count as
(select distinct on(users.id) 
	users.id as user_id, 
	count(*) OVER (PARTITION BY users.id) as thanks_recieved 

from users

inner join
	(
	select 
		comments.id as thanks_comment_id,
		comments.user_id as thanks_user_id,
		posts.user_id as post_user_id
	from comments
	inner join posts on comments.post_id = posts.id
	where comments.is_thanks IS TRUE)
	as subquery

on users.id = subquery.post_user_id

where users.id = subquery.post_user_id
and users.id != subquery.thanks_user_id);

--
-- Now OUTER JOIN all the temp tables together 
-- (outer to make sure that we have a row even when a user has no entry in one or more of the tables)

select * from
(
select
	users.id as user_id,
	comments_count, 
	discussions_count, 
	polls_count,
	arts_count,
	videos_count,
	demos_count,
	thanks_recieved,
	(
		COALESCE(comments_count,0) +
		COALESCE(discussions_count,0) +
		COALESCE(polls_count,0) +
		COALESCE(arts_count,0) +
		COALESCE(videos_count,0) +
		COALESCE(demos_count,0) +
		COALESCE(thanks_recieved,0)
	) as total

from users

full outer join comments_by_user_count
on users.id = comments_by_user_count.user_id

full outer join discussions_by_user_count
on users.id = discussions_by_user_count.user_id

full outer join polls_by_user_count
on users.id=polls_by_user_count.user_id

full outer join arts_by_user_count
on users.id=arts_by_user_count.user_id

full outer join videos_by_user_count
on users.id=videos_by_user_count.user_id

full outer join demos_by_user_count
on users.id=demos_by_user_count.user_id

full outer join thanks_recieved_count
on users.id=thanks_recieved_count.user_id
) as all_counts
where total != 0

--