class UploadedFile
  include DataMapper::Resource
  property :created_at, DateTime, :required => false  
  
  property :id, Serial

  property :url, Text
  property :filepath, Text
  property :filename, String, :length => 260
  property :filesize, Integer
  property :filetype, String, :length => 255
  property :s3_unique_id, String
  property :description, Text

  property :checked, Boolean, :default => false

  belongs_to :post, :required => false
  belongs_to :user, :required => false

  def initialize(params=nil)
    if (params)
      self.url = URI.unescape params[:url] # note that urls, paths and filenames from Amazon are escaped, so we need to unescape them
      self.filepath = URI.unescape params[:filepath]
      self.filename = URI.unescape params[:filename]
      self.filesize = params[:filesize]
      self.filetype = params[:filetype]
      self.s3_unique_id = params[:unique_id]
      self.description = params[:description]    
    end
  end

end