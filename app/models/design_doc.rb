class DesignDoc
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :title, String, :length => 140, :required => true
  property :body, Text, :required => true

  belongs_to :idea

end