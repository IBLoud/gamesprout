class NewsAlert
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :text, Text, :required => true
  property :stop_at, DateTime, :required => false

  def self.get_alert
    self.all.select { |news_alert| news_alert.stop_at.nil? || (news_alert.stop_at.is_a?(DateTime) && news_alert.stop_at >= Time.zone.now) }.first
  end

  def self.visible?
    not self.get_alert.nil?
  end
end