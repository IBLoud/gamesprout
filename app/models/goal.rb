class Goal
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :summary, String, :length => 500, :required => true
  property :details, Text, :required => true
  property :what_kind, Enum[:art, :demo, :design, :sound], :required => true
  
  belongs_to :idea

  # -- Completion
  property :completed, Boolean, :required => false, :default => false
  property :completed_at, DateTime, :required => false
  belongs_to :completed_by_post, :model => Post, :required => false

  # how to get list of enums for property :what_kind
  # self.what_kind.options[:flags]

  # -- Scopes
  def self.unresolved
    all(:completed_at => nil)
  end


  # returns the enum value the database will use for a given what_kind symbol, useful for custom sql
  def self.what_kind_enum_value_for(sym)
    self.what_kind.options[:flags].index(sym) + 1
  end

  def self.what_kind_options
    self.what_kind.options[:flags]
  end

  def completed?
    not completed_at.nil?
  end
end