class PollVote
  include DataMapper::Resource
  timestamps :created_at

  property :id, Serial

  belongs_to :poll_option
  belongs_to :user
end