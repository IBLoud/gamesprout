class Follow
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :idea_id, Integer
  property :user_id, Integer

  belongs_to :idea
  belongs_to :user

  default_scope(:default).update(:idea => {:deleted => false})

end