class Vote
  include DataMapper::Resource
  
  timestamps :at

  property :id, Serial
  property :ip, String
  
  belongs_to :idea
  belongs_to :user

end
