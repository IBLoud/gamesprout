class UserReputationData
  include DataMapper::Resource

  property :id, Serial

  ##
  ## Counts for various useful data regarding user activity, to be used for calculating reputation.
  ## Currently we're only counting activity done on others' ideas or comments on other people's posts.
  ## Activity on your own ideas or comments on your own posts doesn't count, since it's self-serving (which is okay, but not what we're counting).
  ##
  property :comments_count,          Integer
  property :discussions_count,       Integer
  property :polls_count,             Integer
  property :arts_count,              Integer
  property :videos_count,            Integer
  property :demos_count,             Integer
  property :thanked_count,           Integer

  belongs_to :user

  REPUTATION_MULTIPLIERS = {
    comments_count: 1.0,
    thanked_count: 1.0,
    discussions_count: 2.0,
    polls_count: 2.0,
    arts_count: 6.0,
    videos_count: 8.0,
    demos_count: 10.0
  }

  # * Comments by you on other people's posts
  # * Discussion posts by you on other people's ideas
  # * Polls by you on other people's ideas
  # * Art Posts by you on other people's ideas
  # * Videos posted by you on other people's ideas
  # * Playable prototypes posted by you on other people's ideas

  # Special:

  # * Thanks received
  # * Thanks given (small amount)

  def self.for_user_id(user_id)
    user_rep = first(:user_id => user_id)
      
    # create a new data object if the user doesn't have
    unless user_rep
      user_rep = UserReputationData.new
      user_rep.user_id = user_id
    end

    user_rep
  end

  def self.get_reputation_data
    sql = <<-eos
      DISCARD TEMP;

      --
      -- Comments by you on other people's posts
      create TEMP table comments_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as comments_count 

      from users

      inner join
        (
        select 
          comments.user_id as commentor_id,
          posts.user_id as posts_user_id
        from comments
        inner join posts on comments.post_id = posts.id) 
        as comments_tmp
      on users.id = comments_tmp.commentor_id

      where users.id != posts_user_id
      order by users.id);

      --

      create TEMP table discussions_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as discussions_count 

      from users

      inner join
        (-- Discussion posts by you on other people's ideas
        select 
          posts.user_id as discussions_user_id,
          ideas.owner_id as ideas_user_id
        from posts
        inner join ideas on posts.idea_id = ideas.id
        where posts.type = 'discussion'
        and posts.removed_by_id is null
        and ideas.deleted_by_id is null)
        as subquery

      on users.id = subquery.discussions_user_id

      where users.id = subquery.discussions_user_id
      and users.id != subquery.ideas_user_id);

      --
      -- Polls by you on other people's ideas
      create TEMP table polls_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as polls_count 

      from users

      inner join
        (
        select 
          posts.user_id as post_user_id,
          ideas.owner_id as ideas_user_id
        from posts
        inner join ideas on posts.idea_id = ideas.id
        where posts.type = 'poll'
        and posts.removed_by_id is null
        and ideas.deleted_by_id is null)
        as subquery

      on users.id = subquery.post_user_id

      where users.id = subquery.post_user_id
      and users.id != subquery.ideas_user_id);

      --
      -- Art Posts by you on other people's ideas
      create TEMP table arts_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as arts_count 

      from users

      inner join
        (
        select 
          posts.user_id as post_user_id,
          ideas.owner_id as ideas_user_id
        from posts
        inner join ideas on posts.idea_id = ideas.id
        where posts.type = 'art_collection'
        and posts.removed_by_id is null
        and ideas.deleted_by_id is null)
        as subquery

      on users.id = subquery.post_user_id

      where users.id = subquery.post_user_id
      and users.id != subquery.ideas_user_id);

      --
      -- Videos posts by you on other people's ideas
      create TEMP table videos_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as videos_count 

      from users

      inner join
        (
        select 
          posts.user_id as post_user_id,
          ideas.owner_id as ideas_user_id
        from posts
        inner join ideas on posts.idea_id = ideas.id
        where posts.type = 'video_prototype'
        and posts.removed_by_id is null
        and ideas.deleted_by_id is null)
        as subquery

      on users.id = subquery.post_user_id

      where users.id = subquery.post_user_id
      and users.id != subquery.ideas_user_id);

      --
      -- Playable prototypes posted by you on other people's ideas
      create TEMP table demos_by_user_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as demos_count 

      from users

      inner join
        (
        select 
          posts.user_id as post_user_id,
          ideas.owner_id as ideas_user_id
        from posts
        inner join ideas on posts.idea_id = ideas.id
        where (posts.type = 'browser_playable_prototype' OR posts.type = 'downloadable_prototype')
        and posts.removed_by_id is null
        and ideas.deleted_by_id is null)
        as subquery

      on users.id = subquery.post_user_id

      where users.id = subquery.post_user_id
      and users.id != subquery.ideas_user_id);

      --
      -- Thanks given TO YOU [ie. comments that are thanks not by you on posts by you]
      create TEMP table thanks_received_count as
      (select distinct on(users.id) 
        users.id as user_id, 
        count(*) OVER (PARTITION BY users.id) as thanks_received 

      from users

      inner join
        (
        select 
          comments.id as thanks_comment_id,
          comments.user_id as thanks_user_id,
          posts.user_id as post_user_id
        from comments
        inner join posts on comments.post_id = posts.id
        where comments.is_thanks IS TRUE)
        as subquery

      on users.id = subquery.post_user_id

      where users.id = subquery.post_user_id
      and users.id != subquery.thanks_user_id);

      --
      -- Now OUTER JOIN all the temp tables together 
      -- (outer to make sure that we have a row even when a user has no entry in one or more of the tables)

      select * from
      (
      select
        users.id as user_id,
        comments_count, 
        discussions_count, 
        polls_count,
        arts_count,
        videos_count,
        demos_count,
        thanks_received,
        (
          COALESCE(comments_count,0) +
          COALESCE(discussions_count,0) +
          COALESCE(polls_count,0) +
          COALESCE(arts_count,0) +
          COALESCE(videos_count,0) +
          COALESCE(demos_count,0) +
          COALESCE(thanks_received,0)
        ) as total

      from users

      full outer join comments_by_user_count
      on users.id = comments_by_user_count.user_id

      full outer join discussions_by_user_count
      on users.id = discussions_by_user_count.user_id

      full outer join polls_by_user_count
      on users.id=polls_by_user_count.user_id

      full outer join arts_by_user_count
      on users.id=arts_by_user_count.user_id

      full outer join videos_by_user_count
      on users.id=videos_by_user_count.user_id

      full outer join demos_by_user_count
      on users.id=demos_by_user_count.user_id

      full outer join thanks_received_count
      on users.id=thanks_received_count.user_id
      ) as all_counts
      where total != 0

      --
    eos
    
    if Rails.env.production?
      uri = URI.parse(ENV['DATABASE_URL'])
      conn = PG.connect(uri.hostname, uri.port, nil, nil, uri.path[1..-1], uri.user, uri.password)
    else
      # connect to the db directly, datamapper's adapter.select chokes on this
      config   = Rails.configuration.database_configuration
      host     = config[Rails.env]["host"]
      database = config[Rails.env]["database"]
      username = config[Rails.env]["username"]
      password = config[Rails.env]["password"]      

      conn = PG.connect(dbname: database, host: host, user: username)
    end
    
    result = conn.exec(sql)

    return result.to_a
  end

  def calculated_reputation
    REPUTATION_MULTIPLIERS.inject(0) do |total, kvp|
      sym = kvp[0]
      multiplier = kvp[1]
      value = self.send(sym) || 0

      total + (value * multiplier)
    end
  end

end
