class Idea
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :moped_id, String
  property :yes_count, Integer, :default => 0
  property :activity_count, Integer, :default => 0
  property :tasks, Json, :default => {}
  property :deleted, Boolean, :default => false

  has n, :posts, :constraint => :destroy
  has n, :votes, :constraint => :destroy
  has n, :design_docs, :constraint => :destroy
  has n, :follows, :constraint => :destroy
  has n, :goals, :constraint => :destroy
  has n, :yeses, :model => 'Yes', :constraint => :destroy

  has 1, :details, :model => 'IdeaDetails', :constraint => :destroy

  has_tags_on :tags

  belongs_to :owner, :model => 'User'
  belongs_to :submitter, :model => 'User', :required => false
  belongs_to :deleted_by, :model => 'User', :required => false  

  validates_with_block do
    valid = !self.details.nil? && self.details.valid?

    if valid
      true
    else
      [false, "Idea details must be filled in"]
    end
  end

  # -- Scopes
  def self.active
    all(:order => :activity_count.desc)
  end

  def self.not_deleted
    all(:deleted => false)
  end

  # -- Custom sql work to retrieve ideas based on their goals
  def self.with_help_needed(options={how_many: 10, offset: 0})
    # -- prepare various options for sql statement
    what_kind = options[:what_kind] ? options[:what_kind] : Goal.what_kind.options[:flags]
    what_kind_enum_array = what_kind.map { |sym| Goal.what_kind_enum_value_for(sym) }
    
    case options[:order_by]
    when :latest_goals
      order_by = "goal_created_at"
    when :newly_added
      order_by = "idea_created_at"
    when :popular
      order_by = "yes_count"
    when :active
      order_by = "activity_count"
    end

    # -- set some option parts of the query (changes depending on options)
    if options[:tags]
      # format the tags as a string that psql understands as an array
      formatted_tags_string = options[:tags].map { |tag| "\"#{tag}\"" }.join(',')

      # add a concatenation of tags as part of the main query so we can filter on them
      from = <<-eos
        from (
          select ideas.*, array_agg(tags.name) as tags 
          from ideas

          join taggings on taggings.taggable_id=ideas.id
          join tags on taggings.tag_id=tags.id

          group by ideas.id
        ) as ideas
      eos

      # @> is the psql for contains, eg array1 @> array2 is array1 contains array2 (http://www.postgresql.org/docs/8.3/static/functions-array.html)
      where = <<-eos
        where ideas.tags @> '{#{formatted_tags_string}}'
      eos
    else
      from = "from ideas"
      where = ''
    end

    # -- the actual sql statement
    sql = <<-eos

      select ideas.id as idea_id, ideas.created_at as idea_created_at, goals.id as goal_id, goals.what_kind, goals.created_at as goal_created_at
      #{from}

      inner join goals
      on goals.id = (
        select id from goals

        where idea_id = ideas.id
        and goals.completed_at is NULL -- not completed
        and goals.what_kind = ANY(array#{what_kind_enum_array.to_s}) -- includes only the given types
        
        order by created_at desc
        limit 1
      ) -- select the latest not-deleted, not-complete goal

      #{where}

      order by #{order_by} desc
      limit ?
      offset ?

    eos

    custom_results = repository(:default).adapter.select(sql, options[:how_many], options[:offset])
    idea_ids = custom_results.collect { |c| c.idea_id } # this is ordered correctly

    # retrieving on an array of id's does NOT maintain order, so after retrieval we'll use this hash (looks like { 123=>0 }, as in { idea_id=>index })
    # to reorder the results
    order_hash = Hash[idea_ids.map.with_index.to_a]

    # reorder just the retrieved items
    Idea.all(id: idea_ids).sort do |a, b|
      order_hash[a.id] <=> order_hash[b.id]
    end
  end

  def self.search_query(query)
    # very un-intelligent search using ILIKE (case-insensitive LIKE, a psql extension of LIKE)
    sql = <<-eos
      select ideas.* from ideas
            
      join users 
      on ideas.owner_id = users.id

      join idea_details
      on ideas.id = idea_details.idea_id

      join (select array_agg(name)::text as tag_list, taggings.taggable_id as idea_id
        from tags

        join taggings
        on taggings.tag_id=tags.id

        join posts
        on taggings.taggable_id=posts.id 
        and taggings.taggable_type='Idea'

        group by taggings.taggable_id) as tags
      on tags.idea_id=ideas.id

      WHERE ideas.deleted_by_id IS NULL
      AND 
        (idea_details.title ILIKE ?
        OR users.username ILIKE ?
        OR tags.tag_list ILIKE ?)
    eos

    query = "%#{query}%"
    find_by_sql([sql, query, query, query])
  end

  # -- Instance methods
  def title
    self.details.title
  end

  def yesed_by?(user)
    yeses.all(user_id: user.id).any?
  end

  def featured_demo
    posts.first(featured_demo: true)
  end

  def demo_gallery
    posts.all(type: Post.prototype_type_names, in_gallery: true)
  end

  def recount_activity!
    posts_count = Post.count(:removed => false, :idea_id => self.id)
    comments_count = Comment.count(:removed => false, :"post.idea_id" => self.id)

    self.activity_count = posts_count + comments_count
  end
end