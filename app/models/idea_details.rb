class IdeaDetails
  include DataMapper::Resource

  timestamps :at

  property :id, Serial
  property :title, String, :length => 60, :required => true, :message => "Working title can't be blank"
  property :pitch, String, :length => 140, :required => true, :message => "Game summary can't be blank"
  property :platform, String, :length => 600
  property :mechanics, String, :length => 600
  property :aesthetics, String, :length => 600
  property :favorite, String, :length => 600  
  property :banner_image, Text

  belongs_to :idea, :required => false

  has n, :image_links, :constraint => :destroy

  validates_uniqueness_of :name, :case_sensitive => false
  validates_with_block do
    self.idea && !!(self.idea.new? ? self.idea : self.idea_id)
  end

  def banner_image?
    !(banner_image.nil? || banner_image.empty?)
  end

end