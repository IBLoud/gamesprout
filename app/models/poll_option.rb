class PollOption
  include DataMapper::Resource

  property :id, Serial
  property :text, String, :length => 140, :required => true
  property :vote_count, Integer, :default => 0

  has n, :votes, :model => 'PollVote', :constraint => :destroy

  belongs_to :post

  def vote_percent
    if post.total_votes == 0 # prevent a divide-by-0
      0
    else
      vote_count.to_f/post.total_votes.to_f * 100.0
    end
  end

end
