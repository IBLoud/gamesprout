class User
  include DataMapper::Resource
  timestamps :at

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook]

  ## Database authenticatable
  property :username,           String, :required => true
  property :email,              String, :required => true, :default => "", :length => 255
  property :encrypted_password, String, :required => true, :default => "", :length => 255

  ## Recoverable
  property :reset_password_token,   String
  property :reset_password_sent_at, DateTime

  ## Rememberable
  property :remember_created_at, DateTime

  ## Trackable
  property :sign_in_count,      Integer, :default => 0
  property :current_sign_in_at, DateTime
  property :last_sign_in_at,    DateTime
  property :current_sign_in_ip, String
  property :last_sign_in_ip,    String

  ## Extended Tracking for Visits
  property :visit_count, Integer, :default => 0
  property :last_visit_at, DateTime
  property :last_visit_ip, String
  property :last_viewed_dashboard_at, DateTime

  ## Simple (eg lazy) roles
  property :admin, Boolean, :default => false
  property :is_jesse_schell, Boolean, :default => false

  ## Facebook stuff
  property :provider, String, :default => ""
  property :uid, String, :default => ""
  property :created_by_provider, String, :default => ""

  ## Encryptable
  # property :password_salt, String

  ## Confirmable
  # property :confirmation_token,   String
  # property :confirmed_at,         DateTime
  # property :confirmation_sent_at, DateTime
  # property :unconfirmed_email,    String # Only if using reconfirmable

  ## Lockable
  # property :failed_attempts, Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # property :unlock_token,    String # Only if unlock strategy is :email or :both
  # property :locked_at,       DateTime

  ## Token authenticatable
  # property :authentication_token, String, :length => 255

  ## Invitable
  # property :invitation_token, String, :length => 255

  # virtual attribute to set what devise will use to log a user in
  attr_accessor :login, :terms

  property :id, Serial
  property :moped_id, String
  property :parental_signature, String, :length => 255
  property :notify, Boolean, :default => true
  property :last_notified_on, DateTime, :default => lambda { |r, p| DateTime.now }

  has n, :owned_ideas, :model => 'Idea', :child_key => [:owner_id]
  has n, :submitted_ideas, :model => 'Idea', :child_key => [:submitter_id]
  has n, :followed_ideas, :model => 'Follow'
  has n, :selections
  has n, :yeses, :model => 'Yes'
  has n, :mentions, :model => 'Mention', :child_key => [:mentioned_user_id]
  has n, :files, :model => 'UploadedFile'
  has n, :comments
  has n, :posts

  has 1, :user_details, :model => 'UserDetails', :constraint => :destroy
  has 1, :reputation_data, :model => 'UserReputationData', :constraint => :destroy

  validates_acceptance_of :terms, :allow_nil => false, :if => :new?
  validates_with_method :validate_allowable_characters
  
  def validate_allowable_characters
    if self.username.count("\"") > 0
      [false, "Doube-quote (\") characters are not allowed in usernames."]
    else
      true
    end
  end

  # function to handle user's login via email or username
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    # login = login.downcase unless login.nil?

    if login
      # all(conditions).where('$or' => [ {:username => /^#{Regexp.escape(login)}$/i}, {:email => /^#{Regexp.escape(login)}$/i} ]).first
      conditions_result = all(conditions)
      result = conditions_result.all(:username => login) | conditions_result.all(:email => login)
      result.first
    else
      all(conditions).first
    end
  end

  def self.find_by_id_or_username(id_or_username)
    if id_or_username.to_i > 0
      user = User.first(id: id_or_username)
    else
      user = User.first(username: id_or_username)
    end

    if user.nil?
      raise "Unable to find user using id or username #{id_or_username}"
    end

    return user
  end

  #function to get facebook credentials
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    if signed_in_resource.nil?
      user = User.first(:provider => auth.provider, :uid => auth.uid)
      unless user
        user = User.first(:email => auth.info.email)
      end

      if user
        user.provider = auth.provider
        user.uid = auth.uid
        user.save
      else
         #check email or die
        email = auth.info.email
        unless (email =~ Devise::email_regexp)
          return nil
        end
        if auth.extra.raw_info.name.nil? || auth.extra.raw_info.name.empty?
          username = "Sprouter" + rand(1000..9999).to_s
        else
          username = auth.extra.raw_info.name
        end
        while User.count(:username => username) > 0
          username = auth.extra.raw_info.name + rand(1000..9999).to_s
        end
        user = User.new( username:username,
                         provider:auth.provider,
                         uid:auth.uid,
                         email:auth.info.email,
                         password:Devise.friendly_token[0,20]
                         )
        user.user_details = UserDetails.new
        user.user_details.avatar = auth.info.image
        user.terms = '1'
        user.save
      end
    else
      user = signed_in_resource
      user.provider = auth.provider
      user.uid = auth.uid
      user.save
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
        user.uid = data["id"]
        user.provider = "facebook"
      end
    end
  end

  def self.date_format
    user_details.date_format
  end

  def notify?(sym)
    settings = self.notification_settings || NotificationSettings.new
    settings.send(sym)
  end

  def adminify!
    self.admin = true
    self.save!
  end

  def following_user?(user)
    !follows.where(:user_id => user.id).empty?
  end

  def committed_to_ideas
    Idea.where('goals.commitments.user_id' => self.id)
  end

  def avatar
    (user_details.nil? || user_details.avatar.strip.empty?) ? nil : user_details.avatar
  end

  def avatar?
    !self.avatar.nil?
  end

  def viewed?(obj, options={})
    view_date = options[:view_date] || self.view_date
    obj_is_viewed = view_date >= obj.updated_at
  end

  def selected?(group, item)
    selection = self.selections.first(group: group, user_id: self.id)
    selection && selection.value == item
  end

  def no_selection?(group)
    selection = self.selections.first(group: group, user_id: self.id)
    selection.nil? || selection.value.nil? || selection.value.empty?
  end

  def view_date
    last_viewed_dashboard_at || last_visit_at || last_sign_in_at || created_at
  end

  def reputation
    UserReputationData.for_user_id(self.id).calculated_reputation.to_i
  end

end