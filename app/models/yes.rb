class Yes
  include DataMapper::Resource
  timestamps :created_at

  property :id, Serial

  belongs_to :idea
  belongs_to :user
end