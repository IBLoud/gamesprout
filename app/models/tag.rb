class Tag
  # monkey-patch for the Tag class created by dm-tags
  def self.popular_tags(limit=9)
    sql = <<-eos
      select tags.name, taggings.count
      from tags
      inner join
        (
          select tag_id, count(*) as count
          from taggings
          group by tag_id
        ) taggings
      on  tags.id = taggings.tag_id
      order by taggings.count desc
      limit #{limit};
    eos

    return repository(:default).adapter.select(sql)
  end

  def self.popular_tag_names(limit=9)
    self.popular_tags(limit).collect { |t| t.name }
  end
end