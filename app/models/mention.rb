class Mention
  include DataMapper::Resource
  timestamps :created_at

  COMPLEX_RGX = /\@".+?"/ # matches @"anything in quotes after an @ sign"
  SIMPLE_RGX = /\@[A-Za-z0-9_-]+/ # matches any alphanumeric, underscore, dash after an @ sign, eg. @michailv99-dash_underscore

  property :id, Serial
  property :mention_string, String
  property :mentioned_at, DateTime

  belongs_to :mentioned_user, :model => 'User'
  belongs_to :comment, :required => false
  belongs_to :news_item, :required => false
  belongs_to :post, :required => false

  validates_with_method :validate_has_model?

  def self.check_for_mentions(model)
    text = ""

    if model.is_a?(Comment)
      text = model.text
    elsif model.is_a?(NewsItem)
      text = model.content
    elsif model.is_a?(Post)
      text = model.text_for_mention
    else
      return
    end
    
    [COMPLEX_RGX, SIMPLE_RGX].each do |regex|
      text.scan(regex).each do |match|
        username = match[1..-1].gsub("\"", "") # remove the @ and quotes from the username
        if (user = User.first(:username => username)) && model.mentions.all(:mention_string => match).empty?
          self.add_mention(match, model, user)
        end
      end
    end
  end

  def self.update_mentions(model)
    model.mentions.destroy # this is pretty lazy, but also should be very error-free. Eventually the increased number of writes may matter.
    self.check_for_mentions(model)
  end

  def mentioned_in_model
    self.comment || self.news_item || self.post
  end

  def mentioned_by
    mentioned_in_model.user
  end

  private
  def self.add_mention(match, model, mentioned_user)
    mention = Mention.new
    mention.mention_string = match    
    mention.mentioned_user = mentioned_user

    if model.is_a?(Comment)
      mention.mentioned_at = model.created_at
      mention.comment = model
    
    elsif model.is_a?(Post)
      mention.mentioned_at = model.created_at
      mention.post = model
    
    elsif model.is_a?(NewsItem)
      mention.mentioned_at = model.pub_date
      mention.news_item = model
    end
    
    mention.save
  end

  def validate_has_model?
    not mentioned_in_model.nil?
  end
end