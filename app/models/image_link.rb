class ImageLink
  include DataMapper::Resource

  property :id, Serial
  property :url, Text, :required => true
  property :description, Text

  belongs_to :post, :required => false
  belongs_to :idea_details, :required => false

  validates_with_block do
    valid = !!(post_id || idea_details_id)
    if valid
      true
    else
      [false, "ImageLink must have either a post_id or idea_details_id"]
    end
  end

end
