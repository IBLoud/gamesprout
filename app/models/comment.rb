class Comment
  include DataMapper::Resource

  timestamps :at

  property :id, Serial  

  property :text, Text, :required => true
  property :removed, Boolean, :default => false
  property :user_ip, String  
  property :is_thanks, Boolean, :default => false

  belongs_to :removed_by, :model => 'User', :required => false
  belongs_to :post
  belongs_to :user

  has n, :mentions, :constraint => :destroy  
  
  def contains_thanks?
    # starts with thank (thanks, thank you), nice, sweet, cool, great, wow, +1. If starts with such, very or much those words are ignored. Such +1! Very thanks.
    thanks_rgx = /^\s*(such|very|much)?\s*(thank|nice|sweet|cool|great|wow|awesome|\+1)/i
    !!(thanks_rgx.match text)
  end

end
