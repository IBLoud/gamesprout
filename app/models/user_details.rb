class UserDetails
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :full_name, String, :default => "", :length => 150
  property :bio, String, :length => 600
  property :location, String, :length => 150
  property :twitter_handle, String, :length => 150
  property :public_email, String, :length => 150
  property :date_format, String, :default => "%m/%d/%y"
  property :avatar, Text, :default => "/assets/default-avatar.png"

  belongs_to :user

  validates_length_of :full_name, maximum: 150
  validates_length_of :bio, maximum: 600
  validates_length_of :location, maximum: 150
  validates_length_of :twitter_handle, maximum: 150
  validates_length_of :public_email, maximum: 150
end