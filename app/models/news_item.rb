class NewsItem
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :title, String, :required => true, :length => 140
  property :summary, String, :required => true, :length => 400
  property :content, Text, :required => true
  property :pub_date, DateTime, :required => true

  belongs_to :posted_by, model: "User"
  belongs_to :edited_by, model: "User", :required => false

  has n, :mentions, :constraint => :destroy

  def self.published
    all(:pub_date.lt => Time.zone.now, :order => :pub_date.desc)
  end

  def self.latest
    all(:order => :pub_date.desc)
  end

  def publish_now=(bool_string)
    case bool_string
    when "1", "yes", "true", "t"
      @publish_now = true
    else
      @publish_now = false
    end
  end

  def publish_now
    @publish_now || false
  end

  def publish_now?
    publish_now
  end

  def edited?
    not edited_by.nil?
  end

  def future_dated?
    pub_date > Time.zone.now
  end
end