class Feedback
  include DataMapper::Resource
  timestamps :at

  property :id, Serial
  property :type, String, :required => true
  property :email, String
  property :content, String, :required => true, :length => 1000
  property :logged, Boolean, :default => false
  property :notes, String, :default => '', :length => 1000

  belongs_to :user, :required => false

  # -- class methods
  def self.types
    [:bug_report, :feedback, :help_request, :other]
  end

  def self.types_with_names
    self.types.collect { |t| [t.to_s.humanize.titleize, t] }
  end

  # -- scopes
  def self.visible
    all(:logged => false)
  end

  def self.newest_created
    all(:order => :created_at.desc)
  end

  def self.oldest_created
    all(:order => :created_at.asc)
  end

  def self.logged
    all(:logged => true).oldest_created()
  end

  
  # -- instance methods
  def shortened_content
    if content.size > 100
      content[0..100] + "..."
    else
      content
    end
  end

  def user?
    !user.nil?
  end
end