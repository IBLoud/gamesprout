class Post
  include DataMapper::Resource
  
  timestamps :at

  property :id, Serial
  property :type, String, :required => true
  property :moped_id, String

  property :title, String, :length => 140
  property :text, Text
  property :total_votes, Integer, :default => 0
  property :comment_count, Integer, :default => 0
  property :summary, Text
  property :removed, Boolean, :default => false
  property :in_gallery, Boolean, :default => false
  property :featured_demo, Boolean, :default => false  

  # -- prototype fields
  property :description, Text
  property :url, Text
  property :source_url, Text
  property :image, Text
  property :instructions, Text
  property :will_upload_files, Boolean, :default => false
  property :main_uploaded_file_id, Integer, :required => false

  belongs_to :user
  belongs_to :idea
  belongs_to :removed_by, :model => 'User', :required => false

  has n, :mentions, :constraint => :destroy
  has n, :image_links, :constraint => :destroy
  has n, :comments, :constraint => :destroy
  has n, :poll_options, :constraint => :destroy
  has 1, :uploaded_file, :constraint => :destroy

  default_scope(:default).update(:order => [:created_at.desc], :idea => {:deleted => false})

  # Comment out when importing data
  validates_presence_of :title, :if => lambda { |post| post.is_poll? || post.is_prototype_or_video? }
  validates_presence_of :text, :if => lambda { |post| post.is_discussion? }
  validates_presence_of :description, :if => lambda { |post| post.is_prototype_or_video? }    
  validates_with_method :validate_has_polls, :if => lambda { |post| post.is_poll? }
  validates_with_method :validate_has_images, :if => lambda { |post| post.is_art_collection? }

  # videos are required to have a url; prototypes (web playable or downloadable) must have a url or have files uploaded to Amazon S3
  validates_presence_of :url, :if => lambda { |post| post.is_video? }
  validates_with_method :validate_has_files, :if => lambda { |post| post.is_prototype? } # check if files will be uploaded or that a url is present

  def self.new_art_collection(user, idea_id)
    art_collection = self.new(type: 'art_collection', user: user, idea_id:  idea_id)
    5.times { art_collection.image_links << ImageLink.new }

    art_collection
  end

  def self.prototype_type_names
    %w{browser_playable_prototype downloadable_prototype video_prototype}
  end

  # -- Scopes
  def self.not_removed
    all(:removed_by_id => nil)
  end

  def self.posts_made_or_commented_on_by(user)
    Post.all(:user_id => user.id) | Post.all(Post.comments.user_id => user.id)
  end

  def self.prototypes
    all(:type => Post.prototype_type_names)
  end

  # -- Custom retrieval of posts sorted by latest mention; returns posts and mentions together in a custom hash that includes the post, mention and relevant timestamp
  def self.order_by_latest_mention_for_user(user_id, page=1, how_many=10)
    sql = <<-eos
      select 
        comments.post_id as post_id, 
        max(mentioned_at) as mentioned_at, 
        max(mentions.id) as mention_id, 
        max(mentions.mentioned_user_id) as user_id
      
      from (
        select * 
        from mentions 
        where mentioned_user_id = ?
      ) as mentions
      
      join comments on mentions.comment_id = comments.id
      join posts on post_id = posts.id
      
      group by post_id
      
      order by mentioned_at desc
      limit ?
      offset ?
    eos

    offset = (page - 1) * how_many
    results = repository(:default).adapter.select(sql, user_id, how_many, offset)

    # let's retrieve all of the posts (remember, a call like this DOES NOT MAINTAIN ORDER)
    post_ids = results.collect { |r| r.post_id }
    posts = Post.all(:id => post_ids)

    # similarly, let's get the mentions
    mention_ids = results.collect { |r| r.mention_id }
    mentions = Mention.all(:id => mention_ids)

    # now mash them all together
    results.map do |struct|
      post = posts.first { |p| p.id == struct.post_id }
      mention = mentions.first { |m| m.id == struct.mention_id }

      {
        mentioned_at: struct.mentioned_at,
        post: post,
        mention: mention
      }
    end
  end

  # -- Custom validations
  def validate_has_polls
    if self.poll_options.size > 1
      true
    else
      [false, "Must have at least two poll options"]
    end
  end

  def validate_has_images
    if self.image_links.size > 0
      true
    else
      [false, "Must have at least one image"]
    end
  end

  def validate_has_files
    if !self.will_upload_files || self.url
      true
    else
      [false, "Must provide a url or select that you will upload files."]
    end
  end

  # -- Instance methods
  def verb
    case self.type
    when "browser_playable_prototype"
      'play'
    when "downloadable_prototype"
      'download'
    when "video_prototype"
      'watch'
    else
      '** unknown type on Post#verb **'
    end 
  end

  def has_comments_by?(user)
    self.comments.any? { |comment| comment.attributes['user_id'] == user.id }
  end

  def get_posts_made_or_commented_on_by(user)
    Post.all(:user_id => user.id) | Post.all(Post.comments.user_id => user.id)
  end

  # easy type booleans
  def is_discussion?
    type == 'discussion'
  end

  def is_poll?
    type == 'poll'
  end

  def is_art_collection?
    type == 'art_collection'
  end

  def is_prototype?
    type == 'browser_playable_prototype' || type == 'downloadable_prototype'
  end

  def is_video?
    type == 'video_prototype'
  end

  def is_prototype_or_video?
    type.include? 'prototype'
  end

  def is_browser_playable?
    type == 'browser_playable_prototype'
  end

  def is_downloadable?
    type == 'downloadable_prototype'
  end

  # -- Poll methods
  def user_has_voted?(user)
    poll_options.each do |option|
      option.votes.each do |vote|
        if vote.user.id.eql? user.id
          return true
        end
      end
    end
    return false
  end

  def count_total_votes!
    tv = 0
    poll_options.each do |option|
      tv += option.vote_count
    end
    self.total_votes = tv
  end

  def has_any_votes?
    poll_options.select { |option| option.vote_count > 0 }.any?
  end

  def file_url
    if self.uploaded_file
      if self.uploaded_file.filename.ends_with?(".unity3d")
        "/unity_web_wrapper.html?url=#{self.uploaded_file.url}"
      else
        self.uploaded_file.url
      end
    else
      nil
    end
  end

  # -- Prototype methods
  def model_name
    self.class.to_s.underscore
  end

  # -- Mention helper
  def text_for_mention
    if is_discussion?
      self.text
    elsif is_art_collection?
      self.summary
    elsif is_prototype_or_video?
      self.description
    else
      ""
    end
  end

end