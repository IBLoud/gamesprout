class Selection
  include DataMapper::Resource

  property :id, Serial
  property :group, String
  belongs_to :user

  property :value, String
end