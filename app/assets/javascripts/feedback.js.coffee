# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
	$('#feedback-show, #feedback-index').find('a[data-feedback-action]').click ->
		actionToTake = $(this).data('feedback-action')
		actionUrl = $(this).prev('.action_url').val()		
		item = $(this)
		
		index_page = item.parents('#feedback-items').size() > 0
		item.parents('.feedback-item').fadeOut() if index_page

		$.post actionUrl, { action_to_take: actionToTake }, (r) ->
			window.location.reload() if (r && r.success) unless index_page

