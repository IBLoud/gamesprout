# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
	# -- Bookmarking
	bookmark_links = '.bookmark a'
	$('body').on 'click', bookmark_links, (e) ->
		if $(this).data('method') == 'delete'
			unless confirm 'Are you sure you want to remove this bookmark?'
				e.preventDefault()
				return false			

		$(this).next('.ajax-loader').show()

	$('body').on 'ajax:success', bookmark_links, (e, result) ->
		$(this).next('.ajax-loader').hide()

		if result and result.successful
			new_change_text = $(this).text()
			$(this).text $(this).data('change-text')
			$(this).data('change-text', new_change_text)
			
			$(this).attr 'href', result.new_href

			method = $(this).data 'method'
			new_method = null

			if method == 'delete'
				new_method = 'post'			
				$(this).prev('i').removeClass('icon-thumbs-down').addClass('icon-thumbs-up')
				$('div.above-button-text').text("Want to follow this idea? Bookmark it!")

			else
				new_method = 'delete'
				$(this).prev('i').removeClass('icon-thumbs-up').addClass('icon-thumbs-down')
				$('div.above-button-text').text("You've bookmarked this idea!")

			$(this).data 'method', new_method