$ ->
	$("a[href^=http]").each ->
		if this.href.indexOf('http') != -1 && this.href.indexOf(location.hostname) == -1 && $(this).data('no-blank') != true
			$(this).attr {
				target: "_blank"
			}

	# -- expand/collapse
	# * Data attributes:
	# * data-expander-id => unique id for the content TO BE EXPANDED
	# * data-expander-target-id => put on the clickable element and provide a data-expander-id value (ie. what will be expanded on click)
	# * data-expander-transition-speed => int, optional; time in milliseconds for the animation to take, default 400
	# * data-callback => function; a callback function bound to data eg. $object.data('callback', function(){})
	window.setExpandCollapse = (html) ->
		html ||= $('body')
		html.find('[data-expander-id]').hide()

		html.find('[data-expander-target-id]').each (i, expandCollapse) ->
			expandCollapse = $(expandCollapse)
			expandCollapse.data 'expander-status', 'collapsed'
			
			expandCollapse.text expandCollapse.data('collapsed-text')
			collapsedText = expandCollapse.text()

			expandCollapse.click (e) ->
				e.preventDefault()
				
				status = $(this).data('expander-status')
				target = $ "[data-expander-id=" + expandCollapse.data('expander-target-id') + "]"

				expandedText = $(this).data('expanded-text')
				expandedClass = $(this).data('expanded-class')
				transition = $(this).data('expander-transition')
				transitionSpeed = $(this).data('expander-transition-speed') || 400

				if status == 'expanded'
					$(this).data('expander-status', 'collapsed').text($(this).data('collapsed-text'))
					
					if transition == 'slide'
						target.slideUp(transitionSpeed)
					else
						target.hide()

					expandCollapse.text(collapsedText) if expandedText
				else
					$(this).data('expander-status', 'expanded').text($(this).data('expanded-text'))
					
					if transition == 'slide'
						target.slideDown(transitionSpeed)
					else
						target.show()

					expandCollapse.text(expandedText) if expandedText

				# -- toggle expanded class if one is set
				expandCollapse.toggleClass(expandedClass) if expandedClass

				# -- fire callback method if one is named
				callback = $(this).data 'callback'
				if (typeof callback) == 'function'
					callback.call(this)
	window.setExpandCollapse()

	# -- open dialog
	# * Data attributes:
	# * data-open-dialog-id => unique id given to an element that should be opened on click
	# * data-dialog-id => unique id given to an element that should only be shown via dialog (note: is not automatically hidden at this time, need to manually display: none)
	$('[data-open-dialog-id]').click (e) ->
		e.preventDefault()
		
		dialogId = $(this).data('open-dialog-id')
		$('[data-dialog-id=' + dialogId + ']').dialog()

	# -- apply datepicker to any .datepicker
	$('.datepicker').datepicker({dateFormat: 'yy-mm-dd', minDate: new Date})
	$('.datetimepicker').datetimepicker({
		dateFormat: 'yy-mm-dd', 
		minDate: new Date,
		timeFormat: 'hh:mm tt'
		})

	# -- Character count as you type
	$('input[type=text][data-limit-id], textarea[data-limit-id]').each ->		
		limitId = $(this).data 'limit-id'
		$limitDisplay = $('.charlimit[data-limit-for-id=' + limitId + ']').children('.remaining')
		
		# remember the max count as determined by the defaul display on this element
		$limitDisplay.data 'maxChars', parseInt($limitDisplay.text())
		
		# remember the appropriate display element for this so we don't have to search for it with every keypress
		$(this).data 'limit-display', $limitDisplay
	.keyup (e) ->
		$limitDisplay = $(this).data 'limit-display'
		maxChars = $limitDisplay.data 'maxChars'

		fiveChars = '12345'
		length = $(this).val().replace(/\r\n/g, fiveChars).replace(/[\r\n]/g, fiveChars).length # line breaks should count as 5 chars
		
		remainingChars = maxChars - length
		$limitDisplay.text(remainingChars)

		$parent = $limitDisplay.parent('.charlimit')
		if remainingChars in [0..10]
			$parent.addClass('warning').removeClass('at-limit') unless $parent.hasClass('warning')
		else if remainingChars <= -1
			$parent.addClass('at-limit').removeClass('warning') unless $parent.hasClass('at-limit')
		else
			$parent.removeClass('warning').removeClass('at-limit') if $parent.hasClass('warning') || $parent.hasClass('at-limit')
	.trigger('keypress')

	# -- character resizer
	window.resizeText = ($container) ->
		$container ||= $('body')
		$container.find('[data-resize-down-to]').each ->
			targetHeight = $(this).data('resize-down-to')
			while $(this).height() > targetHeight
				font = $(this).css('fontSize').replace('px', '')
				font = parseInt(font)

				newFont = font - 1
				$(this).css 'fontSize', newFont + "px"
	window.resizeText()

	# -- remember selections system
	$('[data-selection]').click ->
		group_and_item = $(this).data('selection').split(':') # selection_group_id:selected_item_id
		group = group_and_item[0]
		item = group_and_item[1]

		$.post '/save_selection', {group: group, item: item}

	# -- async show more ("infinite scrolling") system (used for: activity feed posts, dashboard items)
	bottomIsVisible = ($elem) ->
		rect = $elem[0].getBoundingClientRect()
		return (rect.bottom - $(window).height()) <= 0

	$async_attrib = $('[data-async-scroll]')
	if $async_attrib.size() > 0
		window.scrollTo(0, 0)		
		loader_elem_name = $async_attrib.data('async-scroll-loader-element')

		nextPage = null
		have_more_pages = null
		loading = false

		window.resetAsyncScroll = ->
			nextPage = 2
			have_more_pages = true
			loading = false
		window.resetAsyncScroll()

		loadMorePosts = ->
			$loader = $(loader_elem_name)
			$loader.show()

			#-- build url for GET call
			url = if window.location.pathname != '/' then window.location.pathname else ''
			url += $async_attrib.data('async-scroll') 
			url += if window.async_scrolling_url_addition then window.async_scrolling_url_addition + '/' else ''
			url += nextPage
			url += $async_attrib.data('async-qstring') || ''

			$.getJSON url, (r) ->
				$loader.hide()
				
				if r.html.trim() == ''
					have_more_pages = false
				else
					newHtml = $(r.html)
					newPosts = $loader.before newHtml

					window.resizeText(newHtml)
					window.createDialogButtons(newHtml)
					window.setExpandCollapse(newHtml)
					window.markTagsAlreadySelected(newHtml)

					nextPage += 1
					loading = false
		
		$(window).scroll ->
			if !window.waitBeforeScrollCheck && have_more_pages && !loading && bottomIsVisible($async_attrib)
				loading = true
				window.waitBeforeScrollCheck = false
				loadMorePosts()

	# -- show more comments
	# ...

	# -- tinymce
	config_options = {}

	default_buttons = "bold,italic,undo,redo,link,unlink,removeformat,cleanup,bullist,numlist"

	base_config = {
		theme: "advanced",
		mode: "none",
		plugins: "paste",
		paste_text_sticky_default: true,
		paste_text_sticky: true,
		theme_advanced_buttons1: default_buttons,
		theme_advanced_buttons2: "",
		theme_advanced_buttons3: "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_path: false,
		entity_encoding: "raw",
		add_unload_trigger: false,
		remove_linebreaks: false,
		inline_styles: false,
		convert_fonts_to_spans: false
	}

	# -- user config
	user_config = $.extend {}, base_config
	config_options.default = user_config

	# -- user config with images
	with_images_config = $.extend {}, base_config
	with_images_config.theme_advanced_buttons1 = default_buttons + ", image"
	config_options.with_images = with_images_config

	# -- admin config
	admin_config = $.extend {}, base_config
	admin_config.theme_advanced_buttons1 = default_buttons + ", forecolor, fullscreen, pastetext, image"
	admin_config.plugins += ',fullscreen'
	config_options.admin = admin_config

	# -- commenting config
	comment_config = $.extend {}, base_config
	comment_config.theme_advanced_buttons1 = "bold,italic,link,unlink"
	config_options.comment = comment_config

	$('.tinymce').each ->
		config = $(this).data('editor-config') || 'default'
		config_obj = config_options[config]

		$(this).tinymce(config_obj)