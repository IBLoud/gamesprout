$ ->
	# -- Tagging
	createTag = (tag) ->
		$tag = $("<li></li>").addClass('badge tag').text(tag)
		$('#tag-display').append($tag)
		
		return $tag

	rebuildList = ->
		tags = []
		$('#tag-display').find('.tag').each -> 
			tags.push $(this).text()
		$('#idea_tags').val tags.join(',')

	# only necessary on the new/edit details page
	if $('#new-idea').size() > 0
		popular_tags = $('#tags').val().split(',')

		$('#idea_tags').val().split(',').forEach (existing_tag) ->
			existing_tag = existing_tag.trim()
			createTag(existing_tag) if existing_tag != ''


		$tagsInterface = $("#idea_tags_interface")
		$tagsInterface.autocomplete({ source: popular_tags })
		$tagsInterface.keypress (e) ->
			if (e.which == 44) || (e.which == 13)
				e.preventDefault()
				e.stopPropagation()

				new_tag = $(this).val()

				# clear the input box
				$(this).val('')

				# make sure the tag doesn't already exist
				if $('#idea_tags').val().toLowerCase().indexOf(new_tag.toLowerCase()) isnt -1
					return false

				if $('#tag-display .tag').size() < 6
					# add tag to visible tag list
					createTag(new_tag)

					# rebuild the actual tag list
					rebuildList()
				else
					alert('An idea can only have 6 tags.')

		# keydown seems to get a more consistent code for ff, chrome
		$tagsInterface.keydown (e) ->
			# create an enter-pressed event
			keydown = $.Event('keypress')
			keydown.which = 13

			if e.which == 9
				e.preventDefault()
				$tagsInterface.trigger(keydown)

		$('#tag-display').on 'click', '.tag', (e) ->
			e.preventDefault()
			
			$(this).remove()
			rebuildList()