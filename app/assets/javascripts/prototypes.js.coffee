# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
	$('.dv-tab1-comment-button').click (e) ->
	   $('#ui-id-2').click()

	# -- remove demo overlay on click
	$('.instruction-container #play-or-download[data-playable=true]').click -> 
		$('.dv-demo-instructions').hide()
		$('.dv-demo-embed').show()