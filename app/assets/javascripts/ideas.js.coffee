# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
	# -- tabbing for update section of ideas#show
	$('.updates-frame').tabs()

	# -- sub-tab for demos participate tab
	$('#demo-tab-links a').click (e) ->
		e.preventDefault()
		$('#demo-tab-links a').removeClass('active')
		$(this).addClass('active')

		which = $(this).data('show-form')
		$('#demo-tab-forms').children().hide()
		$('#demo-tab-forms').children('#' + which).show()

	# -- filtering for ideas#show sidebar
	togglePosts = (e) ->
		hideSpecial = $(this).data('hide-special')
		checked = $(this).is(':checked')

		if hideSpecial
			$(".post[data-special=" + hideSpecial + "]").toggle()
		else
			modelToHide = $(this).data('hide-model')
			$(".post[data-model=" + modelToHide + "]").toggle()

	# -- filtering for ideas#browse_play
	$('#filters-for-browse input[type=radio]').change (e) ->
		window.location = $(this).data('href')

	# -- Browse search
	$('.simplesearch').keypress (e) ->
		if e.which == 13
			query = encodeURIComponent $(this).val()
			window.location.href = $(this).data('url') + '?query=' + query

	# -- idea containter twitter/fb links
	((d, s, id) ->
	  js = undefined
	  fjs = d.getElementsByTagName(s)[0]
	  return  if d.getElementById(id)
	  js = d.createElement(s)
	  js.id = id
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=131897727003298"
	  fjs.parentNode.insertBefore js, fjs
	) document, "script", "facebook-jssdk"
	 
	not ((d, s, id) ->
	  js = undefined
	  fjs = d.getElementsByTagName(s)[0]
	  unless d.getElementById(id)
	    js = d.createElement(s)
	    js.id = id
	    js.src = "//platform.twitter.com/widgets.js"
	    fjs.parentNode.insertBefore js, fjs
	) document, "script", "twitter-wjs"	

	# -- Modal Forms, used mostly on design doc but occasionally elsewhere
	window.createDialogButtons = ($html) ->
		$html ||= $('body')
		
		$('[data-dialog-size]').each (e) ->
			if $(this).data('dialog-size') == ''
				minWidth = 500
				height = 300
			else
				widthByHeight = $(this).data('dialog-size').split('x')
				minWidth = widthByHeight[0]
				height = widthByHeight[1]

			$(this).dialog({
				height: height,
				minWidth: minWidth,
				autoOpen: false,
				modal: true,
				resizable: false
				})

		window.test = $html
		$html.find('[data-dialog-target-id]').click (e) ->
			target = '#' + $(this).data('dialog-target-id')
			$(target).dialog('open')

			e.preventDefault
			return false
	window.createDialogButtons()

	
	# -- check/uncheck idea checklist items
	$('.task-checkbox.owner').click (e) ->
		parent = $(this).parent() # all the data we need is an attribute on parent
		taskInfo = {
			taskId: parent.data('task-id'),
			subgroupName: parent.data('subgroup-name'),
			taskGroupId: parent.data('task-group-id')
		}

		button = $(this)
		tasksCompleted = button.parents('.expanded-task-group').first().prev('.task-group').find('.tasks-completed')

		toggleClasses = () ->
			was_checked = button.hasClass('on')

			button.toggleClass('off')
			button.toggleClass('on')

			totalCompleted = parseInt(tasksCompleted.text())
			
			if was_checked
				tasksCompleted.text(totalCompleted - 1)
			else
				tasksCompleted.text(totalCompleted + 1)

		# assume success, so toggle
		toggleClasses()

		fail = ->
			# toggle back if call fails
				toggleClasses()

		$.post 'checklist/toggle_task', {checklist_task: taskInfo}, (r) -> 
			fail() unless r.success == true
		.fail(fail)

	# -- clicking on tags
	get_query_string_hash = (qstring=null)->
		qhash = {}
		add_param = (kvp) ->
			split = kvp.split('=')
			k = split[0]
			v = split[1]
			qhash[k] = v

		qstring ||= window.location.href.split('?')[1]
		add_param kvp for kvp in qstring.split('&') unless qstring == undefined
		return qhash

	$('body').on 'click', 'a[data-tag]', (e) ->
		e.preventDefault()
		e.stopPropagation()

		tag = $(this).data 'tag'
		qstring = window.location.href.split('?')[1]

		if qstring == undefined || qstring == ''
			url_addition = "?tags=" + tag
		else
			# break the query string up into a hash
			qhash = get_query_string_hash(qstring)

			if qhash['tags']
				if $(this).hasClass('selected')
					# already selected, so we need to REMOVE the tag from the tags list
					remaining_tags = qhash['tags'].split(',').filter (t) -> decodeURIComponent(t).replace(/\+/g, " ") != tag
					qhash['tags'] = remaining_tags.join(',')
				else
					qhash['tags'] = qhash['tags'] + "," + tag
					
				url_addition = decodeURIComponent("?" + $.param(qhash))
			else
				url_addition = "?tags=" + tag

		action = window.location.pathname.split('/')[2]
		if action != undefined
			if action == 'play'
				target_location = 'play'
			else if action == 'help_out'
				target_location = 'help_out'
			else if action == 'more'
				target_location = 'more'
			else
				target_location = 'browse'
		else
			target_location = 'browse'

		window.location = "http://" + window.location.host + "/ideas/" + target_location + url_addition

	# -- mark tags already selected
	window.markTagsAlreadySelected = ($html) ->
		qhash = get_query_string_hash()
		tags = decodeURIComponent(qhash['tags']).replace(/\+/g, ' ')
		$html ||= $('body')

		$html.find('a[data-tag]').each (e) ->		
			tag = $(this).data 'tag'
			if tags && tags.indexOf(tag) != -1
				$(this).addClass('selected')
	
	window.markTagsAlreadySelected()

	$('[data-open-tab]').click (e) ->
		e.preventDefault()
		e.stopPropagation()

		tab = $(this).data('open-tab')
		$(this).parents('.updates-frame').children('ul').find('[href=' + tab + ']').click()

	# -- clicking on the fake radio buttons on the ideas collection page (right side) needs to work
	$('.fake-radio').click ->
		href = $(this).children('a').first().attr('href')
		window.location = 'http://' + window.location.host + "/" + href

	# -- need to prevent user from clicking "save image" multiple times when adding an image to the gallery
	$('[data-add-gallery-image]').click (e) ->
		e.stopPropagation()

		if $(this).data('working')
			e.preventDefault()
		else
			$(this).data('working', true)
			$(this).next('.ajax-loader').show()