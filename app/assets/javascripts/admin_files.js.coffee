$ ->
	$('#known .file').add('#unknown .file').find('input[type=checkbox]').prop('checked', false).click ->
		data = {
			file_type: $(this).data('file-type'),
			id: $(this).data('file-id'),
			key: $(this).data('file-key'),
			url: $(this).data('file-url')
		}

		checkbox = $(this)

		onFail = () ->
			checkbox.prop('checked', false)
			alert 'Something went wrong :('

		onSuccess = (r) ->			
			if (r)
				checkbox.parents('.file').fadeOut()
			else
				fail()

		$.post('/admin/files/mark_checked', data).success(onSuccess).fail(onFail)