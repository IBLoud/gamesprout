# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
	$('#js_file_uploader').S3Uploader().bind 's3_upload_complete', (e, content) ->
		content.description = $('#file_description').val()

		$.post '/files/upload_complete', content, -> location.reload(true)