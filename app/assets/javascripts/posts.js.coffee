###
This file should include any code that relates to a post, ie. discussion, poll, art collection, prototype
Isolated from ideas.js.coffee because posts show up in other pages than just idea pages, eg. homepage activity feed(s)
###

$ ->
	# -- poll voting
	$('#posts, #home-feed').on 'click', '.post .poll .polllist a', (e) ->
		e.preventDefault()
		$chart = $(this).parents('.poll').first()
		vote = $(this).data('poll-vote')
		url = $(this).attr('href')

		$.post url, {"poll_vote": vote}, (result) ->
			if result
				$('div.poll').first().parent().next('.post-controls').children('a.edit').remove()
				$result = $(result.html)
				$chart.replaceWith $result
				window.resizeText $result

	# -- rate anything
	callback = (rating, $this) ->
		return (e) ->                   
			$this.parents('form').first().next('img').hide()
			$this.parents('form').first().show()

			$count = $this.parent().children('.num.pos')
			countAsInt = parseInt $count.text()

			alreadyVoted = $count.data('already-voted') == true
			if alreadyVoted
			       rateValue = 2 
			else
			       rateValue = 1
			       $count.data('already-voted', true) # make sure we mark that the user has voted now

			if rating == 'up'
			       countAsInt = countAsInt + rateValue
			else
			       countAsInt = countAsInt - rateValue

			$count.text countAsInt

	$('#posts, #home-feed').on 'click', '.rating a', (e) ->
		if $(this).hasClass('voted')
			# ignore event if it's user clicked to repeat their vote
			return
		else
			# show the loading gif
			$(this).parents('form').hide()
			$(this).parents('form').first().next('img').show()

			# otherwise switch which button is selected as 'voted'
			$(this).parent().children('.voted').removeClass('voted')
			$(this).addClass('voted')

		rating = $(this).data('rating')
		url = $(this).parent('form').attr('action')
		$.post url, {rating: rating}, callback(rating, $(this)), 'json'

	# -- comment box dynamic expansion
	$('body').on 'keypress', '.comment-container .field textarea', (e) ->
		currentHeight = $(this).prop('clientHeight')
		scrollHeight = $(this).prop('scrollHeight')

		if e.which == 13
			$(this).height(currentHeight + 15) # currentHeight + line-height, which can't always be accurately gotten dynamically
		
		if currentHeight < scrollHeight
			$(this).height(scrollHeight)

	# -- async comment submission
	$('body').on 'click', 'form.new_comment input[type=submit]', (e) ->
		if $(this).data('in-progress') == true || $(this).parents('#new_comment').find('#comment_text').val().trim() == ""
			window.test = $(this)
			e.stopPropagation()
			return false
		else
			$(this).data('in-progress', true).next('.ajax-loader').show()

	$('body').on 'ajax:complete', 'form.new_comment', (e, xhttp) ->
		$(this).find('input[type=submit]').data('in-progress', false)
		$(this).find('.ajax-loader').hide()
		result = JSON.parse xhttp.responseText
		if result
			$(this).before(result.html)
			$(this).find('textarea').val('')

	# -- async show older comments 
	$('body').on 'click', '.viewmore-comments', (e) ->
		e.preventDefault()
		$link = $(this)
		$loader = $link.find('img#show-more-loader')

		# paging data is contained in the link
		pageNum = $link.data('nextPage') || 2

		$loader.show()
		postId = $link.data('post-id')		
		url = '/posts/' + postId + '/more_comments/' + pageNum

		$.getJSON url, (result) ->
			$loader.hide()
			$('#show-more').before(result.html)
			
			if result.html.trim() != ''
				$(result.html).insertAfter $link
				$link.data('nextPage', pageNum + 1)
			
			if result.has_more == false
				$link.remove()

	$("#myS3Uploader").S3Uploader().bind "ajax:success", (e, data) ->
		postId = $(this).data('demo-id')
		window.location.href = "http://" + window.location.host + "/posts/" + postId

	# -- show source_url field when "will upload files" is unchecked, hide when checked
	$('[data-demo-type]').click ->
		type = $(this).data('demo-type')
		$('#demo_url[data-demo-type=' + type + ']').toggle(!this.checked)