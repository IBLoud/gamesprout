# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
	$('[data-expander-target-id=under-18-area]').data 'callback', ->
		$('.landingImg2').toggleClass('moveSketch')

	$('#home-logged-out-submit-button').click (e) ->
		$(this).parents('form').submit()

	$('#newsAlert').click -> 		
		$(this).hide()
	
	# home dashboard feed
	$feed = $('#feed-content')
	if $feed.size() > 0
		# sidebar count loading
		viewDate = $('#lastViewedDate').val()
		$.get('/dashboard/counts_for_navigation/' + viewDate).done (r) ->
			$('#feedButtonbar a[data-feed-url]').each ->
				key = $(this).data('feed-url')
				count = r[key]
				$(this).find('.feedCountIco').text(count) if count > 0


		count_success = (selector) ->
			return (r) ->
				$(selector).find('.feedCountIco').each ->
					ideaId = $(this).data('idea-id')
					count = r[ideaId]
					$(this).text(count) if count > 0
					$(this).css "display", "inline-block"

		$.get('/dashboard/counts_for_owned_ideas/' + viewDate).done count_success('#myIdeas')
		$.get('/dashboard/counts_for_followed_ideas/' + viewDate).done count_success('#following')

		# main content area loading
		window.waitBeforeScrollCheck = true

		$loading = $('#feed-content #loading').clone()
		$heading = $('#feed-header .header-text')

		active_feed_url = ''
		success = (feed_url) ->
			return (result, status, xhr) ->
				if result && result.html && active_feed_url == feed_url
					$feed.html result.html				
					
					# need to append the loading gif for use by the async scrolling system
					$feed.append $loading.clone().hide()

					window.async_scrolling_url_addition = result.more_url
					window.resizeText() # defined in main.js.coffee
					window.setExpandCollapse($feed) # defined in main.js.coffee

					# auto-expand first news item if news items have been loaded
					$feed.find('[data-news-item]').first().click()

					window.waitBeforeScrollCheck = false

		$('#feedButtonbar a').click (e) ->
			e.preventDefault()

			feed_url = $(this).data('feed-url')
			active_feed_url = feed_url

			# set the heading
			$heading.text $(this).text()

			# reset that more pages exist on the async scrolling system
			window.resetAsyncScroll() if window.resetAsyncScroll

			# clear active and set on the current button
			$('#feedButtonbar').find('.feedBtn.activeFeedBtn').removeClass('activeFeedBtn')
			$(this).find('.feedBtn').addClass('activeFeedBtn')

			# replace the current content with the 'loading' content
			$feed.html $loading.clone()

			url = '/dashboard/' + feed_url + '?view_date=' + $('#lastViewedDate').val()
			
			$.getJSON(url, success(feed_url))
			.fail ->
				$feed.text "An error occured retrieving the feed."

		# autoclick whichever button is active on first load
		$('#feedButtonbar').find('.activeFeedBtn').click()

		# -- special height-based expand/collapse for posts on the dashboard
		$feed.on 'click', '[data-feed-expand-target-id]', (e) ->
			e.preventDefault()

			target = $(this).data 'feed-expand-target-id'
			$content = $("[data-feed-expand-id='" + target + "']")
			expandedClass = $(this).data 'feed-expanded-class'

			$allExpanders = $("[data-feed-expand-target-id='" + target + "']")

			if $content.data('collapsedHeight') && parseInt($content.css('height').replace('px', '')) > parseInt($content.data('collapsedHeight').replace('px', '')) # if already expanded
				$content.css 'height', $content.data('collapsedHeight')
				
				$allExpanders.removeClass expandedClass
				$allExpanders.filter('[data-feed-expanded-text]').each ->
					$(this).text $(this).data('feed-original-text')

			else # if collapsed
				$content.data 'collapsedHeight', $content.css('height')
				$content.css 'height', 'auto'
				$allExpanders.addClass expandedClass
				$allExpanders.filter('[data-feed-expanded-text]').each ->
					$(this).data 'feed-original-text', $(this).text()
					$(this).html $(this).data('feed-expanded-text')

		$('#createNewIdea').click -> 
			loc = $(this).attr 'href'
			window.location.href = loc
