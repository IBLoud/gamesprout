# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
	# -- search for a user when trying to adminify them
	findUserForm = $('#find-user')
	loaderGif = findUserForm.find('.ajax-loader')

	findUserForm.find('button').click -> loaderGif.show()

	findUserForm.on 'ajax:success', (e, result) ->
		if result.user == null || result.user == "null"
			alert('User not found.')
			return

		user = result.user

		loaderGif.hide()

		$('#username').text user.username
		$('#email').text user.email
		$('#id').text user.id


		$('#is-admin').text(if user.admin then 'Yes' else 'No')

		$('#user-id').val user.id
		
		submit = $('#submit')
		if user.admin == true
			submit.val 'Un-Adminify ' + user.username
		else
			submit.val 'Adminify ' + user.username

		$('#hidden').show()

# -- load user join data to table
unless typeof(google) == "undefined" || typeof(google) == undefined
	google.load "visualization", "1", packages: ["annotatedtimeline"]
	google.load "visualization", "1", packages: ["corechart"]

$(document).ready ->
	if $("#admin-user-vis-chart-data").length isnt 0
		$data_daily = new google.visualization.DataTable()
		$data_total = new google.visualization.DataTable()
		$data_daily.addColumn('date', 'Date')
		$data_daily.addColumn('number', 'New Users')
		$data_total.addColumn('date', 'Date')
		$data_total.addColumn('number', 'Total Users')
		$table = $("#user-vis-chart-data")
		$rows = $table.find("tr")
		$daily_arr = new Array()
		$total = 0
		$($("tr").get().reverse()).each (index) ->
			$rowData = []
			$(this).find("td").each (v) ->
				$rowData[v] = $(this).text()
			$dateData = []
			if typeof $rowData[0] isnt "undefined"
				$total = $total + parseInt($rowData[1])
				$dateData = $rowData[0].split("-")
				$data_daily.addRow [new Date(parseInt($dateData[0]), parseInt($dateData[1]) - 1, parseInt($dateData[2])), parseInt($rowData[1])]
				$data_total.addRow [new Date(parseInt($dateData[0]), parseInt($dateData[1]) - 1, parseInt($dateData[2])), $total]
		annotatedtimeline = new google.visualization.AnnotatedTimeLine(document.getElementById("admin-visualization-daily"))
		annotatedtimeline.draw $data_daily, displayAnnotations: false
		annotatedtimeline = new google.visualization.AnnotatedTimeLine(document.getElementById("admin-visualization-total"))
		annotatedtimeline.draw $data_total, displayAnnotations: false

	if $("#admin-idea-task-vis-chart-data").length isnt 0
		$table_hash = {}
		$("tr").each (index) ->
			$rowData = []
			$(this).find("td").each (v) ->
				$rowData[v] = $(this).text()
			unless $table_hash[$rowData[0]]?
				$table_hash[$rowData[0]] = [parseInt($rowData[1]), 1]
			else
				$table_hash[$rowData[0]][1] += 1
				$table_hash[$rowData[0]][0] += parseInt($rowData[1])
		$table_arr = []
		for $key of $table_hash
			$table_arr.push [$key, $table_hash[$key][0], $table_hash[$key][1]]
		$table_arr.sort (a, b) ->
			return -1  if a[0] < b[0]
			return 1  if a[0] > b[0]
			0
			
		$data_task = new google.visualization.DataTable()
		$data_task.addColumn('string', 'Month')
		$data_task.addColumn('number', 'Ideas Created That Use TaskList')
		$data_task.addColumn('number', 'Ideas Created')
		$i = 0
		while $i < $table_arr.length
			$data_task.addRow($table_arr[$i])
			$i++
		$ac = new google.visualization.AreaChart(document.getElementById("admin-visualization-idea-task"))
		$ac.draw $data_task,
			title: "Idea TaskList Use By Month of Idea Creation"
			isStacked: false
			width: 800
			height: 400
			vAxis:
				title: "Ideas"

			hAxis:
				title: "Month"


