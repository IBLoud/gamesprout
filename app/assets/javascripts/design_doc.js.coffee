$ ->
	# -- Design Document Page - Edit Container Details
	$("#billboard .editable .gameinfo").hover ((e) ->
	    $("#splash-container img").css
	      "z-index": 2
	      opacity: .5
	    $(".gameinfo").css "background-color", "#eee"

	    $("#edit-banner-image").show()
	    $("#edit-main-details").show()
	    $(this).addClass "active"
	  ), (e) ->
	    $("#splash-container img").css
	      "z-index": 0
	      opacity: 1
	    $(".gameinfo").css "background-color", "#fff"

	    $("#edit-banner-image").hide()
	    $("#edit-main-details").hide()
	    $(this).removeClass "active"
	$(".gallery .thumb .editable").hover ((e) ->
	    $(this).parent("li").find(".gallery-img img").css
	      "z-index": 2
	      opacity: .5
	    $(this).addClass "active"
	  ), (e) ->
	    $(this).parent("li").find(".gallery-img img").css
	      "z-index": 0
	      opacity: 1
	    $(this).removeClass "active"
	
	$("#design-details .btn-edit-platform").click ->
    	if $("#design-details #core-platform-edit").css("display") == "none"
    		$("#design-details #core-platform-edit").css "display": "block"
    	else
    		$("#design-details #core-platform-edit").css "display": "none"
    	$("#design-details #core-platform").toggle()
    	$("#design-details .btn-edit-platform").toggle()
    	$("#design-details .btn-save-platform").toggle()
	
	$("#design-details .btn-edit-mechanics").click ->
    	$("#design-details #core-mechanics").toggle()
    	$("#design-details .btn-edit-mechanics").toggle()
    	$("#design-details .btn-save-mechanics").toggle()
    	id = "mechanics"
    	if $("#design-details #core-mechanics-edit").css("display") == "none"
    		$("#design-details #core-mechanics-edit").css "display": "block"
    	else
    		$("#design-details #core-mechanics-edit").css "display": "none"
	
	$("#design-details .btn-edit-aesthetics").click ->
    	if $("#design-details #core-aesthetics-edit").css("display") == "none"
    		$("#design-details #core-aesthetics-edit").css "display": "block"
    	else
    		$("#design-details #core-aesthetics-edit").css "display": "none"
    	$("#design-details #core-aesthetics").toggle()
    	$("#design-details .btn-edit-aesthetics").toggle()
    	$("#design-details .btn-save-aesthetics").toggle()
	
	$("#design-details .btn-edit-favorite").click ->
    	if $("#design-details #core-favorite-edit").css("display") == "none"
    		$("#design-details #core-favorite-edit").css "display": "block"
    	else
    		$("#design-details #core-favorite-edit").css "display": "none"
    	$("#design-details #core-favorite").toggle()
    	$("#design-details .btn-edit-favorite").toggle()
    	$("#design-details .btn-save-favorite").toggle()