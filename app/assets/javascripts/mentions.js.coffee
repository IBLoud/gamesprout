## *********************************************************************************
## *** All of this code relates to the @mention UI that pops up in comment boxes ***
## *********************************************************************************

simpleRgx = /[A-Za-z0-9_-]+/ # same as found in mention.rb, but without the @

getNamesStartingWith = (str, textarea, callback) ->
	str = str.replace('"', '')
	post_id = $(textarea).parents('form').first().children('#comment_post_id').val()
	$.post '/search_users', {search_str: str, post_id: post_id}, (result) ->
		callback(result.usernames)

# -- handles creating the mention helper box for a textarea if it doesn't exist yet; eventually all of the keyboard and click triggers will go here as well
# -- note that this class mostly acts as a wrapper for a jq object; it's not isolated in a philosophically sound way.
class MentionHelper
	constructor: (@textarea) ->
		@helper = $("<div class='mention_helper' />")
		@helper.append("<ul />")

		# scope shennaningans
		textarea = @textarea
		helper = @helper

		@helper.on 'click', 'ul li', (e) ->
			name = $(this).text()
			if name.replace(simpleRgx, '').length > 0
				name = '"' + name + '"'

			# get all text before and after the current working text
			cursorLoc = $(textarea).prop('selectionStart')
			length = helper.data('lastSearchTerm').length

			beforeText = $(textarea).val()[0...(cursorLoc-length)]
			afterText = $(textarea).val()[cursorLoc..-1]

			finalText = beforeText + name + afterText
			$(textarea).val(finalText)

			deactivateMentionOn(textarea)


	addNames: (names, @searchTerm) ->
		@helper.children('ul').empty()
		for n in names
			li = $("<li class='mention_selection' />").text(n)
			@helper.children('ul').append(li)

	css: (cssObj) ->
		@helper.css(cssObj)

	show: ->
		@helper.show()

	$get: -> 
		return @helper

	setLastSearchTerm: (searchTerm) ->
		@helper.data('lastSearchTerm', searchTerm)

	moveDown: ->
		selected = @helper.find('li.selected')
		if selected.length > 0
			selected.removeClass('selected')
			selected.next().addClass('selected')
		else
			@helper.find('li').first().addClass('selected')

	moveUp: ->
		selected = @helper.find('li.selected')
		if selected.length > 0
			selected.removeClass('selected')
			selected.prev().addClass('selected')
		else
			@helper.find('li').last().addClass('selected')

	makeSelection: ->
		@helper.find('li.selected').click()



# -- handles the display, placement and filling-with-content of the mention helper box
displayMentionHelper = (textarea, names, searchTerm) ->
	caretCoords = getCaretCoordinates(textarea)
	textareaCoords = $(textarea).offset()
	lineHeight = parseInt $(textarea).css('lineHeight').replace('px', '')
	padding = parseInt $(textarea).css('paddingLeft').replace('px', '')

	coords = {
		left: textareaCoords.left + padding,
		top: caretCoords.top + textareaCoords.top + lineHeight # we want it to be below the text
	}

	helper = $(textarea).data('helper') # see if there is already a box associated with this textarea
	if !helper		
		helper = new MentionHelper textarea
		$(textarea).data('helper', helper)
		$("body").append(helper.$get())

	helper.css {
		left: coords.left,
		top: coords.top
	}

	helper.setLastSearchTerm searchTerm
	helper.show()
	helper.addNames(names) if names.length > 0



# -- handles all logic for when the mention system has been activated and complex logic needs to go down
activeMentionHandler = (e) ->	
	textarea = this
	text = $(textarea).val()
	cursorLoc = $(textarea).prop('selectionStart')
	char = String.fromCharCode(e.which) # since this hasn't propogated yet, we need to know what the last char is	

	# if the user hits enter after an @, just break them out of the mention system
	if e.which == 13 && text[cursorLoc - 1] == "@" # 13 == enter
		deactivateMentionOn(this)
		return

	# if the user hit backspace and deleted an @ sign, disable the mention system
	if e.which == 8 && text[cursorLoc - 1] == "@" # 8 == backspace
		deactivateMentionOn(this)
		return
	else if e.which == 8 # special handler for non-deactivating backspace
		cursorLoc--
		char = ''

	# handle arrow keys
	if (e.keyCode in [37..40]) || (e.which == 13) # 37..40 == arrow keys, 37 is left and moving clockwise; 13 == enter
		e.stopPropagation()
		e.preventDefault()

		helper = $(textarea).data('helper')

		if e.keyCode in [38, 37] # up or left
			helper.moveUp()
		else if e.keyCode in [40, 39] # down or right
			helper.moveDown()
		else if e.which == 13
			helper.makeSelection()

		return
	
	nameCharArray = []
	for x in [cursorLoc..0]
		if x == cursorLoc # the character at cursorLoc is undefined right now b/c the event hasn't propogated yet
			c = char
		else
			c = text[x]

		break if c == '@'
		nameCharArray.push(c)

	searchTerm = nameCharArray.reverse().join('').trim()
	return unless searchTerm

	getNamesStartingWith searchTerm, textarea, (names) ->
		displayMentionHelper(textarea, names, searchTerm)


mentionIsActiveOn = (obj) ->
	return $(obj).data('mention-active') || false

activateMentionOn = (obj) ->
	$(obj).data('mention-active', true)

deactivateMentionOn = (obj) ->
	$(obj).data('mention-active', false)
	helper = $(obj).data('helper')
	helper.$get().hide() if helper

$ ->
	arrowKeys = [37, 38, 39, 40]

	# handle JUST the arrow keys; need to handle it here b/c chrome only throw keydown for arrow keys
	$('body').on 'keydown', '[data-show-mention-ui=true]', (e) ->
		if mentionIsActiveOn(this) && (e.keyCode in arrowKeys)
			activeMentionHandler.call this, e

	$('body').on 'keypress', '[data-show-mention-ui=true]', (e) ->
		if e.which != 0
    	char = String.fromCharCode(e.which);    

		# here all we want to do is look at whether the mention...
		# - is inactive and should stay so (any key press other than @)
		# - is inactive but needs to be activated (the @ key, e.which == 64, has been pressed)
		# - is active and should handle the keypress (pass the event object on to the activeMentionHandler func)
		# - is active but should be deactivated
		if !mentionIsActiveOn(this) && char == "@"
			activateMentionOn(this)
		
		else if mentionIsActiveOn(this) && (e.which == 32 || e.keyCode == 27) # 32 == space, 27 == escape
			deactivateMentionOn(this)

		else if mentionIsActiveOn(this) && !(e.keyCode in arrowKeys) # chrome only fires keydown for arrow keys, but mozilla fires both, so we handle on keydown and supress here
			activeMentionHandler.call this, e # .call sets 'this' as first argument, then parameters as remaining arguments
		
		else
			# do nothing