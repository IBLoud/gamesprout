module CommentsHelper
  def user_can_delete_comment?(comment)
    return false unless user_signed_in? && !comment.removed?

    is_idea_owner = defined?(@top_parent) && !@top_parent.nil? && @top_parent.respond_to?(:owner) && @top_parent.owner == current_user    
    is_idea_owner || current_user.admin? || (current_user == comment.user)
  end

  private
  def combined_path_elements(parents)
    parents.inject("") do |path, parent|
      path + "#{parent.class.model_name.to_s.underscore}:#{parent.id}/"
    end
  end
end
