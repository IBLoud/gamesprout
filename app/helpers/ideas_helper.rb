module IdeasHelper
  def get_feed_icon(post)
    case post.type
    when "discussion"
      "discussion"
    when "poll"
      "poll"
    when "art_collection"
      "art"
    when "browser_playable_prototype", "video_prototype", "downloadable_prototype"
      "demo"
    else
      "unknown post type"
    end
  end

  def following?(idea)
    if user_signed_in?
      not current_user.followed_ideas.first(:idea_id => idea.id).nil?
    else
      false
    end
  end

  def browse_type_class
    actual_name = request.path_info.split('/')[2]

    # to prevent css issues, for now the original names are used instead of the new, clearer names
    case actual_name
    when "browse"
      "design"
    when "play"
      "play"
    when "help_out"
      "build"
    when "search"
      "search"
    end
  end

  def filter_for_url(filter)
    filter == "all" ? url_for(:filter_for => nil) : url_for(params.merge(:filter_for => filter))
  end

  def idea_banner(idea)
    idea.details.banner_image? ? idea.details.banner_image : "/assets/default-idea.png"
  end

  def prototype_banner(prototype)
    prototype.image ? prototype.image : "/assets/default-demo.png" #"http://placehold.it/175x120"
  end

  def search_text
    params[:query] ? {:value => params[:query]} : {:placeholder => "Search all games..."}
  end

  def user_can_delete_post?(post)
    return false unless user_signed_in?
    current_user.admin? || post.idea.owner == current_user || post.user == current_user
  end

  def user_can_edit_post?(post)
    return false unless user_signed_in?
    can_edit = current_user.admin? || post.user == current_user
    
    if post.is_poll? && !current_user.admin?
      can_edit = can_edit && !post.has_any_votes?
    end

    can_edit
  end

  def by_staff?(idea, post)
    is_admin = post.user.admin?
    is_owner = idea.owner == post.user

    is_admin and not is_owner
  end

  def commit_count_text(goal)
    c = goal.commitments
    if c.size == 1
      text = '1 person is'
    else
      text = "#{c.size} people are"
    end

    "#{text} working on this goal!"
  end

  def special_rateable_attribs(rateable, text_before_id, hash={})
    if rateable.hidden?
      h = {:class => 'hidden', :'data-expander-id' => "#{text_before_id}_#{rateable.id.to_s}"}
    else
      h = {}
    end

    h = h.merge(hash)
    return h
  end

  def advice_bar_phase(idea)
    unless idea.details.complete?
      return :incomplete_design_doc
    end

    if idea.goals.nil? || idea.goals.empty?
      return :ask_help
    end
    
    if idea.goals.where(:what_kind => :demo).count.eql? 0
      return :ask_demo
    end
    
    return :ongoing
  end

  def generate_browse_cache
    {:collection => @results, :order => @order_scope, :tags => @tags}
  end

  def facebook_script
    %Q{
      window.open(
        'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
        'facebook-share-dialog', 
        'width=626,height=436'); 
      return false;
    }
  end

  def twitter_script
    %Q{
        var width  = 575,
        height = 400,
        left   = ($(window).width()  - width)  / 2,
        top    = ($(window).height() - height) / 2,
        url    = 'http://twitter.com/share?text='+document.title+'&url='+location.href +'&hashtags=gamesprout',
        opts   = 'status=1' +
                 ',width='  + width  +
                 ',height=' + height +
                 ',top='    + top    +
                 ',left='   + left;
    
        window.open(url, 'twitter', opts);
     
        return false;

      }
  end

  def task_classes(task, idea)
    task_id = task["TaskID"]
    task = @idea.tasks[task_id]
    
    if task && task["checked"]
      on_off = "on"
    else
      on_off = "off"
    end

    if current_user && idea.owner == current_user
      owner = "owner"
    else
      owner = ""
    end

    "#{on_off} #{owner}"
  end

  def show_demo_url_field?
    @prototype.is_video? || (!@prototype.new? && !@prototype.will_upload_files)
  end
end
