module AdminHelper
  def news_alert_date_value(news_alert)
    if news_alert.new? || news_alert.stop_at.nil?
      ''
    else 
      @news_alert.stop_at.strftime('%Y-%m-%d %I:%M %p').downcase
    end
  end

  def is_task_used?(idea)
    task = idea.tasks
    is_used = task != {}
  end
end
