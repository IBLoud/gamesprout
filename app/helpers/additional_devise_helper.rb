module AdditionalDeviseHelper

  def special_devise_error_messages!(resource)
    flash_alerts = []
    error_key = 'errors.messages.not_saved'

    if !flash.empty?
      flash_alerts.push(flash[:error]) if flash[:error]
      flash_alerts.push(flash[:alert]) if flash[:alert]
      flash_alerts.push(flash[:notice]) if flash[:notice]
      error_key = 'devise.failure.invalid'
    end

    # we don't actually want to add these errors to the real resource because then they're displayed in the form that
    # follows, so we just create a dummy user just for the purposes of rendering our error page
    user_with_errors = User.new(resource.attributes)

    unless flash_alerts.empty?      
      flash_alerts.each do |error_msg|
        user_with_errors.errors.add(:base, error_msg)
      end
    end

    return user_with_errors
  end

end