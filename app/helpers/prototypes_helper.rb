module PrototypesHelper

  def get_title_descriptor(type)
    case type.to_s
    when "browser_playable_prototype"
      return "Short Description of the Prototype [140 Characters Max]"
    when "downloadable_prototype"
      return "Short Description of the Prototype [140 Characters Max]"
    when "video_prototype"
      return "Title / Summary of Video"
    else
      return ""
    end
  end

  def get_description_descriptor(type)
    msg = ""

    case type.to_s
    when "browser_playable_prototype"
      msg = "What is in the prototype? If there are other prototypes, what makes this one unique? What should playtesters be trying to test?"
    when "downloadable_prototype"
      msg = "What is in the prototype? If there are other prototypes, what makes this one unique? What should playtesters be trying to test?"
    when "video_prototype"
      msg = "What is happening in this video? What features is it showing off? If it's another game, what relevance does it have?"
    end

    return "#{msg} [max 280 characters]"
  end

  def get_image_descriptor(type)
    "An image url associated with your prototype (optional)."
  end

  def get_source_url_descriptor(type)
    "Link to a public repository containing your source code (optional!)"
  end

  def get_url_descriptor(type)
    case type.to_s
    when "browser_playable_prototype"
      return "If not uploading your demo to GameSprout, provide the url of the page hosting the Flash or Unity game. It will displayed in a 980x600 pixel frame."
    when "downloadable_prototype"
      return "If not uploading your demo to GameSprout, provide the url to the zip file containing your prototype."
    when "video_prototype"
      return "The direct url to a Youtube video. The embed code will be generated automatically."
    else
      return ""
    end
  end

  def get_instructions_descriptor(type)
    case type.to_s
    when "browser_playable_prototype"
      return "How do you playtest this prototype? What are the controls? Are there any known issues the players should be aware of? Be as descriptive as possible."
    when "downloadable_prototype"
      return "How do you playtest this prototype? What steps are required to install this? What are the controls? Are there any known issues the players should be aware of? Be as descriptive as possible."
    else
      return ""
    end
  end

  def get_link_descriptor(type)
    case type.to_s
    when "video_prototype"
      return "Url of the Youtube or Vimeo video. Note: not the embed code! The embed code will be generated automatically."
    else
      return ""
    end
  end

  def prototype_page_title
    type = @prototype.class.to_s.titleize
    "Add #{type} - #{@idea.title} - GameSprout Idea"
  end

  def browse_prototype_icon_class(prototype)
    case prototype.type
    when 'browser_playable_prototype'
      'proto'
    when 'video_prototype'
      'video'
    when 'downloadable_prototype'
      'download'
    end
  end 

end
