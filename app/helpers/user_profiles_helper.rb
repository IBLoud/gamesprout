module UserProfilesHelper
  def my_profile?
    user_signed_in? && @user == current_user
  end

  def user_settings_sidebar_class
    case request.path
    when user_details_path
      "profile-settings"
    when notification_settings_path
      "notification-settings"
    end
  end

  def twitter_handle_link(user)
    twitter_handle = user.user_details.twitter_handle.strip
    link_text = twitter_handle.starts_with?('@') ? twitter_handle : "@" + twitter_handle
    url_text = twitter_handle.starts_with?('@') ? twitter_handle[1..-1] : twitter_handle

    link_to link_text, "http://twitter.com/#{url_text}"
  end
end
