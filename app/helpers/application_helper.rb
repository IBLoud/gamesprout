module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title }
  end

  def user_owns_idea?(idea=@idea, user=nil)
    user ||= (current_user if user_signed_in?)
    !user.nil? && idea.owner == user
  end

  def admin?
    user_signed_in? && current_user.admin?
  end

  def username
    current_user.username
  end  

  def get_user_date_format(include_time = false)
    if user_signed_in? && (not current_user.user_details.nil?)
      format = current_user.user_details.date_format
    else
      format = "%m/%d/%y"
    end
    if include_time
      format = format + " at %l:%M%P"
    end
    format
  end

  def time_for(datetime)
    datetime.strftime("%l:%M%P").downcase.gsub(' 0', ' ')
  end

  def get_formatted_date(date, options = {})
    if date.nil?
      return "nil"
    end
    date.strftime(get_user_date_format(options[:include_time])).downcase.gsub(' 0', ' ')
  end

  def main_nav_link(text, path)
    link_to text, path
  end

  def if_selected(group, item, css_class, options={})
    if current_user.selected?(group, item) || (options[:default] && current_user.no_selection?(group))
      css_class
    else
      ''
    end
  end

  def youtube_embed(youtube_url, options={})
    if youtube_url[/youtu\.be\/([^\?]*)/]
      youtube_id = $1
    else
      youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      youtube_id = $5
    end

    width = options[:width] || 980
    height = options[:height] || 600

    %Q{<iframe title="YouTube video player" width="#{width}" height="#{height}" src="http://www.youtube.com/embed/#{ youtube_id }" frameborder="0" allowfullscreen></iframe>}
  end

  def browser_is_IE10?
    !!(request.env["HTTP_USER_AGENT"] && request.env["HTTP_USER_AGENT"].match(/MSIE 10/))
  end

  def cache_key_for_post(post, prefix)
    "#{prefix.to_s}/#{post.class.to_s}/#{post.id.to_s} updated_at:#{post.updated_at.to_s}"
  end

  def post_date(date)
    if (DateTime.now - 2.day) <= date
      distance_of_time_in_words(DateTime.now, date) + " ago"
    elsif (DateTime.now - 1.year) <= date
      date.strftime("%B %e")
    else
      date.strftime("%B %e, Y")
    end
  end
end