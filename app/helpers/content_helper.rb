module ContentHelper
  def admin_content(html)
    html = html.maintain_linebreaks.convert_urls_to_anchors unless (html.nil? || html.empty?)
    raw Sanitize.clean(html, Sanitize::Config::RELAXED)
  end

  def comment_content(html)
    html = html.maintain_linebreaks.convert_urls_to_anchors unless (html.nil? || html.empty?)
    raw Sanitize.clean(html, CustomSanitizeConfig::COMMENT)
  end

  def content(html)
    html = html.maintain_linebreaks.convert_urls_to_anchors unless (html.nil? || html.empty?)
    raw Sanitize.clean(html, CustomSanitizeConfig::USER)
  end

  def text_with_mention_links(text, model)
    model.mentions.each do |mention|
      link = link_to(mention.mention_string, mention.mentioned_user)
      text.gsub!(mention.mention_string, link)
    end

    return text
  end
end