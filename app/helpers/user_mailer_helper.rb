module UserMailerHelper
  include ContentHelper

  def absolute_src(relative_path)
    # avoid a double slash by removing the initial slash in the relative_path if it has one
    root_url + (relative_path.starts_with?('/') ? relative_path[1..-1] : relative_path)
  end

  # -- Notification helper methods
  def art_and_prototypes_from(posts)
    posts.select { |post| post.is_art_collection? || post.is_prototype_or_video?}
  end

  # title or equivalent for a post
  def post_title(post)
    begin
      if post.is_discussion?
        post.text.gsub('<br/>', ' ').split(' ')[0..8].join(' ') + '...'
      elsif post.is_art_collection?
        post.summary.gsub('<br/>', ' ').split(' ')[0..8].join(' ') + '...'
      else
        post.title
      end
    rescue
      post.model_name.to_s.humanize
    end
  end

  def posts_with_comments_from(comments)
    comments.inject({}) do |hash, comment|
      hash[comment.post] ||= []
      hash[comment.post].push comment

      hash
    end
  end

  # -- Batch Email Notifications
  # def anchor_style
  #   add_style 'text-decoration' => 'none'

  #   apply_styles
  # end

  # -- Styles
  def header_style
    add_style 'display' => 'block'
    add_style 'margin'  => '0 auto'

    apply_styles
  end

  def body_style
    add_style 'width'       => "641px"
    add_style 'margin'      => '0 auto 0 auto'
    add_style 'color'       => '#444444'

    apply_styles
  end

  def email_content_style
    add_style 'margin'      => '0 auto'
    add_style 'width'       => '560px'

    apply_styles
  end

  def footer_divider_style
    add_style "width"       => "600px"
    add_style "min-height"  => "38px"
    add_style 'display'     => 'block'
    add_style 'margin'      => '0 auto'

    apply_styles
  end

  def footer_content_style
    add_style 'width'             => '600px'
    add_style 'margin'            => '0 auto'
    add_style 'background-color'  => '#e8f2da'

    apply_styles
  end

  def footer_p_container_style
    add_style 'margin-top'  => '4px'
    add_style 'padding'     => '1px 10px'
    add_style 'font-size'   => '14px'    

    apply_styles
  end

  def small_style
    add_style "font-size" => "12px"

    apply_styles
  end

  private
  def add_style(hash)
    @styles_hash ||= {}
    @styles_hash.merge!(hash)
  end

  def apply_styles
    styles = ''
    @styles_hash.each do |attrib, value|      
      styles += "#{attrib}: #{value};"
    end

    @styles_hash.clear
    return styles
  end
end