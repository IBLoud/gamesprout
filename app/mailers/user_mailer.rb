class UserMailer < ActionMailer::Base
  default from: "GameSprout <gs-support@schellgames.com>"  
  layout 'email'

  def activity_notification(user, activity)
    @user = user
    @activity = activity    
    @date = Time.zone.now.yesterday

    mail(to: email, subject: "GameSprout Activity Notification")
  end

  def gamejam_signup(user, password)
    @user = user
    @password = password
    @name = user.username

    mail(to: user.email, subject: "Welcome to GameSprout!")
  end

  private
  def email
    "#{@user.username} <#{@user.email}>"
  end
end
