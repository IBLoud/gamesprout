class PagesController < ApplicationController
  def splash_07_13
    session[:visited_splash_page_07_13] = true
  end

  def reputation
    begin      
      @user = User.find_by_id_or_username params[:user]
    rescue
    end

    if @user
      @user_reputation_data = UserReputationData.for_user_id(@user.id)
    end
  end
end