class DesignDocsController < ApplicationController  
  before_filter :must_be_signed_in!, :except => :show
  before_filter :get_idea
  before_filter :get_design_doc, :except => [:new, :create]
  before_filter :must_own_idea, :only => [:create, :date]

  def new
    @design_doc = DesignDoc.new
  end

  def create
    @design_doc = DesignDoc.new(params[:design_doc])
    @design_doc.idea = @idea

    if @design_doc.save
      flash[:notice] = "New design details successfully added!"
      redirect_to design_document_idea_path(@idea)
    else
      render :action => :new
    end
  end

  def edit
  end

  def update
    if @design_doc.update(params[:design_doc])
      flash[:notice] = "Design details document successfully updated!"
      redirect_to idea_design_doc_path(@idea, @design_doc)
    else
      flash[:alert] = "There was an error updating the design details document."
      render :action => :edit
    end
  end

  def destroy
    if @design_doc.destroy
      flash[:notice] = "Design details document successfully deleted!"
      redirect_to design_document_idea_path(@idea)
    else
      flash[:alert] = "There was an error deleting the design details document."
      render :action => :edit
    end
  end

  private
  def get_idea
    @idea = Idea.get(params[:idea_id])
  end
  
  def get_design_doc
    @design_doc = DesignDoc.get(params[:id])    
  end

  def must_own_idea
    unless @idea.owner == current_user
      flash[:alert] = "Permission denied."
      redirect_to @idea
    end
  end
end