class FollowsController < ApplicationController
  before_filter :must_be_signed_in!
  before_filter :set_idea_id

  def create
    follow = Follow.first_or_create(idea_id: @idea_id, user_id: current_user.id)
    redirect_to idea_path(@idea_id)
  end

  def destroy
    Follow.first(idea_id: @idea_id, user_id: current_user.id).destroy
    redirect_to idea_path(@idea_id)
  end

  private
  def set_idea_id
    @idea_id = params[:id]
  end
end