class FeedbackController < ApplicationController
  before_filter :user_must_be_admin!, :except => [:new, :create, :thank_you]
  before_filter :get_feedback, :only => [:show, :edit, :update, :change_logged]

  def index
    if not params[:commit].eql? "Filter"
      params[:view] = params[:commit]
    end
    
    case params[:category]
    when "Logged"
      @feedback = Feedback.logged.newest_created
    when "All"
      @feedback = Feedback.oldest_created
    else
      @feedback = Feedback.visible.newest_created
    end

    if not (params[:type].blank? || (params[:type].eql? "All"))
      @feedback = @feedback.delete_if {|f| not f.type.eql? params[:type]}
    end

    if not params[:owner].blank?
      search_email = nil
      search_user = nil
      if params[:owner].match(/\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/)
        search_email = params[:owner]
      else
        search_user = User.first(:username => params[:owner])
        if not search_user.nil?
          search_email = search_user.email
        end
      end
      @feedback = @feedback.delete_if {|f| not f.email.eql? search_email}
    end
  end

  def show    
  end

  def new
    @feedback = Feedback.new
  end

  def create    
    @feedback = Feedback.create(params[:feedback])
    @feedback.user = current_user if user_signed_in?

    if @feedback.save
      redirect_to :action => :thank_you
    else
      render :action => :new
    end
  end

  def edit
  end

  def update
    @feedback_item.notes = params[:feedback][:notes]

    if @feedback_item.save    
      redirect_to @feedback_item
    else
      render :action => :edit
    end
  end

  def thank_you
  end

  def change_logged
    if params[:action_to_take] == "log"
      @feedback_item.logged = true
      flash[:notice] = "Feedback item logged."
    elsif params[:action_to_take] == "unlog"
      @feedback_item.logged = false
      flash[:notice] = "Feedback item un-logged."
    end

    render :json => { success: @feedback_item.save }
  end

  private
  def get_feedback
    @feedback_item = Feedback.get(params[:id])
  end
end