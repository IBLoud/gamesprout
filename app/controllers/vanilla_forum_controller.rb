class VanillaForumController < ApplicationController
  before_filter :authenticate_user!

  def single_sign_on
    user = {
      "uniqueid"  => current_user.id.to_s,
      "name"      => current_user.username,
      "email"     => current_user.email,
      "photourl"  => photo_url
    }

    # 4. Generate the jsConnect string.
    secure = true # this should be true unless you are testing.
    
    client_id = Rails.configuration.vanilla_forum_client_id
    secret = Rails.configuration.vanilla_forum_secret
    json = JsConnect.getJsConnectString(user, self.params, client_id, secret, secure)
    
    render :text => json
  end

  def embedded_forum
  end

  private
  def photo_url
    if Rails.env.development?
      host = 'localhost:3000'
    else
      host = ENV['HOST_URL']
    end
      
    "http://#{host}/assets/default-avatar.png"
  end

end
