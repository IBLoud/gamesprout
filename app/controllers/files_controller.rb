class FilesController < ApplicationController
  before_filter :must_be_signed_in!
  before_filter :get_user!

  def all
    @all_files = @user.files.all(:order => :created_at.desc)
  end

  def index
    # either the current user, or if given a param AND are an admin, any user

    @all_files = @user.files.all(:order => :created_at.desc)
    @recent_files = @all_files.take(5)
  end

  def upload_complete
    file = UploadedFile.new(params)
    file.user = current_user
    success = file.save

    render :json => {success: success}
  end  

  private
  def get_user!
    if params[:user]
      if current_user.admin?
        @user = User.get(params[:user])
      else
        flash[:alert] = "Permission denied."

        path = Rails.application.routes.recognize_path request.path
        redirect_to :controller => path[:controller], :action => path[:action]
      end
    else
      @user = current_user
    end
  end
end
