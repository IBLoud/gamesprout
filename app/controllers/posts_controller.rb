class PostsController < ApplicationController
  before_filter :must_be_signed_in!, :except => [:show, :more_comments]
  before_filter :find_post, :except => [:create]
  before_filter :check_edit_permissions, :only => [:edit, :update]
  before_filter :declare_named_post_variable, :only => [:edit, :update]

  SHOW_HOW_MANY_INITIAL_COMMENTS = 3
  SHOW_HOW_MANY_MORE_COMMENTS = 9

  def show
    @idea = @post.idea
  end

  def create
    poll_options = params[:post].delete :poll_options
    image_links = params[:post].delete :image_links

    params[:post][:will_upload_files] = params[:post].delete "will_upload_files_#{params[:post][:type]}"

    @post = Post.new(params[:post])
    declare_named_post_variable

    # Poll Options (for polls)
    poll_options.take(5).each do |k, v|
      v = v.strip
      unless v.nil? || v.empty?
        poll_option = PollOption.new(text: v)
        @post.poll_options << poll_option
      end
    end unless poll_options.nil?

    # Image Links (for art collections)
    image_links.take(5).each do |k, v|
      v = v.strip
      unless v.nil? || v.empty?
        image_link = ImageLink.new(url: v)
        @post.image_links << image_link
      end
    end unless image_links.nil?

    if @post.save
      @post.idea.recount_activity!
      @post.idea.save

      Mention::check_for_mentions(@post)

      if @post.will_upload_files
        redirect_to upload_files_post_path(@post)
      else
        redirect_to @post.idea
      end
    else
      type_dir = @post.is_prototype_or_video? ? "prototypes" : @post.type.pluralize
      render "/posts/#{type_dir}/edit"
    end
  end

  def edit
    render_edit
  end

  def update
    poll_options = params[:post].delete :poll_options
    image_links = params[:post].delete :image_links

    @post.update(params[:post])

    # Poll Options (for polls)
    if @post.is_poll?
      @post.poll_options.destroy
      @post.reload

      poll_options.take(5).each do |k, v|
        v = v.strip
        unless v.nil? || v.empty?
          poll_option = PollOption.new(text: v)
          @post.poll_options << poll_option
        end
      end
    end

    # Image Links (for art collections)
    if @post.is_art_collection?
      @post.image_links.destroy
      @post.reload

      image_links.take(5).each do |k, v|
        v = v.strip
        unless v.nil? || v.empty?
          image_link = ImageLink.new(url: v)
          @post.image_links << image_link
        end
      end
    end

    if @post.save
      Mention::update_mentions(@post)

      redirect_to @post.idea
    else
      render_edit
    end
  end

  def destroy
    if view_context.user_can_delete_post?(@post)
      @post.update(removed: true, removed_by_id: current_user.id)      
      redirect_to idea_path(@post.idea, :anchor => @post.id)
    else
      flash[:alert] = "Permission denied."
      redirect_to @post.idea
    end
  end

  def vote
    unless @post.is_poll?
      raise "Post with id #{@post.id.to_s} is not a poll."
    end

    if @post.user_has_voted?(current_user)
      render :json => {error: "You've already voted on this poll.", success: false}
      return
    end

    vote = PollVote.new
    vote.user = current_user

    poll_option = @post.poll_options.get(params[:poll_vote])
    poll_option.votes << vote
    poll_option.vote_count += 1
    poll_option.save

    @post.count_total_votes!
    @post.save

    render :json => {
      html: render_to_string(:partial => 'ideas/show/post_type_poll', :layout => false, :locals => {post: @post})
    }
  end

  def more_comments
    comments = @post.comments(:order => :created_at.desc)
    
    page = params[:page].to_i
    page_offset = page < 2 ? 0 : (page - 2)
    offset = SHOW_HOW_MANY_INITIAL_COMMENTS + (SHOW_HOW_MANY_MORE_COMMENTS * (page_offset)) # first page is just 3 comments

    paged_comments = comments.all(:offset => offset, :limit => SHOW_HOW_MANY_MORE_COMMENTS)
    has_more = comments.all(:offset => (offset + SHOW_HOW_MANY_MORE_COMMENTS), :limit => SHOW_HOW_MANY_MORE_COMMENTS).any?

    rendered_results = render_to_string :partial => 'shared/comment', :collection => paged_comments.to_a.reverse, :as => :comment, :layout => false

    render :json => {
      html: rendered_results,
      has_more: has_more
    }
  end

  def upload_files
  end

  def upload_completed
    # unlink any existing file(s)
    UploadedFile.all(:post_id => @post.id).each do |file|
      file.post_id = nil
      file.save
    end

    file = UploadedFile.new(params)
    file.post = @post
    file.user = current_user    
    file.save

    render :nothing => true, :status => :ok
  end

  private
  def find_post
    @post = Post.get(params[:id])
  end

  # set appropriately named var, eg. the discussion form expects @discussion not @post
  def declare_named_post_variable
    var_name = @post.is_prototype_or_video? ? "@prototype" : "@#{@post.type}"    
    instance_variable_set(var_name, @post)
  end

  def check_edit_permissions
    unless view_context.user_can_edit_post?(@post)
      flash[:alert] = "Permission denied."
      redirect_to idea_path(@post.idea, :anchor => @post.id)
    end
  end

  def render_edit
    dir = @post.is_prototype_or_video? ? "prototypes" : @post.type.pluralize
    render "posts/#{dir}/edit"
  end
end