class IdeasController < ApplicationController
  POSTS_AT_ONCE = 4

  before_filter :must_be_signed_in_with_redirect, :except => [:show, :more_posts, :checklist, :design_document, :deleted]
  before_filter :find_idea, :except => [:new, :create]
  before_filter :must_own_idea, :only => [:edit, :update, :destroy]

  def show
    respond_to do |format|
      format.html {
        @discussion = Post.new(type: 'discussion', user: current_user, idea_id:  @idea.id)
        @poll = Post.new(type: 'poll', user: current_user, idea_id:  @idea.id)
        @art_collection = Post.new_art_collection(current_user, @idea.id)

        @browser_playable_prototype = Post.new(type: 'browser_playable_prototype', user: current_user, idea_id:  @idea.id)
        @downloadable_prototype = Post.new(type: 'downloadable_prototype', user: current_user, idea_id:  @idea.id)
        @video_prototype = Post.new(type: 'video_prototype', user: current_user, idea_id:  @idea.id)
      }

      format.json {
        hash = {}

        # details
        hash[:details] = @idea.details.attributes
        hash[:details][:image_links] = []
        @idea.details.image_links.each do |image_link|
          hash[:details][:image_links].push image_link
        end

        # design docs
        hash[:design_docs] = []
        @idea.design_docs.each do |design_doc|
          hash[:design_docs].push design_doc.attributes
        end

        # tags
        hash[:tags] = @idea.tag_list
        
        # posts
        hash[:posts] = []
        @idea.posts.each do |post|
          post_hash = post.attributes

          if post.is_art_collection?            
            post_hash[:image_links] = [] 
            post.image_links.each do |image_link|
              post_hash[:image_links].push image_link.attributes
            end
          end

          if post.is_poll?
            post_hash[:poll_options] = []
            post.poll_options.each do |poll_option|
              post_hash[:poll_options].push poll_option.attributes
            end
          end

          hash[:posts].push post_hash
        end        

        render :json => hash.to_json
      }
    end    
  end

  def more_posts
    page = params[:page].to_i
    min = IdeasController::POSTS_AT_ONCE * (page - 1)
    max = IdeasController::POSTS_AT_ONCE * page

    all_posts = @idea.posts
    posts = all_posts[min...max]
    html = render_to_string :partial => 'ideas/show/post', :collection => posts, :as => :post, :layout => false

    render :json => {
      html: html
    }
  end

  def checklist
    path = File.expand_path('./config/locales/checklist.en.yml')
    @checklist = YAML::load_file(path)

    # aggregate some information about task groups
    completed_tasks = @idea.tasks.inject({}) do |task_group_hash, task|
      task = task.last # iterating over a hash, so we get an array of structure: [key, value]; we just care about the value
      key = task["group_id"]
      task_group_hash[key] ||= 0
      task_group_hash[key] += 1 if task["checked"] == true

      task_group_hash
    end

    @task_group_info = @checklist["en"]["TaskGroups"].inject({}) do |hash, element|
      key = element["TaskGroupID"]
      
      total_tasks = element["SubGroups"].collect { |subgroup| subgroup["Tasks"] }.flatten      

      hash[key] = {
        task_count: total_tasks.count,
        completed: completed_tasks[key].nil? ? 0 : completed_tasks[key]
      }

      hash
    end
  end

  def toggle_checklist_task
    if user_signed_in? && @idea.owner == current_user
      task_hash = params["checklist_task"]
      taskGroupId = task_hash["taskGroupId"]
      subgroupName = task_hash["subgroupName"]
      taskId = task_hash["taskId"]

      task = @idea.tasks[taskId] || {}
      task["group_id"] = taskGroupId
      task["subgroup"] = subgroupName

      # toggle whether it's checked
      task["checked"] = !task["checked"]

      @idea.tasks[taskId] = task
      success = @idea.save
    else
      success = false
    end

    render :json => {
      success: success
    }
  end

  def deleted
    unless @idea.deleted
      redirect_to @idea
    end
  end

  # -- Idea CRUD
  def new
    @idea = Idea.new
    @idea.details = IdeaDetails.new
  end

  def edit
  end

  def create
    @idea = Idea.new
    @idea.details = IdeaDetails.new

    tags = params[:idea].delete("tags")
    tags = tags.split(',') if tags

    @idea.attributes = params[:idea]
    @idea.submitter = current_user
    @idea.owner = current_user

    tags = sanitize_tags_array(tags)
    tags.each do |tag_name|
      tag = Tag.first_or_create(name: tag_name)
      @idea.tags << tag
    end

    if @idea.save
      redirect_to design_document_idea_path(@idea)
    else
      render :action => :new
    end
  end

  def update
    # manually change title and pitch to avoid blowing away other things in @idea.details (featured demo, galleries, etc.)
    details = params[:idea][:details]
    @idea.details.title = details[:title]
    @idea.details.pitch = details[:pitch]

    current_tags = @idea.tags

    new_tags = []
    tags = params[:idea].delete("tags")
    tags = tags.split(',') if tags
    tags = sanitize_tags_array(tags)
    tags.each do |tag_name|
      tag = Tag.first_or_create(name: tag_name)
      new_tags << tag
    end

    removed_tags = current_tags - new_tags
    removed_tags.each do |removed_tag|
      removed_tag.taggings.each(&:destroy)
      removed_tag.destroy if removed_tag.taggings.none?
    end
    
    added_tags = new_tags - current_tags
    added_tags.each do |new_tag|
      @idea.tags << new_tag
    end
    
    if @idea.save
      redirect_to design_document_idea_path(@idea)
    else
      render :action => :edit
    end
  end

  def destroy
    # tags m:m relationship does not have a constraint defined (because it's using a gem), so we need to manually clear them
    # however, the :tags method is referencing some autoloaded information that needs to be clear out, otherwise destroy validation will fail
    # so after destroying tags, we have to reload the model
    # @idea.tag_taggings.destroy
    # @idea.reload
    # @idea.destroy

    # paranoid delete
    @idea.deleted = true
    @idea.deleted_by = current_user
    @idea.save
    
    flash[:notice] = "Idea successfully deleted. If this was a mistake, please contact GameSprout support: gs-support@schellgames.com"
    redirect_to :root
  end

  private
  def find_idea
    @idea = Idea.get(params[:id])
    
    # check in case an old url (back when this ran on mongo) is being used
    if @idea.nil? && !params[:id].nil?
      @idea = Idea.first(:moped_id => params[:id])
    end

    if @idea.deleted && params[:action] != "deleted"
      redirect_to deleted_idea_path(@idea)
    end
  end

  def must_own_idea
    unless @idea.owner == current_user
      render :nothing => true, :status => :unauthorized
    end
  end
end