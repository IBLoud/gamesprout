class HomeController < ApplicationController
  after_filter :remember_viewed_dashboard

  def index
    if user_signed_in?
      @my_ideas = current_user.owned_ideas.not_deleted
      
      total_follows = current_user.followed_ideas.ideas.all(:order => :updated_at.desc)
      @total_follow_count = total_follows.count
      @my_follows = total_follows.all(:limit => 6)
    else
      a_week_ago = DateTime.now - 1.week
      @featured_games = Idea.active.all(:updated_at.gte => a_week_ago).take(4)
      if @featured_games.size < 4
        @featured_games += Idea.active.take(4)
        @featured_games = @featured_games.uniq.take(4)
      end
    end

    render :layout => 'application'
  end

  private
  def has_unviewed?(collection)
    not collection.first(:updated_at.gt => current_user.view_date).nil?
  end  

  def count_unviewed(collection, options={})
    if collection.first.is_a?(Idea)
      posts = collection.collect { |idea| idea.posts }.flatten
    else
      posts = collection
    end

    # if we haven't been forced to go to the database yet, we should be able to count faster
    if posts.is_a?(DataMapper::Collection) && !options[:count_comments]
      unviewed = posts.count(:updated_at.gt => current_user.view_date)
    else
      unviewed = posts.select { |post| not current_user.viewed?(post) }.size
    end

    if options[:count_comments]
      comments = posts.collect { |p| p.comments }.flatten
      unviewed += comments.select { |comment| not current_user.viewed?(comment) }.size
    end

    return unviewed
  end

  def remember_viewed_dashboard
    if user_signed_in?      
      current_user.last_viewed_dashboard_at = DateTime.now
      current_user.save
    end
  end

end