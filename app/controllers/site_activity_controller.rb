class SiteActivityController < ApplicationController
  def index
    @results = get_activity
  end

  def more
    results = get_activity(params[:page].to_i)
    rendered_html = render_to_string :partial => 'activity', :collection => results, :as => :item, :layout => false

    render :json => {
      html: rendered_html
    }
  end

  private
  def get_activity(page=1)
    items_per_page = 10
    offset = (items_per_page * page) - items_per_page

    sql = <<-eos
      (select id, created_at as timestamp, 'post' as type
      from posts)
      union all

      (select id, created_at, 'idea'
      from ideas)
      union all

      (select id, created_at, 'comment'
      from comments)
      union all

      (select id, created_at, 'goal'
      from goals)
      union all

      (select id, created_at, 'design_doc'
      from design_docs)
      union all

      (select id, updated_at, 'idea_details'
      from idea_details)

      order by timestamp desc
      limit ?
      offset ?
    eos

    results = repository(:default).adapter.select(sql, items_per_page, offset)
    results = results.map do |struct|
      klass = struct.type == "idea_details" ? IdeaDetails : struct.type.classify.constantize
      model = klass.get(struct.id)
      {
        info: struct,
        model: model
      }
    end
  end
end