require 'dm-rails/middleware/identity_map'
class ApplicationController < ActionController::Base
  use Rails::DataMapper::Middleware::IdentityMap
  protect_from_forgery

  after_filter :track_visit
  before_filter :store_location, :if => :format_html?
  # ------------------------------ devise/warden hooks ------------------------------
  
  # if the user has just signed in or out, remember the url for forum sign in/out
  before_filter -> {
    if session[:just_signed_in]
      client_id = Rails.configuration.vanilla_forum_client_id
      @forum_sso_url = "#{Rails.configuration.vanilla_forum_url}/entry/jsconnect?client_id=#{client_id}"
    elsif params.has_key? :just_signed_out
      @forum_sso_url = "#{Rails.configuration.vanilla_forum_url}/entry/signout"
    end

    session.delete :just_signed_in
  }

  def after_sign_in_path_for(user)
    store_location = session[:return_to]
    clear_stored_location
    path = (get_after_sign_in_redirect || ((store_location.nil?) ? nil : store_location.to_s) || root_path)
    
    session[:just_signed_in] = true
    path
  end

  def after_sign_out_path_for(user)
    session.delete :just_signed_in # just in case
    
    # Note: so it always looks like it would be better to just use session here, but you CANNOT, because session is CLEARED after this point
    # in the signing out lifecycle, so you will lose whatever information you try to put in. But it's safe to use the query string, because
    # we currently do not try to take you back anywhere except the root_path after signing out.
    root_path + '?just_signed_out=true'
  end
  # ------------------------------ end devise/warden hooks ------------------------------

  def user_must_be_admin!
    unless user_signed_in? && current_user.admin?
      flash[:alert] = "Permission denied."
      redirect_to :root
    end
  end

  def must_be_signed_in!
    unless user_signed_in?
      respond_to do |format|
        format.html { render :file => "public/401.html", :status => :unauthorized, :layout => false }
        format.json { render :nothing => true, :status => :unauthorized }
      end
    end
  end

  def must_be_signed_in_with_redirect
    unless user_signed_in?
      flash[:alert] = "You need to sign in or sign up before continuing."
      redirect_to user_session_path
    end
  end

  # make sure there are only 6 tags; make sure each tag is max 15 characters; always downcase; strip whitespace; only keep unique tags
  def sanitize_tags_array(tags_array)
    tags_array = tags_array.take(6).collect do |tag|
      tag[0..14].strip.downcase
    end.uniq
  end

  def set_after_sign_in_redirect(path)
    session[:after_sign_in_redirect] = path
  end

  def get_after_sign_in_redirect
    redirect = session.delete :after_sign_in_redirect
  end

  def track_visit
    if user_signed_in? && session[:visit_tracked] != true
      current_user.visit_count = current_user.visit_count + 1
      current_user.last_visit_at = Time.zone.now
      current_user.last_visit_ip = request.ip

      if current_user.save
        session[:visit_tracked] = true
      end
    end
  end

  def format_html?
    if request.nil? || request.format.nil?
      return false
    end
    request.format.html?
  end

  def store_location
    if request.fullpath.eql? new_user_session_path
      return session[:return_to]
    end
    if (request.request_method.eql? "GET") && !(request.fullpath.split('/')[1].eql? "users")
      session[:return_to] = request.fullpath
    end
  end

  def clear_stored_location
    session[:return_to] = nil
  end
end