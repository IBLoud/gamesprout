class UserProfilesController < ApplicationController
  before_filter :user_must_be_admin!, :only => :index

  def show
    if params[:id]
      # find by id OR username
      @user = User.find_by_id_or_username params[:id]
    elsif user_signed_in?
      @user = current_user
    else
      redirect_to new_user_session_path
    end

    @user.user_details ||= UserDetails.new
    @followed_ideas = @user.followed_ideas.ideas.all(:deleted => false, :order => :created_at)
  end

  def search
    find_by = params[:find_by]

    user = User.find_by_id_or_username(find_by)

    if user.nil?
      user = User.first(email: find_by)
    end

    render :json => {
      user: user
    }
  end
end