class DashboardController < ApplicationController
  HOW_MANY_FEED_POSTS = 5

  # before_filter :must_be_signed_in!
  # after_filter :mark_viewed, :except => :more

  before_filter :must_be_signed_in!
  before_filter :set_view_date
  before_filter :get_my_ideas, :only => [:counts_for_navigation, :counts_for_owned_ideas]
  before_filter :get_my_follows, :only => [:counts_for_navigation, :counts_for_followed_ideas]
  before_filter :get_mentions, :only => :counts_for_navigation

  def counts_for_navigation
    def count(collection)
      collection.count(:updated_at.gt => @view_date)
    end

    # -- count up the number of unviewed posts/comments      
    unviewed = {
      users_ideas: count(@my_ideas),
      followed_ideas: count(@my_follows),
      posts_made_or_commented_on: count(Post.not_removed.posts_made_or_commented_on_by(current_user)),
      news: count(NewsItem.published),
      mentions: @mentions.count(:mentioned_at.gt => @view_date)
    }

    render :json => unviewed
  end

  def counts_for_owned_ideas    
    render :json => count_unviewed(@my_ideas)
  end

  def counts_for_followed_ideas
    render :json => count_unviewed(@my_follows)
  end

  def users_ideas
    @comments_count_for_viewed = true
    
    posts = Post.not_removed.all(Post.idea.owner_id => current_user.id) # posts for ideas owned by the curent user
    compile_feed_for posts
  end

  def followed_ideas
    posts = Post.not_removed.all(Post.idea_id => current_user.followed_ideas.collect { |follow| follow.idea_id })
    compile_feed_for posts
  end

  def posts_made_or_commented_on
    @comments_count_for_viewed = true
    
    posts = Post.not_removed.posts_made_or_commented_on_by(current_user) # made or commented on
    compile_feed_for posts
  end

  def news
    @feed = NewsItem.published
    @render_as = :news_item
    
    paginate_feed
    render_feed
  end

  def mentions
    @feed = get_mentions
    @render_as = :mention

    paginate_feed
    render_feed
  end

  def more
    action_name = params[:what].to_sym
    self.send action_name if self.public_methods.include?(action_name)
  end

  private
  def compile_feed_for(posts)
    @feed = posts.all(:order => :updated_at.desc)

    paginate_feed
    render_feed
  end

  def paginate_feed
    page = params[:page] ? params[:page].to_i : 1
    min = HOW_MANY_FEED_POSTS * (page - 1)
    max = HOW_MANY_FEED_POSTS * page
    @feed = @feed[min...max]
  end

  def render_feed
    if @render_as
      rendered_results = render_to_string :partial => @render_as.to_s, :collection => @feed, :as => @render_as, :layout => false
    else
      rendered_results = render_to_string :partial => 'feed', :collection => @feed, :as => :post, :layout => false
    end
    
    render :json => {
      html: rendered_results,
      more_url: params[:action]
    }
  end

  private
  def set_view_date
    if date_string = params["view_date"]
      @view_date = DateTime.parse(date_string)
    else
      raise "view_date not set"
    end
  end  

  def count_unviewed(collection)
    unviewed = {}
    collection.each do |idea|
      unviewed[idea.id] = Post.count(:idea_id => idea.id, :updated_at.gt => @view_date)
    end

    return unviewed
  end

  def get_my_ideas
    @my_ideas = current_user.owned_ideas.not_deleted
  end

  def get_my_follows
    total_follows = current_user.followed_ideas.ideas.all(:order => :updated_at.desc)
    total_follow_count = total_follows.count    
    @my_follows = total_follows.all(:limit => 6)
  end

  def get_mentions
    @mentions = Mention.all(:mentioned_user_id => current_user.id, :mentioned_at.lt => DateTime.now, :order => :mentioned_at.desc)
  end
end