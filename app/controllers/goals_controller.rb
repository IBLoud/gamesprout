class GoalsController < ApplicationController
  before_filter :must_be_signed_in!
  before_filter :find_goal, :except => [:new, :create, :resolve]
  before_filter :find_idea, :except => [:resolve]
  before_filter :user_must_own_idea!, :except => [:commit_to, :still_working_on, :resolve]

  def new
    @goal = Goal.new
  end

  def create
    @goal = Goal.new(params[:goal])
    @idea.goals << @goal

    if @goal.save
      @idea.touch
      redirect_to @idea
    else
      render :action => :new
    end
  end

  def edit    
  end

  def update
    @goal.attributes = params[:goal]

    if @goal.save
      @goal.idea.touch
      redirect_to @idea
    else
      render :action => :new
    end
  end

  def destroy
    if @goal.destroy
      flash[:notice] = "Goal successfully deleted!"
      redirect_to @goal.idea
    else
    end
  end

  def resolve
    post = Post.get(params[:post_id])
    @goal = Goal.get(params[:goal_to_resolve])
    @idea = @goal.idea

    # -- verify permissions
    user_must_own_idea!
    
    @goal.completed = true
    @goal.completed_at = DateTime.now
    @goal.completed_by_post = post

    if @goal.save
      @goal.idea.touch
      redirect_to @idea
    end
  end

  private
  def find_goal
    @goal = Goal.get(params[:id])
  end

  def find_idea
    @idea = @goal.nil? ? Idea.get(params[:idea_id]) : @goal.idea
  end

  def user_must_own_idea!
    unless @idea.owner == current_user
      flash[:alert] = "Permission denied."
      redirect_to @goal.idea
    end
  end
end