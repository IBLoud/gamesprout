class AdminController < ApplicationController
  before_filter :authenticate_user!
  before_filter :user_must_be_admin!

  def index
  end

  def users
    range = (Date.parse("2013-5-28")..Date.current)
    @result = range.map { |d| d.to_datetime }.map do |d| 
      # (User.count(:created_at => {'$gte' => d, '$lt' => (d + 1.day - 1.second)}))
      { d.to_date => User.count(:created_at.gte => d, :created_at.lt => (d + 1.day - 1.second)) }
    end
  end

  def user_metrics
    @user_count = User.count
    @day_range = (Time.now - (60 * 60 * 24))..Time.now
    @week_range = (Time.now - (60 * 60 * 24 * 7))..Time.now
    @month_range = (Time.now - (60 * 60 * 24 * 30))..Time.now
    @day_new_users = 0
    @day_active_users = 0
    @day_retained_users = 0
    @week_new_users = 0
    @week_active_users = 0
    @week_retained_users = 0
    @month_new_users = 0
    @month_active_users = 0
    @month_retained_users = 0
    User.each do |u|
      # new users & retained
      if @day_range.cover? u.created_at
        @day_new_users += 1
        if !(u.last_visit_at.nil?)
          if @day_range.cover? u.last_visit_at
            @day_retained_users += 1
          end
        end
      end
      if @week_range.cover? u.created_at
        @week_new_users += 1
        if !(u.last_visit_at.nil?)
          if @day_range.cover? u.last_visit_at
            @week_retained_users += 1
          end
        end
      end
      if @month_range.cover? u.created_at
        @month_new_users += 1
        if !(u.last_visit_at.nil?)
          if @day_range.cover? u.last_visit_at
            @month_retained_users += 1
          end
        end
      end
      # active users
      if !(u.last_visit_at.nil?)
        if @day_range.cover? u.last_visit_at
          @day_active_users += 1
        end
        if @week_range.cover? u.last_visit_at
          @week_active_users += 1
        end
        if @month_range.cover? u.last_visit_at
          @month_active_users += 1
        end
      end
    end
    @other_metrics = {} # MapReduce::UserMetrics.build
  end

  def set_homepage_features
    hf = HomepageFeatures.new

    if !Idea.where(:id => params["admin"]["idea1"]).empty?
      hf.idea1 = params["admin"]["idea1"]
    end
    if !Idea.where(:id => params["admin"]["idea2"]).empty?
      hf.idea2 = params["admin"]["idea2"]
    end
    if !Idea.where(:id => params["admin"]["idea3"]).empty?
      hf.idea3 = params["admin"]["idea3"]
    end
    if !Idea.where(:id => params["admin"]["idea4"]).empty?
      hf.idea4 = params["admin"]["idea4"]
    end

    demo1i = Idea.where(:id => params["admin"]["demo1_idea"]).first
    if !demo1i.nil? && !demo1i.prototypes.where(:id => params["admin"]["demo1"]).empty?
      hf.demo1_idea = params["admin"]["demo1_idea"]
      hf.demo1 = params["admin"]["demo1"]
    end
    demo2i = Idea.where(:id => params["admin"]["demo2_idea"]).first
    if !demo2i.nil? && !demo2i.prototypes.where(:id => params["admin"]["demo2"]).empty?
      hf.demo2_idea = params["admin"]["demo2_idea"]
      hf.demo2 = params["admin"]["demo2"]
    end
    demo3i = Idea.where(:id => params["admin"]["demo3_idea"]).first
    if !demo3i.nil? && !demo3i.prototypes.where(:id => params["admin"]["demo3"]).empty?
      hf.demo3_idea = params["admin"]["demo3_idea"]
      hf.demo3 = params["admin"]["demo3"]
    end
    
    hf.save

    redirect_to homepage_features_path
  end

  def generate_email_list
    @users = User.all
    @date = DateTime.now
    list = render_to_string('generate_email_list', :layout => false)

    send_data list, :filename => "gamesprout_email_list_#{@date.year}_#{@date.month}_#{@date.day}"
  end

  def new_admin
  end

  def adminify
    user_id = params[:user_id].first
    user = User.get(user_id)

    user.admin = !user.admin # toggle admin status
    if user.save
      msg = user.admin? ? 'adminified' : 'de-adminified'
      flash[:notice] = "User #{user.username} has been #{msg}."
    else
      flash[:alert] = "Something went wrong!"
    end

    redirect_to new_admin_path
  end

  def news_alert
    @news_alert = NewsAlert.first || NewsAlert.new
  end

  def set_news_alert
    @news_alert = NewsAlert.first || NewsAlert.new
    @news_alert.text = params[:news_alert][:text]
    @news_alert.stop_at = params[:news_alert][:stop_at].empty? ? nil : params[:news_alert][:stop_at]

    @news_alert.save

    redirect_to :news_alert
  end

  def kill_news_alert
    @news_alert = NewsAlert.first
    @news_alert.destroy unless @news_alert.nil?

    redirect_to :news_alert
  end  

end
