class NotificationSettingsController < ApplicationController
  before_filter :authenticate_with_redirect!

  def edit
  end

  def update
    checkbox_value = params[:notify]
    current_user.notify = checkbox_value == "true"

    if current_user.save
      flash[:notice] = "Notification settings saved!"
    else
      flash[:alert] = "There was an error saving your notification settings..."
    end

    redirect_to update_notification_settings_path
  end

  private
  def authenticate_with_redirect!
    unless user_signed_in?
      set_after_sign_in_redirect notification_settings_path
      authenticate_user!
    end
  end
end