class UserApiController < ApplicationController
  before_filter :must_be_signed_in!

  def save_selection
    group = params["group"]
    item = params["item"]

    selection = current_user.selections.first_or_create(group: group)
    selection.value = item
    selection.save

    render :nothing => true, :status => :ok
  end

  def save_viewed
    render :json => {
      params: params
    }
  end  

  def search_users
    search_str = params[:search_str]
    search_str = search_str.strip if search_str

    if search_str && !search_str.empty?
      search_str += '%' # specific syntax for LIKE that basically makes this mean "starts with", eg. "mi%" matches "mike"
      post_id = params[:post_id] || nil

      # here we want to prioritize names that match the search that are also involved in the current conversation
      sql = <<-eos
        select Coalesce(A.username, B.username) as username, Coalesce(A.in_conversation, B.in_conversation) as in_conversation
        from
          (select distinct users.username, true as in_conversation 
          from comments
          join users on users.id=comments.user_id 
          where post_id=? and username ILIKE ?) as A

        full outer join

          (select users.username, false as in_conversation
          from users
          where username ILIKE ?) as B

        on A.username=B.username

        order by in_conversation desc
        limit 10
      eos

      usernames = repository(:default).adapter.select(sql, post_id, search_str, search_str).collect { |result| result["username"] }
    else
      usernames = []
    end

    render :json => {
      usernames: usernames
    }
  end
end