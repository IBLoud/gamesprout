class UserDetailsController < ApplicationController
  before_filter :authenticate_with_redirect!

  def edit
    @user_details = current_user.user_details || UserDetails.new
  end

  def update
    current_user.user_details ||= UserDetails.new(params[:user_details])
    saved = current_user.user_details.new? ? current_user.user_details.save : current_user.user_details.update(params[:user_details])

    if saved
      flash[:notice] = "Your details have been saved!"
      redirect_to update_user_details_path
    else
      @user_details = current_user.user_details
      render :action => :edit
    end
  end

  private
  def authenticate_with_redirect!
    unless user_signed_in?
      set_after_sign_in_redirect user_details_path
      authenticate_user!
    end
  end
end