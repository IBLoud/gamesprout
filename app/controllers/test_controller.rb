class TestController < ApplicationController
  def upload_files
  end

  def upload_completed
    file = UploadedFile.new
    file.url = URI.unescape params[:url] # note that urls, paths and filenames from Amazon are escaped, so we need to unescape them
    file.filepath = URI.unescape params[:filepath]
    file.filename = URI.unescape params[:filename]
    file.filesize = params[:filesize]
    file.filetype = params[:filetype]
    file.s3_unique_id = params[:unique_id]

    # file.demo = @demo
    file.save

    render :nothing => true, :status => :ok
  end
end
