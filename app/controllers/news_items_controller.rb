class NewsItemsController < ApplicationController
  before_filter :user_must_be_admin!, :except => [:index, :show]
  before_filter :get_news_item, :only => [:show, :edit, :update, :destroy]

  def index
    @news_items = NewsItem.published
  end

  def admin
    @news_items = NewsItem.latest
    render "admin_index"
  end

  def show
    if @news_item.pub_date > Time.zone.now
      unless user_signed_in? && current_user.admin?
        redirect_to :root, alert: "Permission denied."
      end
    end
  end

  def new
    @news_item = NewsItem.new
  end

  def edit
  end

  def create
    @news_item = NewsItem.new(params[:news_item])
    @news_item.pub_date = DateTime.now if @news_item.publish_now?
    @news_item.posted_by = current_user

    Mention.check_for_mentions(@news_item)

    if @news_item.save
      redirect_to @news_item, notice: 'news item was successfully created.'
    else
      render :action => :new
    end
  end

  def update
    @news_item.attributes = params[:news_item]
    @news_item.pub_date = Time.zone.now if @news_item.publish_now?
    @news_item.edited_by = current_user

    Mention.update_mentions(@news_item)

    if @news_item.save
      redirect_to @news_item, notice: 'news item was successfully updated.'
    else
      render :action => :edit
    end
  end

  def destroy
    @news_item.destroy
    redirect_to news_items_url
  end

  private
  def get_news_item
    @news_item = NewsItem.get(params[:id])
  end
end
