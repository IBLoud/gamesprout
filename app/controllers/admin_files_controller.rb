class AdminFilesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :user_must_be_admin!

  def unchecked    
    bucket = AWS.s3.buckets.find { |bucket| bucket.name == S3DirectUpload.config.bucket }
    files_in_bucket = bucket.objects.to_a
    
    kvp = {} # we want the keys to match the way they're stored in our db to prep for the query, but we still need to reference the object itself
    files_in_bucket.each { |file| kvp["/#{S3DirectUpload.config.bucket}/#{file.key}".gsub(' ', '+')] = file }

    files_in_db = UploadedFile.all()
    @known_files = files_in_db.all(:filepath => kvp.keys, :checked => false, :order => :created_at.desc)
    @unknown_files = kvp.select { |key| not files_in_db.collect { |file| file.filepath }.include?(key) }.keys
  end

  def checked
    @checked_files = UploadedFile.all(:checked => true, :order => :created_at.desc)
  end

  def mark_checked
    success = false

    if params[:file_type] == 'known'
      file = UploadedFile.get params[:id]
      file.checked = true
      
      success = file.save
    
    else
      file = UploadedFile.new
      file.url = params[:url]
      file.filepath = params[:key]
      file.checked = true
      
      success = file.save
    end

    render :json => {success: success}
  end
end