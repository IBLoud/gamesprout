class IdeasCollectionController < ApplicationController
  def browse
    @results = Idea.not_deleted
    @order_by = {:newly_added => 'Newly added', :popular => 'Popular', :active => 'Active', :recently_updated => 'Recently updated'}

    apply_browse_params! :default_scope => :popular
  end

  def help_out
    # -- ordering
    @order_by = {:newly_added => 'Latest Idea', :popular => 'Popular Idea', :active => 'Active Idea', :latest_goals => 'Latest goals'}
    if params.has_key?(:order_by)
      @order_scope = params[:order_by].to_sym 
    else
      @order_scope = :latest_goals
    end

    # -- paging
    paging = get_paging_info

    # -- filter by what_kind
    @filter_for = (params.has_key?(:filter_for) && !params[:filter_for].empty?) ? [params[:filter_for].to_sym] : Goal.what_kind.options[:flags]
    what_kind = Goal.what_kind.options[:flags] & @filter_for

    if what_kind.empty? # if you unselect them all just don't even go to the database
      @results = []
    else
      # -- filter by tags
      @tags = params[:tags].split(',') if (params.has_key?(:tags) && !params[:tags].empty?)
      @results = Idea.not_deleted.with_help_needed(how_many: @items_per_page, offset: paging[:offset], order_by: @order_scope, what_kind: what_kind, tags: @tags)
    end
  end

  def play
    @results = Post.prototypes.not_removed
    @order_by = {:newly_added => 'Newly added', :active => 'Active'}

    apply_browse_params! :default_scope => :newly_added
  end

  def search
    # -- set order_by options
    @order_by = {:relevance => 'Relevance', :newly_added => 'Newly Added', :recently_updated => 'Recently updated', :popular => 'Popular', :active => 'Active'}

    query = params[:query].strip.gsub(/[^A-Za-z0-9 ]/, '') # strip out non-alphanumerics
    @results = Idea.search_query(query)
    
    apply_browse_params! :default_scope => :relevance
  end

  def more
    rendered_results = nil

    case params[:browsing_for]
    when 'browse'
      browse
      partial = 'design_game_card'
    when 'help_out'
      help_out
      partial = 'build_game_card'
    when 'play'
      play
      partial = 'play_game_card'
      rendered_results = render_to_string :partial => partial, :collection => @results, :as => :prototype, :layout => false
    when 'search'
      search
      partial = 'design_game_card'
    end

    if rendered_results.nil?
      rendered_results = render_to_string :partial => partial, :collection => @results, :as => :idea, :layout => false
    end
    
    # -- 
    render :json => {
      html: rendered_results
    }
  end

  private
  # apply order_by and tag filter
  def apply_browse_params!(opts={})
    # -- Order as requested (must happen before tagging)
    apply_order_scope! opts
    
    # -- Filter by tags
    filter_by_tags! opts

    # -- Paginate
    paginate! opts
  end

  def filter_by_tags!(opts={})
    @tags = params[:tags].split(',') if (params.has_key?(:tags) && !params[:tags].empty?)
    if @tags
      @tags.each do |tag|
        if @results.model == Idea
          @results = @results & @results.tagged_with(tag)
        elsif @results.model == Post
          @results = @results & @results.all(:idea => {:taggings => {:tag => {:name => tag}}})
        end
      end
    else
      @tags = []
    end
  end

  def apply_order_scope!(opts={})
    @order_scope = params.has_key?(:order_by) ? params[:order_by].to_sym : opts[:default_scope]

    case @order_scope
    when :recently_updated
      @results = @results.all(:order => :updated_at.desc)
    when :newly_added
      @results = @results.all(:order => :created_at.desc)
    when :active
      if @results.model == Idea        
        @results = @results.all(:order => :activity_count.desc)
      elsif @results.model == Post
        @results = @results.all(:order => :comment_count.desc)
      end
    when :popular
      @results = @results.all(:order => :yes_count.desc)
    when :order_by_relevance
      @results = @results.all()
    end
  end

  def get_paging_info
    @items_per_page = 9
    page = params.has_key?(:page) ? params[:page].to_i : 1
    offset = @items_per_page * (page - 1)
    excluded_end = offset + @items_per_page

    {
      page: page,
      start: offset,
      offset: offset,
      excluded_end: excluded_end
    }
  end

  def paginate!(opts={})
    paging = get_paging_info
    @results = @results.to_a[paging[:start]...paging[:excluded_end]]
    
    # For some reason using db :offset and :limit caused duplicate items to appear
    # @results = @results.all(:offset => paging[:offset], :limit => @items_per_page)
    # @results = @results.take(@items_per_page)
  end
end