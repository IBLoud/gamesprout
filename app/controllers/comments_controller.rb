class CommentsController < ApplicationController
  before_filter :must_be_signed_in!
  before_filter :find_comment, :except => [:create]
  before_filter :check_edit_permissions, :only => [:edit, :update]

  def show
    @idea = @comment.post.idea
  end

  def create
    @comment = Comment.new(params[:comment])
    @comment.user = current_user
    @comment.is_thanks = @comment.contains_thanks?

    success = @comment.save

    @comment.post.idea.recount_activity!
    @comment.post.idea.save

    @comment.post.comment_count = @comment.post.comments.count
    @comment.post.touch

    Mention.check_for_mentions(@comment)
    
    respond_to do |format|
      format.html { redirect_to @comment.post.idea, :anchor => @comment.id }
      format.js do 
        render :json => { 
          html: success ? render_to_string(:partial => 'shared/comment', :layout => false, :locals => {comment: @comment}) : nil,
          success: success
        }
      end
    end
  end

  def edit        
  end

  def update
    if @comment.update(params[:comment])
      @comment.post.update(comment_count: @comment.post.comments.count)
      Mention.update_mentions(@comment)
      
      redirect_to idea_path(@comment.post.idea, :anchor => @comment.id)
    else
      render :edit
    end
  end

  def destroy
    if view_context.user_can_delete_comment?(@comment)
      @comment.update(removed: true, removed_by_id: current_user.id)
      @comment.post.comment_count = @comment.post.comments.count - 1
      @comment.post.save

      redirect_to idea_path(@comment.post.idea, :anchor => @comment.id)
    else
      flash[:alert] = "Permission denied."
      redirect_to idea_path(@comment.post.idea, :anchor => @comment.id)
    end
  end

  private
  def find_comment
    @comment = Comment.get(params[:id])
  end

  def check_edit_permissions
    unless @comment.user == current_user || current_user.admin?
      flash[:alert] = "Permission denied."
      redirect_to idea_path(@comment.post.idea, :anchor => @comment.id)
    end
  end
end