class IdeaDetailsController < ApplicationController
  before_filter :must_be_signed_in!
  before_filter :get_idea
  before_filter :must_own_idea

  def edit_details
    [:title, :pitch, :platform, :mechanics, :aesthetics, :favorite].each do |field|
      unless params[field].nil? || params[field].empty?
        method = (field.to_s + "=").to_sym # generate method name that includes equals, eg. :title=
        @idea.details.send method, params[field]
      end
    end

    @idea.save if @idea.dirty?

    redirect_to design_document_idea_path
  end

  def edit_banner_image
    if params[:url] && !params[:url].strip.empty?
      @idea.details.banner_image = params[:url].strip
      @idea.save
    end

    redirect_to design_document_idea_path
  end

  def add_gallery_image
    image = ImageLink.new
    image.url = params[:url]
    
    @idea.details.image_links << image
    @idea.save

    redirect_to design_document_idea_path
  end

  def remove_gallery_image
    image = @idea.details.image_links.get(params[:image])
    image.destroy

    redirect_to design_document_idea_path
  end

  def add_gallery_demo
    if params[:demo]
      demo = @idea.posts.prototypes.get(params[:demo])
      demo.in_gallery = true
      demo.save
    end

    redirect_to design_document_idea_path
  end

  def remove_gallery_demo
    demo = @idea.demo_gallery.get(params[:demo])
    demo.in_gallery = false
    demo.save

    redirect_to design_document_idea_path
  end

  def select_featured_demo
    # remove current featured (if any)
    if old_featured = @idea.posts.prototypes.first(:featured_demo => true)
      old_featured.featured_demo = false
      old_featured.save
    end

    # set new featured (unless we just wanted to remove the featured entirely)
    if params[:commit] != "Remove Featured Demo" && params[:demo]
      new_featured = @idea.posts.prototypes.get(params[:demo])
      new_featured.featured_demo = true
      new_featured.save
    end

    redirect_to design_document_idea_path
  end

  private
  def get_idea
    @idea = Idea.get(params[:id])
  end

  def must_own_idea
    unless @idea.owner == current_user
      flash[:alert] = "Permission denied."
      redirect_to design_document_idea_path(@idea)
    end
  end
end