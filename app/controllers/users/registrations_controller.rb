class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :fix_password_confirm, :only => [:create, :update]

  protected
  def after_update_path_for(user)
    edit_user_registration_path
  end

  def after_inactive_sign_up_path_for(resource)
    thanks_signup_path
  end

  def after_sign_up_path_for(user)
    thanks_signup_path
  end

  def after_confirmation_path_for(user)
    thanks_signup_path
  end

  def fix_password_confirm
    # we don't want to force password confirmation, so just set it before creating
    params[:user][:password_confirmation] = params[:user][:password] if params[:user][:password]
  end
end