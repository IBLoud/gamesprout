class YesesController < ApplicationController
  before_filter :must_be_signed_in_with_redirect
  before_filter :get_idea
  before_filter :must_not_own_idea!

  def create
    Yes.create(idea_id: @idea.id, user_id: current_user.id)
    @idea.update(yes_count: @idea.yeses.count)
    redirect_to @idea
  end

  def destroy
    Yes.first(idea_id: @idea.id, user_id: current_user.id).destroy
    @idea.update(yes_count: @idea.yeses.count)
    redirect_to @idea
  end

  private
  def get_idea
    @idea = Idea.get(params[:id])
  end

  def must_not_own_idea!
    if @idea.owner == current_user
      flash[:alert] = "Cannot yes your own idea."
      redirect_to @idea
    end
  end
end