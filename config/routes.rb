GamefactoryTest::Application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "users/registrations"}

  # catch attempts to access /users/password with a get (in cases where you mess up on password reminder and then try to access url directly)
  get '/users/password' => redirect('/users/password/new')

  # home routes
  root :to => 'home#index'
  get '/feed' => 'home#feed'

  # -- legacy redirects (from the previous architecture/db)
  get '/ideas/build', to: redirect { |params, request| "/ideas/browse?#{request.query_string}" }

  # dashboard
  scope '/dashboard' do
    get '/counts_for_owned_ideas/(:view_date)' => 'dashboard#counts_for_owned_ideas'
    get '/counts_for_followed_ideas/(:view_date)' => 'dashboard#counts_for_followed_ideas'
    get '/counts_for_navigation/(:view_date)' => 'dashboard#counts_for_navigation'

    get '/users_ideas' => 'dashboard#users_ideas'
    get '/followed_ideas' => 'dashboard#followed_ideas'
    get '/news' => 'dashboard#news'
    get '/posts_made_or_commented_on' => 'dashboard#posts_made_or_commented_on'
    get '/mentions' => 'dashboard#mentions'
    get '/more/(:what)/(:page)' => 'dashboard#more'    
  end

  # custom user routes
  get '/profile', to: 'user_profiles#show', :as => :my_profile
  post '/profile/search', :to => 'user_profiles#search', :as => :search_for_user
  get '/profile(/:id)', to: 'user_profiles#show', :as => :user, :constraints => { :id => /.*/ }
  get '/notification_settings' => 'notification_settings#edit'
  post '/notification_settings' => 'notification_settings#update', :as => :update_notification_settings
  get '/user_details' => 'user_details#edit'
  post '/user_details' => 'user_details#update', :as => :update_user_details
  post '/save_selection' => 'user_api#save_selection'
  post '/save_view' => 'user_api#save_viewed'
  post '/search_users' => 'user_api#search_users'

  # static pages
  get '/about' => 'pages#about'
  get '/about_team' => 'pages#about_team'
  get '/terms' => 'pages#terms'
  get '/faq' => 'pages#faq'
  get '/funding_info' => 'pages#funding_info'
  get '/volunteers' => 'pages#volunteers'
  get '/dev_center' => 'pages#dev_center'
  get '/dev_center_learn' => 'pages#dev_center_learn'
  get '/dev_center_build' => 'pages#dev_center_build'
  get '/community' => 'pages#community'
  get '/help' => 'pages#help'
  get '/news' => 'pages#news' # list of all news items, seperate from the dashboard list of news items
  get '/thanks_signup' => 'pages#thanks_signup', :as => 'thanks_signup'
  get '/about_reputation(/:user)' => 'pages#reputation', :as => 'about_reputation'

  # admin pages
  scope '/admin' do
    get '' => 'admin#index', as: 'admin'
    get 'top_voted' => 'admin#top_voted'
    get 'users' => 'admin#users'
    get 'user_metrics' => 'admin#user_metrics'
    get 'homepage_features' => 'admin#homepage_features'
    get 'generate_email_list' => 'admin#generate_email_list'
    get 'idea_metrics' => 'admin#idea_metrics'

    post 'set_homepage_features' => 'admin#set_homepage_features'

    get 'new_admin', :controller => 'admin'
    post 'adminify', :controller => 'admin'

    get 'news_alert', :controller => 'admin'
    post 'set_news_alert', :controller => 'admin'
    put 'set_news_alert', :controller => 'admin'
    post 'kill_news_alert', :controller => 'admin'

    # file moderation
    get 'files' => 'admin_files#unchecked', :as => 'admin_files'
    get 'files/checked' => 'admin_files#checked', :as => 'admin_files_checked'
    post 'files/mark_checked' => 'admin_files#mark_checked', :as => 'admin_files_mark_checked'
  end

  resources :comments
  resources :posts do
    member do
      post 'vote'
      get 'more_comments/(:page)' => 'posts#more_comments', :as => :more_comments
      
      get 'upload_files' => 'posts#upload_files'
      post 'upload_completed' => 'posts#upload_completed'
    end
  end


  resources :ideas, :except => :index do
    resources :goals
    resources :design_docs

    collection do
      get 'browse' => 'ideas_collection#browse'
      get 'play' => 'ideas_collection#play'
      get 'help_out'=> 'ideas_collection#help_out'
      get 'search' => 'ideas_collection#search'

      get '/(:browsing_for)/more/(:page)' => 'ideas_collection#more', :as => 'more'
    end

    member do
      # -- special page for deleted ideas
      get 'deleted' => 'ideas#deleted'
      
      # -- design document routes
      get 'design_document'
      post 'add_gallery_image' => 'idea_details#add_gallery_image'
      post 'remove_gallery_image' => 'idea_details#remove_gallery_image'
      post 'edit_banner_image' => 'idea_details#edit_banner_image'
      post 'edit_details' => 'idea_details#edit_details'
      post 'add_gallery_demo' => 'idea_details#add_gallery_demo'
      post 'remove_gallery_demo' => 'idea_details#remove_gallery_demo'
      post 'select_featured_demo' => 'idea_details#select_featured_demo'

      # -- checklist
      get 'checklist'
      post 'checklist/toggle_task' => 'ideas#toggle_checklist_task'

      # -- show more posts
      get 'more_posts/(:page)' => 'ideas#more_posts'

      # -- follow/unfollow
      post 'follow' => 'follows#create'
      delete 'unfollow' => 'follows#destroy'

      # -- yes/un-yes
      post 'yes' => 'yeses#create'
      delete 'no' => 'yeses#destroy'
    end
  end

  # -- resolve a goal using a post
  post '/resolve_goal' => 'goals#resolve'

  resources :feedback, :except => [:destroy] do
    member do
      post 'change_logged'
    end

    collection do
      get 'thank_you'      
    end
  end

  resources :news_items do
    collection do
      get 'admin' => 'news_items#admin'
    end
  end

  # -- alternate site activity view
  get 'site_activity' => 'site_activity#index', :as => 'site_activity'
  get 'site_activity/more/(:page)' => 'site_activity#more'

  # Vanilla forum single sign on using jsConnect
  get 'forum/single_sign_on' => 'vanilla_forum#single_sign_on'
  get 'forum' => 'vanilla_forum#embedded_forum'

  # Generic file upload (not tied to a post directly, just to a user)
  get 'files/(:user)/all' => 'files#all', :as => 'all_files'
  get 'files/(:user)' => 'files#index', :as => 'files'
  post 'files/upload_complete' => 'files#upload_complete'  

  # test s3 direct file upload setup
  # get 'upload_files' => 'test#upload_files'
  # post 'upload_completed' => 'test#upload_completed'  
end
