# This YAML file has all of the items used on the idea checklist page. 
#

en:
  TaskGroups:
    - TaskGroupID: TaskGroup00
      Name: "Create and Describe your Idea"
      TaskGroupLongDesc: "Now that you’ve submitted your idea to GameSprout, start describing and elaborating on the details of your game idea. Linking to references (games, art, music, books) is an incredibly helpful way to do just this."
      SubGroups:
        - Name: "General"
          Tasks:
            - TaskID: General00
              Name: "Idea Name (Initial)"
              LongDesc: "Choose a fitting name for your idea. Don't worry, you can change this later."
            - TaskID: General01
              Name: Thumbnail Picture (Initial)
              LongDesc: "Choose an image that best represents your game. It's important to choose something that both catches the eye and communicates your game."
            - TaskID: General02
              Name: Your Favorite Thing (Initial)
              LongDesc: "What is it about this idea that makes you the most excited?"
            - TaskID: General03
              Name: Game Categories
              LongDesc: What categories or genres does this game fall under?
        - Name: "Design"
          Tasks:
            - TaskID: Design00
              Name: "Core Mechanics Description (Initial)"
              LongDesc: "What is the core of your game? How does the player interact? What are the actions that the player can take?"
            - TaskID: Design01
              Name: "List of Key Features (Initial)"
              LongDesc: "If you were describe the features of your game idea on the back of a box, what would they be? If your game is similar to others, how is yours different?"
            - TaskID: Design02
              Name: "Idea Pitch (Initial)"
              LongDesc: "Try and summarize your game in a few sentences. Amanda wrote a great guide to crafting your pitch."
            - TaskID: Design03
              Name: "Similar Games and Media"
              LongDesc: "Does your game take any inspiration from existing games, movies or books? What are they? What elements of them are you incorporating?"
        - Name: "Art"
          Tasks:
            - TaskID: Art00
              Name: "Description of Overall Look and Feel"
              LongDesc: "What does the game look like? What is the tone of the game?"
            - TaskID: Art01
              Name: "Reference Images for Overall Look and Feel"
              LongDesc: "Post images that helps people reading your idea visualize what it looks like."
            - TaskID: Art02
              Name: "Description of Player's Avatar Look and Feel"
              LongDesc: "What does the player's character look like?"
            - TaskID: Art03
              Name: "Reference Images for the Player's Avatar"
              LongDesc: "Post images to helps people visualize what the player's character will look like."
            - TaskID: Art04
              Name: "Description of Other Characters/Elements Look and Feel"
              LongDesc: "What do the other characters and elements look like? This includes monsters, sidekicks, or any other critter in your game."
            - TaskID: Art05
              Name: "Reference Images for Other Characters/Elements Look and Feel"
              LongDesc: "Post images that help others visualize what the other game elements look like."
            - TaskID: Art06
              Name: "Description of Environment Look and Feel"
              LongDesc: "What does the world of the game look like? Why should the world look this way?"
            - TaskID: Art07
              Name: "Reference Images for Environment Look and Feel"
              LongDesc: "Post images that help others visualize the environment of your game."
        - Name: "Tech"
          Tasks:
            - TaskID: Tech00
              Name: "Hardware Platform"
              LongDesc: "What platform do you imagine this game being played on? How do players interact with your game? (Examples: controller, mouse/keyboard, touchscreen ...)"
        - Name: "Story"
          Tasks:
            - TaskID: Story00
              Name: "Story Summary (Initial)"
              LongDesc: "If your game has a story, what is the general premise? Who are the characters?"
        - Name: "Sound"
          Tasks:
            - TaskID: Sound00
              Name: "Description of Sound Design Mood and Feel"
              LongDesc: "How is sound used in your game? What is the tone of the game overall?"
            - TaskID: Sound01
              Name: "Sound Design References (Initial)"
              LongDesc: "Are there games or movies that inspired (or are reflective of) the sound design you’re imagining? Post video or audio clips to give everyone a better understanding!"
            - TaskID: Sound02
              Name: "Description of Music Mood and Feel"
              LongDesc: "What kind of music does this game have? What kind of emotional tone are you aiming for?"
            - TaskID: Sound03
              Name: Music References (Initial)
              LongDesc: "Find and post audio clips / music samples that best represent the style of music you’re going for."
    - TaskGroupID: TaskGroup01
      Name: "Move Forward on GameSprout"
      TaskGroupLongDesc: "With the basics of your idea now posted, it’s time to start proving your concept by creating a few prototypes."
      SubGroups:
        - Name: "Design"
          Tasks:
            - TaskID: Design04
              Name: "UI Description"
              LongDesc: "What does the UI of the game look like? What elements are needed? Are there other games with a similar UI?"
            - TaskID: Design05
              Name: "UI Wireframe (Initial - High Level)"
              LongDesc: "Post a mockup of what the in-game interface will look like. Reference images to other UIs are also helpful."
            - TaskID: Design06
              Name: "List of Key Features (Midway)"
              LongDesc: "As you continue to refine your game idea, be sure to update and re-examine your list of key features."
        - Name: "Art"
          Tasks:
            - TaskID: Art08
              Name: "Concept Art of Overall Look and Feel (Initial)"
              LongDesc: "Having described what the game looks like, the next step is to start seeing it come to life. Post general concept art to illustrate how the game looks and feels."
            - TaskID: Art09
              Name: "Concept Art of Player’s Avatar (Intial)"
              LongDesc: "Post concept art to help others understand what the player’s avatar should look like."
            - TaskID: Art10
              Name: "Concept Art of Other Characters/Elements Look and Feel (Initial)"
              LongDesc: "Post concept art of the other characters, enemies, or interactive objects within the game."
            - TaskID: Art11
              Name: "Concept Art of Environment Look and Feel (initial)"
              LongDesc: "Post concept art of the game’s environment(s). What does the world look like?"
        - Name: "Tech"
          Tasks:
            - TaskID: Tech01
              Name: "List of Systems (Initial)"
              LongDesc: "Consider all of the systems that go into your game - what are they? What sort of information needs to be tracked? Of all the different types of interactions a player can take, what is involved? If you don’t know, be sure to ask for help."
            - TaskID: Tech02
              Name: Interaction Prototype (Initial)
              LongDesc: "Knowing the main interaction(s) a player can take in the game, make a prototype (or several, depending on the amount and variety of interactions) that demonstrates these interactions."
            - TaskID: Tech03
              Name: Controls Prototype (Initial)
              LongDesc: "Based off of the interaction prototype (or list of interactions), create a prototype that focuses on nailing down the controls of the game."
            - TaskID: Tech04
              Name: Gameplay Prototype (Initial)
              LongDesc: "Create a prototype that starts bringing the various elements of your game together - this should illustrate how all the different aspects of the game come together to make the whole. It should also try to give people a better idea of what the game is all about, so make sure other people can understand how to play!"
        - Name: "Story"
          Tasks:
            - TaskID: Story01
              Name: "Story Summary (Midway)"
              LongDesc: "Continue to flesh out your high level summary of your game's story (if it has one)."
            - TaskID: Story02
              Name: "Detailed Plotline (Initial)"
              LongDesc: "If story is important to your game, start plotting out what happens throughout the course of the story. Even if you have a big surprise, you’ll need to get into detail about what it is in your Design Document."
    - TaskGroupID: TaskGroup02
      Name: "Crystallize the Idea"
      TaskGroupLongDesc: "The prototypes that have been made should be giving you a clearer idea as to how the game should not only change, but also what the game wants to be. Spend some time working out further details and continue refining the game through additional prototypes."
      SubGroups:
        - Name: "Design"
          Tasks:
          - TaskID: Design07
            Name: UI Wireframe (Midway)
            LongDesc: "Based off of feedback you’ve received, work on improving the UI wireframe. Consider including more art elements within the wireframe."
        - Name: "Art"
          Tasks:
            - TaskID: Art12
              Name: Concept Art of Overall Look and Feel (Midway)
              LongDesc: Consider feedback received about the concept art and continue to hone in on the look and feel of your game with another round of concepting.
            - TaskID: Art13
              Name: Concept Art of Player's Avatar (Midway)
              LongDesc: "Update the concept art for your player character. Are there elements that are customizable by the player? What are they? If it makes sense for your game, consider these as you move forward."
            - TaskID: Art14
              Name: Concept Art of Other Characters/Elements Look and Feel (Midway)
              LongDesc: "As you continue fleshing out the various elements of the game, continue making and updating concept art for each of them."
            - TaskID: Art15
              Name: Concept Art of Environment Look and Feel (Midway)
              LongDesc: Work towards finalizing the look and feel of the environment by creating more concept art of your game’s world.
        - Name: "Tech"
          Tasks:
            - TaskID: Tech05
              Name: List of Systems (Midway)
              LongDesc: "As you refine your game idea, make sure to keep tabs on what systems you need for your game. Are there any new ones that have come out of the prototypes? Are the systems you originally wrote down still relevant to your game?"
            - TaskID: Tech06
              Name: Interaction Prototype (Midway)
              LongDesc: "Improve on the original interaction prototype(s) and refine the interactions that a player can take. If there are secondary actions a player can take (crafting, for example), be sure to prototype these out."
            - TaskID: Tech07
              Name: Controls Prototype (Near-Complete Functionality)
              LongDesc: The idea should be reaching a point where all of the main interactions should be mappable to keyboard / controller / touchpad. Build a prototype to ensure that the controls in mind make sense.
            - TaskID: Tech08
              Name: Gameplay Prototype (Midway)
              LongDesc: "Using the other prototypes as reference, continue building out gameplay prototypes. Integrating  elements like concept art and sound could be a good step forward here."
        - Name: Sound
          Tasks:
            - TaskID: Sound04
              Name: List of Music (Initial)
              LongDesc: "Begin thinking about the different music elements you need for the game and where they would be used. E.g, is there music for the title screen? Battles? Idling about?"
            - TaskID: Sound05
              Name: List of SFX (Initial)
              LongDesc: "Based on the game so far, start listing out the different sound effects that you may need for the game. Examples include actions the player (and non-player) characters can take, ambient noise for environments, noises that the interface makes (confirm / cancel)."
        - Name: Story
          Tasks:
            - TaskID: Story03
              Name: Story Summary (Midway)
              LongDesc: "As you receive feedback about the story of your game, continue to improve and expand upon it."
        - Name: Production
          Tasks:
            - TaskID: Prod00
              Name: Estimate of Development Staffing (Rough)
              LongDesc: "Based on the needs of your game, what would be the needed team be? What roles would you need filled and why? What specialties / software skills would they need to have?"
            - TaskID: Prod01
              Name: Estimate of Development Time (Rough)
              LongDesc: "Based on both your staffing requirements and the complexity of your game, make a rough guess as to how long this game would take to create. This is a very early estimate, and don’t hesitate to ask for opinions."
        - Name: Business
          Tasks:
            - TaskID: Biz00
              Name: Business Model Summary (Initial)
              LongDesc: "Begin thinking and talking about how this game would make money. Who is the target audience? What sort of business model do you imagine for the game?"
            - TaskID: Biz01
              Name: Business Model Details (Initial)
              LongDesc: "In detail, describe the various ways your game could make money. For example, if your game has in-app purchases, what would they be? Why would the player want to buy those things?"
    - TaskGroupID: TaskGroup03
      Name: "Finalize the Details"
      TaskGroupLongDesc: "You’re getting to the home stretch. With all the work that has gone into your game idea, you’re getting close to making a vertical slice to show off to the world. Continue moving forward by tying up loose ends, updating the appropriate documentation, and start thinking how to best represent your game."
      SubGroups:
        - Name: "General"
          Tasks:
            - TaskID: General07
              Name: Title (Final)
              LongDesc: "At this point, you should be choosing a name for your game that is near-final (if not ACTUALLY final)."
            - TaskID: General08
              Name: Thumbnail Picture (Final)
              LongDesc: "Choose a nice piece of art that really shows off what your game is about. Make sure that this is content that is relevant to your game, not just a Google Image search."
            - TaskID: General09
              Name: Your Favorite Thing (Final)
              LongDesc: "Considering all the cool stuff that has gone into your game, be sure to update (or reiterate) the coolest thing about your game idea. This very well may have changed since you started!"
        - Name: "Design"
          Tasks:
            - TaskID: Design08
              Name: Plan for Vertical Slice
              LongDesc: "At this point, you have a good idea of what the game is about, so it’s time to start considering what you’ll need for building the Vertical Slice. This could be a level design, a set of puzzles, or whatever is relevant to your games design."
            - TaskID: Design09
              Name: UI Wireframe (Final - Detailed)
              LongDesc: "Ensure that your UI’s layout and functionality is fully documented. Be sure to include all of the different screens that are in your game."
            - TaskID: Design10
              Name: List of Key Features (Final)
              LongDesc: "Update (and if necessary, add or remove to) the list of key features your game has. Based off of all the prototypes, consider which features are core to your game, which are secondary, and which might not be needed at all."
            - TaskID: Design11
              Name: List of All System Behaviors
              LongDesc: "Make sure that every behavior in your game is fully detailed out. Ask the community if there is any ambiguity or questions about how a specific piece of your game works."
        - Name: Art
          Tasks:
            - TaskID: Art16
              Name: Concept Art of Overall Look and Feel (Final)
              LongDesc: "Gather up all of the concept art that has been done for your game and find which pieces best represent it. You’ll want to organize it in a way that gives people the best understanding of your game's art style."
            - TaskID: Art17
              Name: Concept Art of Player’s Avatar (Final)
              LongDesc: Gather up all of the concept art done for your player’s character (along with any customization that might go along with it).
            - TaskID: Art18
              Name: Concept Art of Other Characters/Elements Look and Feel (Final)
              LongDesc: "Collect all of the artwork done for the various characters, enemies and random elements that are within your game. Finalize any decisions about the style of these characters and elements."
            - TaskID: Art19
              Name: Concept Art of Environment Look and Feel (Final)
              LongDesc: "Find which environment concept art pieces best represents your game’s levels and world."
            - TaskID: Art20
              Name: List of Vertical Slice Art Assets
              LongDesc: "After planning out what will be shown in the vertical slice, write out a detailed asset list of every character, element, environment, interface element, etc. that will be needed to build this prototype."
        - Name: Tech
          Tasks:
            - TaskID: Tech09
              Name: Software Platform
              LongDesc: "By this time, you know which platform the game will be played on, so be sure to consider in which game engine / platform it will be built in. If you’re unsure, be sure to ask the GameSprout community."
            - TaskID: Tech10
              Name: List of Systems (Final)
              LongDesc: "At this point, all of the different elements of the game’s design and art style are in place, so be sure to have a complete list of game features and modes."
            - TaskID: Tech11
              Name: Controls Prototype (Complete Functionality and Final Feel)
              LongDesc: "Be sure to have a prototype that best shows how the game controls and feels. Things like the player’s movement speed, jump height, etc. should be decided by this point."
            - TaskID: Tech12
              Name: "Interaction Prototype(s) (Final)"
              LongDesc: "All of the different ways you plan on having the player interact with the game (inventory system, dialog, combat, movement, crafting) should have had at least one prototype to prove the concept. Be sure to make any additional prototypes necessary to iron out any rough edges / answer any outstanding questions."
        - Name: Sound
          Tasks:
            - TaskID: Sound06
              Name: List of Music Elements (Final)
              LongDesc: Finalize your list of music that would be needed for a vertical slice of your game.
            - TaskID: Sound07
              Name: List of SFX (Final)
              LongDesc: "Update your list to make sure every element that needs sound is accounted for. Detail is key here - every footstep, menu button, character voice, and cricket chirp should be represented in this list."
        - Name: Story
          Tasks:
            - TaskID: Story04
              Name: Story Summary (Final)
              LongDesc: "If important to the game, the story should be essentially figured out by now. You should know generally what the major events, plot points, and characters are. Consider how story will also play into your vertical slice."
            - TaskID: Story05
              Name: Detailed Plotline (Final)
              LongDesc: "Detailed bios about characters, the world, and the main story should be written out. While you don’t need all lines of dialogue figured out, you should be very detailed in explaining the key plot points of your game."
        - Name: Production
          Tasks:
            - TaskID: Prod02
              Name: Estimate of Development Staffing (Midway)
              LongDesc: "Based off of the work and changes that have happened since the last batch of work that has been done, is your estimate for a development team still accurate?"
            - TaskID: Prod03
              Name: Estimate of Development Time (Midway)
              LongDesc: "Based off of the work and changes that have happened to both your idea and development team estimate, is your estimate at how long this game would take to build accurate?"
        - Name: Business
          Tasks:
            - TaskID: Biz02
              Name: Business Model Details (Midway)
              LongDesc: "You should continue hashing out the smaller details about how you plan for this game to make money."
    - TaskGroupID: TaskGroup04
      Name: "Create the Vertical Slice"
      TaskGroupLongDesc: "It’s time to take all of the work that has been done on your idea and bring it together! As assets are created, you’ll need to make sure that each person working on your idea has what they need to move forward."
      SubGroups:
        - Name: Design
          Tasks:
            - TaskID: Design12
              Name: Detailed System Behaviors
              LongDesc: "Specifically with the vertical slice in mind, be sure that every element you want to show off is planned for. Answer any questions that may arise during implementation."
        - Name: Art
          Tasks:
            - TaskID: Art21
              Name: Vertical Slice Art Assets
              LongDesc: "Based off of your list of assets, keep track of how far along your progress is for both asset creation and implementation. Be sure to update the list of assets as things develop."
        - Name: Tech
          Tasks:
            - TaskID: Tech13
              Name: "Gameplay Prototype (Complete Vertical Slice)"
              LongDesc: "Develop the prototype by implementing both the game mechanics and the art/sound assets that are being created for it. These should come together to make something awesome!"
        - Name: Sound
          Tasks:
            - TaskID: Sound08
              Name: Vertical Slice Music Assets
              LongDesc: "Get at least one representative music track implemented into your game."
            - TaskID: Sound09
              Name: Vertical Slice SFX Assets
              LongDesc: "Keep track of your progress with all of the different sound effects in your game. As they are being implemented, make sure that they are playing correctly."
        - Name: Story
          Tasks:
            - TaskID: Story06
              Name: Vertical Slice Text Assets
              LongDesc: "Make sure that any relevant text for your game’s story is accounted for and implemented within the prototype. Also, remember to proofread!"
        - Name: Production
          Tasks:
            - TaskID: Prod04
              Name: Estimate of Development Staffing (Final)
              LongDesc: "By this point, you should have a good idea of the team you need to build your game beyond the vertical slice. Be sure your estimates are updated accordingly."
            - TaskID: Prod05
              Name: Estimate of Development Time (Final)
              LongDesc: "Once you know all of the features that MUST be in the game, be sure to double check and then update your development time estimates."
        - Name: Business
          Tasks:
            - TaskID: Biz03
              Name: Business Model Summary (Final)
              LongDesc: "Considering the changes that have happened to your game, make sure to update the summary of your business model. Does your target audience still make sense? Is the target audience large enough to market to? How do you plan on distributing your game?"
            - TaskID: Biz04
              Name: Business Model Details (Final)
              LongDesc: "Go through and make sure any numbers involving money are accurate and relevant to your game."

