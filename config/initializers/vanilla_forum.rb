GamefactoryTest::Application.configure do
  # Used for signing Vanilla forums single-sign-on requests
  config.vanilla_forum_client_id = "1787608412"
  config.vanilla_forum_secret = "994b74bf967fde18834801c8be2c5b90"

  # discussions feed
  config.vanilla_forum_url = 'http://forums.gamesprout.com'
  config.vanilla_forum_rss_url = 'http://forums.gamesprout.com/discussions/feed.rss'
end