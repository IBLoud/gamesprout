GamefactoryTest::Application.configure do
  config.facebook_url = 'http://facebook.com/gamesproutsg'
  config.twitter_url = 'http://twitter.com/gamesprout'  
  config.schell_games_url = 'http://schellgames.com'
  config.spreecast_url = 'http://www.spreecast.com/channels/gamesprouts-channel'
  config.press_kit_url = 'http://gamefactory.puzzleclubhouse.com/press-kit/GameSprout_Press_Kit_8_27_13.zip'
end