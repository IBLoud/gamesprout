## NOTE: If you provide the wrong region or bucket name, then you end up with a cross domain policy error.
## This is not actually related to the error! For example, the US Standard buckets have a different url format
## than others, so if you have a bucket that is not US Standard, you HAVE to set an S3_REGION_NAME or you get an CORS error.

## Copypasta of CORS config for testing:

# <?xml version="1.0" encoding="UTF-8"?>
# <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
#     <CORSRule>
#         <AllowedOrigin>*</AllowedOrigin>
#         <AllowedMethod>GET</AllowedMethod>
#         <AllowedMethod>POST</AllowedMethod>
#         <AllowedMethod>PUT</AllowedMethod>
#         <MaxAgeSeconds>3000</MaxAgeSeconds>
#         <AllowedHeader>*</AllowedHeader>
#     </CORSRule>
# </CORSConfiguration>

S3DirectUpload.config do |c|
  c.access_key_id = ENV['S3_ACCESS_KEY_ID']         || "AKIAILIO3AXSHUGFBKNQ"                       # your access key id
  c.secret_access_key = ENV['S3_SECRET_ACCESS_KEY'] || "ckZfa7V7+QXNRZS2kbSGHqPUIy37MoPSJL9fW5Q1"   # your secret access key
  c.bucket = ENV['S3_BUCKET_NAME']                  || "gamesprout-testing2"                         # your bucket name
  c.region = ENV['S3_REGION_NAME'] if ENV['S3_REGION_NAME']                               # your region name (defaults to us-east-1 ["US Standard"] otherwise)
end

# used for admin of the file system
AWS.config access_key_id: S3DirectUpload.config.access_key_id, secret_access_key: S3DirectUpload.config.secret_access_key, region: 'us-east-1'