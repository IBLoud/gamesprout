GamefactoryTest::Application.configure do
  config.youtube_url = 'http://youtube.com/gamesproutvideo'
  config.youtube_rss_url = 'http://gdata.youtube.com/feeds/api/users/GameSproutVideo/uploads'
end