class String
  def maintain_linebreaks
    self.gsub(/\n/, '<br/>').gsub("\r", '')
  end

  def restore_linebreaks
    self.gsub(/<br\/>/, "\n") 
  end

  def convert_urls_to_anchors
    text = self.gsub /(https?:\/\/[^ \r\n\<\]\)]+)/, '<a href=\'\1\'>\1</a>'
    text = text.gsub /((?<!http:\/\/)(?<!https:\/\/)www\.[^ \r\n\<\]\)\}]+)/, '<a href=\'http://\1\'>\1</a>'

    return text
  end
end