# Code from GameSprout
A gift from Jesse Schell.
Jesse Schell was a Keynote speaker the year I worked [SIEGE](https://www.youtube.com/watch?v=h4JuD5k9rg4). 
His team saw itch.io as the new replacement so, this code has no value to them. Probably why he agreed to give it to me.

Unfortunately, his team did NOT give me the code in a useable state. The code is using Ruby 2 and Rails 3, some gems are not supported anymore. Today we are now in Ruby 3 and Rails 6. notes from alessandrostein Fiverr developer 

However, the entire process for a complete novice to create a video game is in this code. I am looking for a POC Ruby developer who is interested in this type of challenge.

## Challenge
Get this code base into a production state using Clean coding principles or a community-driven style Guide using [RuboCop](https://rubocop.org/), which is extremely flexible and most aspects of its behavior can be tweaked via various configuration options. In practice [RuboCop](https://github.com/rubocop-hq/rubocop) supports pretty much every (reasonably popular) coding style that you can think of.

Apart from reporting problems in your code, RuboCop can also automatically fix some of the problems for you.
The style guide uses Forensic Techniques to Arrest Defects, Bottlenecks, and Bad Design in Your Programs or looks at [Your Code as a Crime Scene](https://pragprog.com/titles/atcrime/your-code-as-a-crime-scene/)
15-minute Video Lecture: Code, Crime, Complexity: Analyzing software with forensic psychology - [TEDxTrondheim](https://www.youtube.com/watch?v=qJ_hplxTYJw) by [Adam Tornhill](http://adamtornhill.com/)

### What is the goal of this project?
to connect amateurs and hobbyists with each other and with working professionals so we can all share feedback and ideas, and all help each other to grow. Because the next great game design could come from any one of us – any one of us who takes the time to share their idea with the world. 

Principle the First: Anyone Can Design a Great Game
Principle the Second: All of us Together are Better Designers than any of us Alone
Principle the Third: The More we all Share, the Smarter we all Become

### Why is this project useful?
Good design comes through sharing, not by keeping secrets. [Urban Alien Adventure games](https://urbanalienadventures.github.io/gamesprout/) will grow gradually, and everyone in the community will be able to playtest and comment on the works in progress, so we can all decide together what will be best, and all get a chance to get our ideas in about how to make each game idea as good as it can be. 

### How do I get started?
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md) <br>
If you’re not sure how to get started as a contributor, check out [How to Contribute to Open Source](https://opensource.guide/how-to-contribute/) guide.<br>
For more about the code please read our [Wiki](https://github.com/UrbanAlienAdventures/gamesprout/wiki)<br>
When creating issues, it's important to follow common guidelines to make them extra clear. Here is a few links that we liked to help you achieve that:<br>
* [GitHub Guides: Mastering Issues](https://guides.github.com/features/issues/)
* [Wiredcraft: How We Write Github Issues](https://wiredcraft.com/blog/how-we-write-our-github-issues/)
* [NYC Planning Digital: Writing Useful Github Issues](https://medium.com/nyc-planning-digital/writing-a-proper-github-issue-97427d62a20f)

### Where can I get more help, if I need it?
[![Open Source Helpers](https://www.codetriage.com/urbanalienadventures/gamesprout/badges/users.svg)](https://www.codetriage.com/urbanalienadventures/gamesprout)
[App Academy Open](https://open.appacademy.io/) or check out [Career Karma](https://careerkarma.com/) to learn about other bootcamps and tech training programs.

### What does GPL-3.0 License mean?
The [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/) also lets people do almost anything they want with your project, except distributing closed source versions. Example projects are: [Ansible](https://github.com/ansible/ansible/blob/devel/COPYING), [Bash](https://git.savannah.gnu.org/cgit/bash.git/tree/COPYING), and [GIMP](https://git.gnome.org/browse/gimp/tree/COPYING).
