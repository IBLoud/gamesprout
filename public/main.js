$(function() {
	$arrow = $('.drop-trigger').find('span.arr-down')
	$('.drop-trigger').on('click', function(evt){
		evt.preventDefault();
		var $pDropdown = $(this).closest('.dropdown');
		var isOn = $pDropdown.hasClass("on");

		if(isOn){
			$pDropdown.removeClass('on');
		} else {
			$pDropdown.addClass('on');
		}
	});
});
